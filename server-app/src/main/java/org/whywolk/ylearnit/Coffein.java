/*
 * License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 * Copyright (c) 2023  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit;

import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.core.MediaType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.TimerTask;

public class Coffein extends TimerTask {
    private static final Logger log = LogManager.getLogger(Coffein.class);
    private static final String API_URL = System.getenv("API_URL");
    private final Client client = ClientBuilder.newClient();

    public Coffein() {
        log.info("API URL = {}", API_URL);
    }

    @Override
    public void run() {
        ping();
    }

    public String ping() {
        if (API_URL == null) {
            return "";
        }
        String req = client
                .target(API_URL)
                .path("/monitor/ping")
                .request(MediaType.TEXT_PLAIN)
                .get(String.class);
        return req;
    }
}
