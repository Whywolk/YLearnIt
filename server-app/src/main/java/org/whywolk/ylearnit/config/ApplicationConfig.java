/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022-2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.annotations.servers.Server;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.whywolk.ylearnit.GuiceFeature;
import org.whywolk.ylearnit.security.rest.*;
import org.whywolk.ylearnit.study.rest.resource.*;

import java.util.HashSet;
import java.util.Set;

@OpenAPIDefinition(
        info = @Info(title = "YLearnIt REST API"),
        servers = {@Server(url = "/api", description = "localhost")}
)
@SecurityScheme(
        name = "JWT",
        description = "JWT authentication with bearer token",
        type = SecuritySchemeType.HTTP,
        scheme = "bearer",
        bearerFormat = "Bearer [token]")
public class ApplicationConfig extends ResourceConfig {
    private final Set<Class> resources = new HashSet<>();
    private final Set<Class> filters = new HashSet<>();
    private final Set<Class> features = new HashSet<>();

    public ApplicationConfig(GuiceFeature guiceFeature) {
        this.init(guiceFeature);
    }

    private void init(GuiceFeature guiceFeature) {
        this.setFilters();
        this.setFeatures();
        this.setResources();

        this.register(guiceFeature);
        this.register(this.filters);
        this.register(this.features);
        this.register(this.resources);

        this.packages("io.swagger.v3.jaxrs2.integration.resources");
    }

    private void setResources() {
        this.resources.add(UserResource.class);
        this.resources.add(AuthResource.class);
        this.resources.add(AdminResource.class);
        this.resources.add(MonitorResource.class);

        this.resources.add(StudyResource.class);
        this.resources.add(CourseResource.class);
        this.resources.add(ReviewResource.class);
        this.resources.add(StatsResource.class);
    }

    private void setFilters() {
        this.filters.add(CorsFilter.class);
        this.filters.add(SecurityRequestFilter.class);
        this.filters.add(ResponseLoggingFilter.class);
    }

    private void setFeatures() {
        this.features.add(MultiPartFeature.class);
        this.features.add(JacksonFeature.class);
    }

    private void register(Set<Class> components) {
        components.forEach(this::register);
    }
}
