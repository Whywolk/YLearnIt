/*
 * License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 * Copyright (c) 2022-2023  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Stage;
import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.whywolk.ylearnit.cli.CommandLineOptions;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Application {
    private static final Logger log = LogManager.getLogger(Application.class);
    private static final String HEADER =
            "* YLearnit *\n" +
            "License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>\n" +
            "Copyright (c) 2022-2025  Author: Alex Shirshov <https://codeberg.org/Whywolk>\n";

    public static void main(String[] args) {
        try {
            printHeader(HEADER);

            CommandLineOptions options = new CommandLineOptions();
            CommandLineParser commandLineParser = new DefaultParser();
            CommandLine commandLine = commandLineParser.parse(options.getOptions(), args);

            if (commandLine.hasOption("help")) {
                HelpFormatter helpFormatter = new HelpFormatter();
                helpFormatter.printHelp("ylearnit.jar", options.getOptions(), true);
                return;
            }

            drinkCoffee(commandLine);

            Injector injector = createInjector(commandLine);

            int port = commandLine.getParsedOptionValue("port", YLearnItServer.DEFAULT_PORT);
            boolean isLocalhost = commandLine.hasOption("localhost");

            YLearnItServer server = new YLearnItServer(port, isLocalhost);
            server.initServer(injector);
            server.start();
        } catch (Exception e) {
            log.error(e);
        }
    }

    private static void printHeader(String header) {
        System.out.println(header);
    }

    private static void drinkCoffee(CommandLine commandLine) {
        if (commandLine.hasOption("coffee")) {
            log.info("Drinking some coffee...");
            ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
            long delay = 1;
            long period = 10;
            executor.scheduleAtFixedRate(new Coffein(), delay, period, TimeUnit.MINUTES);
        }
    }

    private static Injector createInjector(CommandLine commandLine) throws ParseException {
        Stage stage = Stage.PRODUCTION;
        if (commandLine.hasOption("environment")) {
            if (Stage.DEVELOPMENT.name().equals(commandLine.getParsedOptionValue("environment"))) {
                stage = Stage.DEVELOPMENT;
            }
        }
        log.info("Application environment is {}", stage.name());

        log.info("Creating Guice injector");
        Injector injector = Guice.createInjector(stage, new ApplicationModule());
        log.info("Guice injector created");
        return injector;
    }
}