package org.whywolk.ylearnit;

import com.google.inject.Injector;
import jakarta.ws.rs.core.Feature;
import jakarta.ws.rs.core.FeatureContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.jersey.InjectionManagerProvider;
import org.jvnet.hk2.guice.bridge.api.GuiceBridge;
import org.jvnet.hk2.guice.bridge.api.GuiceIntoHK2Bridge;

public class GuiceFeature implements Feature {
    private static final Logger log = LogManager.getLogger(GuiceFeature.class);
    private final Injector injector;

    public GuiceFeature(Injector injector) {
        this.injector = injector;
    }

    @Override
    public boolean configure(FeatureContext context) {
        log.info("Creating Guice into HK2 bridge");

        ServiceLocator locator = InjectionManagerProvider.getInjectionManager(context)
                .getInstance(ServiceLocator.class);

        GuiceBridge.getGuiceBridge().initializeGuiceBridge(locator);
        GuiceIntoHK2Bridge guiceBridge = locator.getService(GuiceIntoHK2Bridge.class);
        guiceBridge.bridgeGuiceInjector(injector);

        log.info("Success Guice into HK2 bridge setup");
        return true;
    }
}