/*
 * License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 * Copyright (c) 2023  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit;

import com.google.inject.Injector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ErrorPageErrorHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.spi.ContainerLifecycleListener;
import org.glassfish.jersey.servlet.ServletContainer;
import org.whywolk.ylearnit.config.ApplicationConfig;

public class YLearnItServer {
    private static final Logger log = LogManager.getLogger(YLearnItServer.class);
    public static final int DEFAULT_PORT = 8081;
    private final Server jettyServer;
    private final ServerConnector connector;
    private final ContextHandlerCollection contextCollection;

    public YLearnItServer(int port, boolean localhost) {
        jettyServer = new Server();
        connector = new ServerConnector(jettyServer);
        if (localhost) {
            connector.setHost("localhost");
        } else {
            connector.setHost("0.0.0.0");
        }
        connector.setPort(port);
        jettyServer.addConnector(connector);

        contextCollection = new ContextHandlerCollection();
        jettyServer.setHandler(contextCollection);
    }

    public YLearnItServer() {
        this(DEFAULT_PORT, false);
    }

    public void start() throws Exception {
        try {
            jettyServer.start();
            log.info("Server started at http://" + connector.getHost() + ":" + connector.getPort());
            jettyServer.join();
        } catch (Exception e) {
            log.error(e);
        } finally {
            jettyServer.stop();
            jettyServer.destroy();
            log.info("Server stopped");
        }
    }

    public void initServer(Injector injector) throws Exception {
        GuiceFeature listener = new GuiceFeature(injector);
        ResourceConfig config = new ApplicationConfig(listener);
        ServletContextHandler apiContext = buildUsingResourceConfig(config);
        contextCollection.addHandler(apiContext);

        ServletContextHandler frontendContext = buildUsingStaticHtml("./ylearnit-angular");
        contextCollection.addHandler(frontendContext);
    }

    public ServletContextHandler buildUsingResourceConfig(ResourceConfig config) {
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.NO_SESSIONS);
        context.setContextPath("/api");
        context.addServlet(new ServletHolder(new ServletContainer(config)), "/*");

        return context;
    }

    public ServletContextHandler buildUsingStaticHtml(String resourceBase) {
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/ylearnit");
        context.setWelcomeFiles(new String[] {"index.html"});
        context.setResourceBase(resourceBase);

        ErrorPageErrorHandler errorHandler = new ErrorPageErrorHandler();
        errorHandler.addErrorPage(404, "/index.html");
        context.setErrorHandler(errorHandler);

        context.addServlet(DefaultServlet.class, "/");

        return context;
    }
}
