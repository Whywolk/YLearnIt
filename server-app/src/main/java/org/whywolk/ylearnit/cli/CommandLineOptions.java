package org.whywolk.ylearnit.cli;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

public class CommandLineOptions {
    private final Options options;

    public CommandLineOptions() {
        this.options = new Options();
        this.initOptions();
    }

    public Options getOptions() {
        return options;
    }

    private void initOptions() {
        this.options.addOption(helpOption());
        this.options.addOption(portOption());
        this.options.addOption(isLocalhostOption());
        this.options.addOption(environmentOption());
        this.options.addOption(coffeeOption());
    }

    private Option helpOption() {
        return Option.builder()
                .option("h")
                .longOpt("help")
                .desc("Print help text and exit")
                .build();
    }

    private Option portOption() {
        return Option.builder()
                .option("p")
                .longOpt("port")
                .desc("Server port, default is 8081")
                .type(Integer.class)
                .hasArg()
                .build();
    }

    private Option isLocalhostOption() {
        return Option.builder()
                .option("l")
                .longOpt("localhost")
                .desc("Setup server access only from localhost")
                .build();
    }

    private Option environmentOption() {
        return Option.builder()
                .option("env")
                .longOpt("environment")
                .desc("Application environment, default is 'PRODUCTION', also available 'DEVELOPMENT")
                .hasArg()
                .build();
    }

    private Option coffeeOption() {
        return Option.builder()
                .option("coffee")
                .longOpt("coffee")
                .desc("Send requests to the server to avoid falling asleep")
                .build();
    }
}
