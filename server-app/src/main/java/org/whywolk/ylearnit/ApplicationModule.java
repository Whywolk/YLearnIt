package org.whywolk.ylearnit;

import com.google.inject.AbstractModule;
import org.whywolk.ylearnit.openrussian.OpenRussianModule;
import org.whywolk.ylearnit.security.SecurityModule;
import org.whywolk.ylearnit.study.StudyModule;

public class ApplicationModule extends AbstractModule {

    @Override
    protected void configure() {
        install(new SecurityModule());
        install(new OpenRussianModule());
        install(new StudyModule());
    }
}
