/*
 * License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 * Copyright (c) 2022-2023  Author: Alex Shirshov <https://github.com/Whywolk>
 *
 */

package org.whywolk.ylearnit.japanese.repository;

import jakarta.persistence.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.whywolk.ylearnit.japanese.entity.kana.Kana;
import org.whywolk.ylearnit.japanese.entity.kana.Transliteration;

import java.util.List;

public class KanaRepository {
    private static final Logger log = LogManager.getLogger(KanaRepository.class);

    private static final EntityManagerFactory emf;
    private static final ThreadLocal<EntityManager> threadLocal;

    static {
        log.info("Initialization Kana repository started");
        emf = Persistence.createEntityManagerFactory("kana-unit");
        threadLocal = new ThreadLocal<EntityManager>();
        log.info("Initialization Kana repository finished");
    }

    public KanaRepository() {
    }

    private static EntityManager getEntityManager() {
        EntityManager em = threadLocal.get();
        if (em == null) {
            em = emf.createEntityManager();
            threadLocal.set(em);
        }
        return em;
    }

    private static void closeEntityManager() {
        EntityManager em = threadLocal.get();
        if (em != null) {
            em.close();
            threadLocal.set(null);
        }
    }

    public Kana getById(Integer id) {
        EntityManager em = getEntityManager();
        Kana kana = em.find(Kana.class, id);
        closeEntityManager();
        if (kana == null) {
            log.warn("Can't find Kana by ID = {}", id);
            throw new EntityNotFoundException("Can't find Kana by ID = " + id);
        }
        return kana;
    }

    public Kana getByLiteral(String literal) {
        EntityManager em = getEntityManager();
        TypedQuery<Kana> query = em.createQuery("SELECT k FROM Kana k WHERE k.literal = :s", Kana.class)
                .setParameter("s", literal);
        Kana kana = query.getSingleResult();
        closeEntityManager();
        return kana;
    }

    public List<Kana> getAll() {
        EntityManager em = getEntityManager();
        TypedQuery<Kana> query = em.createQuery("SELECT k FROM Kana k", Kana.class);
        List<Kana> kanaList = query.getResultList();
        closeEntityManager();
        return kanaList;
    }

    public List<Kana> getAllHiragana() {
        EntityManager em = getEntityManager();
        TypedQuery<Kana> query = em.createQuery("SELECT k FROM Kana k WHERE k.type = :type", Kana.class)
                .setParameter("type", Kana.Type.hiragana);
        List<Kana> hiraList = query.getResultList();
        closeEntityManager();
        return hiraList;
    }

    public List<Kana> getAllHiragana(Transliteration.SystemName systemName) {
        EntityManager em = getEntityManager();
        TypedQuery<Kana> query = em.createQuery("SELECT k FROM Kana k, Transliteration t" +
                                                " WHERE k.type = :k_type AND t.systemName = :system", Kana.class)
                .setParameter("k_type", Kana.Type.hiragana)
                .setParameter("system", systemName);
        List<Kana> hiraList = query.getResultList();
        closeEntityManager();
        return hiraList;
    }

    public List<Kana> getAllKatakana() {
        EntityManager em = getEntityManager();
        TypedQuery<Kana> query = em.createQuery("SELECT k FROM Kana k WHERE k.type = :type", Kana.class)
                .setParameter("type", Kana.Type.katakana);
        List<Kana> kataList = query.getResultList();
        closeEntityManager();
        return kataList;
    }

    public List<Kana> getAllKatakana(Transliteration.SystemName systemName) {
        EntityManager em = getEntityManager();
        TypedQuery<Kana> query = em.createQuery("SELECT k FROM Kana k, Transliteration t" +
                                                " WHERE k.type = :k_type AND t.systemName = :system", Kana.class)
                .setParameter("k_type", Kana.Type.katakana)
                .setParameter("system", systemName);
        List<Kana> kataList = query.getResultList();
        closeEntityManager();
        return kataList;
    }
}
