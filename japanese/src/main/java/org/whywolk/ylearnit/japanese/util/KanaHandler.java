/*
 * License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 * Copyright (c) 2022  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.japanese.util;

import org.whywolk.ylearnit.japanese.entity.kana.Kana;
import org.whywolk.ylearnit.japanese.entity.kana.Transliteration;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;
import java.util.ArrayList;
import java.util.List;

public class KanaHandler extends DefaultHandler {
    private StringBuilder currentValue = new StringBuilder();
    private List<Kana> result;

    private Kana curKana;
    private List<Kana> curKanaList = new ArrayList<>();

    private Transliteration curTransliteration;
    private List<Transliteration> curTransliterations = new ArrayList<>();

    public KanaHandler() {}

    public List<Kana> getResult() {
        return result;
    }

    @Override
    public void startDocument() {
        result = new ArrayList<>();
        System.out.println("Started");
    }

    @Override
    public void endDocument() {
        System.out.println("Ended");
    }

    @Override
    public void startElement(String uri,
                             String localName,
                             String qName,
                             Attributes attributes) {
        currentValue.setLength(0);

        if (qName.equals("character")) {
            curKanaList.clear();
        }

        if (qName.equals("hiragana")) {
            curKana = new Kana();
            curKana.setType(Kana.Type.hiragana);
        }

        if (qName.equals("katakana")) {
            curKana = new Kana();
            curKana.setType(Kana.Type.katakana);
        }

        if (qName.equals("transliterations")) {
            curTransliterations = new ArrayList<>();
        }

        if (qName.equals("transliteration")) {
            curTransliteration = new Transliteration();
            switch (attributes.getValue("type")) {
                case "roman":
                    curTransliteration.setType(Transliteration.Type.roman);
                    break;
                case "cyrillic":
                    curTransliteration.setType(Transliteration.Type.cyrillic);
                    break;
            }
        }

    }

    public void endElement(String uri,
                           String localName,
                           String qName) {

        if (qName.equals("hiragana") || qName.equals("katakana")) {
            curKanaList.add(curKana);
        }

        if (qName.equals("transliteration")) {
            curTransliterations.add(curTransliteration);
            curTransliteration.setKanaList(curKanaList);
        }

        if (qName.equals("transliterations")) {
            for (Kana kana: curKanaList) {
                kana.setTransliterations(curTransliterations);
            }
        }

        if (qName.equals("literal")) {
            curKana.setLiteral(currentValue.toString());
        }

        if (qName.equals("stroke_count")) {
            curKana.setStrokeCount(Integer.valueOf(currentValue.toString()));
        }

        if (qName.equals("ucs")) {
            curKana.setUnicode(currentValue.toString());
        }

        if (qName.equals("name")) {
            switch (currentValue.toString()) {
                case "Hepburn":
                    curTransliteration.setSystemName(Transliteration.SystemName.hepburn);
                    break;
                case "Kunrei-shiki":
                    curTransliteration.setSystemName(Transliteration.SystemName.kunreishiki);
                    break;
                case "Nihon-shiki":
                    curTransliteration.setSystemName(Transliteration.SystemName.nihonshiki);
                    break;
                case "Polivanov":
                    curTransliteration.setSystemName(Transliteration.SystemName.polivanov);
                    break;
            }
        }

        if (qName.equals("vowel")) {
            curTransliteration.setVowel(currentValue.toString());
        }

        if (qName.equals("consonant")) {
            curTransliteration.setConsonant(currentValue.toString());
        }

        if (qName.equals("value")) {
            curTransliteration.setValue(currentValue.toString());
        }

        // End analyze kanji
        if (qName.equals("character")) {
            result.add(curKanaList.get(0));
            result.add(curKanaList.get(1));
        }
    }

    @Override
    public void characters(char ch[], int start, int length) {
        currentValue.append(ch, start, length);
    }
}
