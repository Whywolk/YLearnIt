/*
 * License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 * Copyright (c) 2022-2023  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.japanese.service;

import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.NoResultException;
import org.whywolk.ylearnit.japanese.entity.kana.Kana;
import org.whywolk.ylearnit.japanese.repository.KanaRepository;

import java.util.ArrayList;
import java.util.List;

public class KanaService {

    private static volatile KanaService instance;

    private final KanaRepository kanaRepository;

    private KanaService() {
        kanaRepository = new KanaRepository();
    }

    public static KanaService getInstance() {
        KanaService localInstance = instance;

        if (localInstance == null) {
            synchronized (KanaService.class) {
                localInstance = instance;

                if (localInstance == null) {
                    instance = localInstance = new KanaService();
                }
            }
        }
        return instance;
    }

    public Kana getById(Integer id) {
        return kanaRepository.getById(id);
    }

    public List<Kana> getAllHiragana() {
        return kanaRepository.getAllHiragana();
    }

    public List<Kana> getAllKatakana() {
        return kanaRepository.getAllKatakana();
    }

    public Kana getByLiteral(String literal) {
        try {
            return kanaRepository.getByLiteral(literal);
        } catch (NoResultException ee) {
            throw new EntityNotFoundException("Kana not found by symbol" + literal);
        }
    }

    public List<String> getVowels() {
        List<String> vowels = new ArrayList<>();

        for (int i = 1; i < 6; i++) {
            vowels.add(kanaRepository.getById(i).getTransliterations().get(0).getVowel());
        }

        return vowels;
    }

    public List<String> getConsonants() {
        String prevConsonant = " ";
        List<String> consonants = new ArrayList<>();

        // Get all consonants
        for (Kana kana : kanaRepository.getAllHiragana()) {
            String curConsonant = kana.getTransliterations().get(0).getConsonant();

            if (!curConsonant.equals(prevConsonant)) {
                prevConsonant = curConsonant;
                consonants.add(curConsonant);
            }
        }

        return consonants;
    }
}
