/*
 * License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 * Copyright (c) 2022  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.japanese.util;

import org.whywolk.ylearnit.japanese.entity.kanji.*;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class KanjiHandler extends DefaultHandler {

    private StringBuilder currentValue = new StringBuilder();
    private List<Kanji> result;
    private Kanji currentKanji;
    private List<Radical> radicalList;
    private Radical[] radicalArray = new Radical[214];
    private List<Character> radicals;

    private boolean radValueFlag = false;
    private Codepoint currentCodepoint;
    private Dictionary currentDictionary;
    private QueryCode currentQueryCode;
    private Reading currentReading;
    private Meaning currentMeaning;

    public KanjiHandler() {}

    public List<Kanji> getResult() {
        return result;
    }

    public List<Radical> getRadicalList() {
        return radicalList;
    }

    @Override
    public void startDocument() {
        result = new ArrayList<>();
        radicals = new ArrayList<>();
        try (Scanner scanner = new Scanner(new File("./radicals.txt"))){
            for (char c : scanner.useDelimiter("\\Z").next().toCharArray()) {
                radicals.add(c);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("Started");
    }

    @Override
    public void endDocument() {
        radicalList = Arrays.asList(radicalArray);

        for (Kanji kanji : result) {
            for (Character radical : radicals) {
                if (kanji.getLiteral().equals(radical.toString())) {
                    kanji.getRadical().setLiteral(kanji.getLiteral());
                    kanji.getRadical().setKanji(kanji);
                    break;
                }
            }
        }

        for (Radical radical : radicalList) {
            for (Kanji kanji : result) {
                if (radical.equals(kanji.getRadical())) {
                    radical.getKanjiList().add(kanji);
                }
            }
        }
        System.out.println("Ended");
    }

    @Override
    public void startElement(String uri,
                             String localName,
                             String qName,
                             Attributes attributes) {
        currentValue.setLength(0);

        if (qName.equals("character")) {
            currentKanji = new Kanji();
        }

        if (qName.equals("rad_value")) {
            if (attributes.getValue("rad_type").equals("classical")) {
                radValueFlag = true;
            }
        }

        if (qName.equals("cp_value")) {
            currentCodepoint = new Codepoint();
            switch (attributes.getValue("cp_type")) {
                case "ucs" -> currentCodepoint.setType(Codepoint.Type.ucs);
                case "jis208" -> currentCodepoint.setType(Codepoint.Type.jis208);
                case "jis212" -> currentCodepoint.setType(Codepoint.Type.jis212);
                case "jis213" -> currentCodepoint.setType(Codepoint.Type.jis213);
            }
        }

        if (qName.equals("dic_ref")) {
            currentDictionary = new Dictionary();
            switch (attributes.getValue("dr_type")) {
                case "nelson_c" -> currentDictionary.setType(Dictionary.Type.nelson_c);
                case "nelson_n" -> currentDictionary.setType(Dictionary.Type.nelson_n);
                case "halpern_njecd" -> currentDictionary.setType(Dictionary.Type.halpern_njecd);
                case "halpern_kkd" -> currentDictionary.setType(Dictionary.Type.halpern_kkd);
                case "halpern_kkld" -> currentDictionary.setType(Dictionary.Type.halpern_kkld);
                case "halpern_kkld_2ed" -> currentDictionary.setType(Dictionary.Type.halpern_kkld_2ed);
                case "heisig" -> currentDictionary.setType(Dictionary.Type.heisig);
                case "heisig6" -> currentDictionary.setType(Dictionary.Type.heisig6);
                case "gakken" -> currentDictionary.setType(Dictionary.Type.gakken);
                case "oneill_names" -> currentDictionary.setType(Dictionary.Type.oneill_names);
                case "oneill_kk" -> currentDictionary.setType(Dictionary.Type.oneill_kk);
                case "henshall" -> currentDictionary.setType(Dictionary.Type.henshall);
                case "sh_kk" -> currentDictionary.setType(Dictionary.Type.sh_kk);
                case "sh_kk2" -> currentDictionary.setType(Dictionary.Type.sh_kk2);
                case "sakade" -> currentDictionary.setType(Dictionary.Type.sakade);
                case "jf_cards" -> currentDictionary.setType(Dictionary.Type.jf_cards);
                case "henshall3" -> currentDictionary.setType(Dictionary.Type.henshall3);
                case "tutt_cards" -> currentDictionary.setType(Dictionary.Type.tutt_cards);
                case "crowley" -> currentDictionary.setType(Dictionary.Type.crowley);
                case "kanji_in_context" -> currentDictionary.setType(Dictionary.Type.kanji_in_context);
                case "busy_people" -> currentDictionary.setType(Dictionary.Type.busy_people);
                case "kodansha_compact" -> currentDictionary.setType(Dictionary.Type.kodansha_compact);
                case "maniette" -> currentDictionary.setType(Dictionary.Type.maniette);
                case "moro" -> {
                    currentDictionary.setType(Dictionary.Type.moro);
                    currentDictionary.setExtra(attributes.getValue("m_vol") + "." + attributes.getValue("m_page"));
                }
            }
        }

        if (qName.equals("q_code")) {
            currentQueryCode = new QueryCode();
            if (attributes.getLength() == 1) {
                switch (attributes.getValue("qc_type")) {
                    case "skip" -> currentQueryCode.setType(QueryCode.Type.skip);
                    case "sh_desc" -> currentQueryCode.setType(QueryCode.Type.sh_desc);
                    case "four_corner" -> currentQueryCode.setType(QueryCode.Type.four_corner);
                    case "deroo" -> currentQueryCode.setType(QueryCode.Type.deroo);
                }
            } else if (attributes.getLength() == 2 && attributes.getValue("qc_type").equals("skip")) {
                switch (attributes.getValue("skip_misclass")) {
                    case "posn" -> currentQueryCode.setType(QueryCode.Type.skip_misclass_posn);
                    case "stroke_count" -> currentQueryCode.setType(QueryCode.Type.skip_misclass_stroke_count);
                    case "stroke_and_posn" -> currentQueryCode.setType(QueryCode.Type.skip_misclass_stroke_and_posn);
                    case "stroke_diff" -> currentQueryCode.setType(QueryCode.Type.skip_misclass_stroke_diff);
                }
            }
        }

        if (qName.equals("reading")) {
            currentReading = new Reading();
            switch (attributes.getValue("r_type")) {
                case "ja_on" -> currentReading.setType(Reading.Type.on_yomi);
                case "ja_kun" -> currentReading.setType(Reading.Type.kun_yomi);
            }
        }

        if (qName.equals("meaning")) {
            currentMeaning = new Meaning();
            if (attributes.getLength() == 0) {
                currentMeaning.setLang(Meaning.Lang.en);
            } else switch (attributes.getValue("m_lang")) {
                case "fr" -> currentMeaning.setLang(Meaning.Lang.fr);
                case "es" -> currentMeaning.setLang(Meaning.Lang.es);
                case "pt" -> currentMeaning.setLang(Meaning.Lang.pt);
            }
        }
    }

    public void endElement(String uri,
                           String localName,
                           String qName) {

        if (qName.equals("literal")) {
            currentKanji.setLiteral(currentValue.toString());
        }

        if (qName.equals("grade")) {
            currentKanji.setGrade(Integer.parseInt(currentValue.toString()));
        }

        if (qName.equals("stroke_count")) {
            currentKanji.setStrokeCount(Integer.parseInt(currentValue.toString()));
        }

        if (qName.equals("freq")) {
            currentKanji.setFrequency(Integer.parseInt(currentValue.toString()));
        }

        if (qName.equals("jlpt")) {
            currentKanji.setJlpt(Integer.parseInt(currentValue.toString()));
        }

        if (qName.equals("rad_value")) {
            if (radValueFlag) {
                int radicalClassicalId = Integer.parseInt(currentValue.toString());
                if (radicalArray[radicalClassicalId - 1] == null) {
                    Radical radical = new Radical();
                    radical.setClassicalId(radicalClassicalId);
                    radicalArray[radical.getClassicalId() - 1] = radical;
                    currentKanji.setRadical(radical);
                } else {
                    currentKanji.setRadical(radicalArray[radicalClassicalId - 1]);
                }
            }
            radValueFlag = false;
        }

        if (qName.equals("cp_value")) {
            currentCodepoint.setCode(currentValue.toString());
            currentCodepoint.setKanji(currentKanji);

            currentKanji.getCodepoints().add(currentCodepoint);
        }

        if (qName.equals("dic_ref")) {
            try {
                currentDictionary.setRefference(Integer.valueOf(currentValue.toString()));
            } catch (NumberFormatException e) {
                String str = currentValue.toString();
                switch (currentDictionary.getType()) {
                    case moro -> currentDictionary.setRefference(Integer.valueOf(str.replaceAll("\\D+", "")));
                    case busy_people -> currentDictionary.setExtra(str);
                    case nelson_c -> {
                        String[] parts = str.split(" ");
                        currentDictionary.setRefference(Integer.valueOf(parts[0]));
                        currentDictionary.setExtra(parts[1]);
                    }
                }
            }
            currentDictionary.setKanji(currentKanji);

            currentKanji.getDictionaryList().add(currentDictionary);
        }

        if (qName.equals("q_code")) {
            currentQueryCode.setCode(currentValue.toString());
            currentQueryCode.setKanji(currentKanji);

            currentKanji.getQueryCodeList().add(currentQueryCode);
        }

        if (qName.equals("rad_name")) {
            currentKanji.getRadical().getNameList().add(currentValue.toString());
        }

        if (qName.equals("reading")) {
            if (currentReading.getType() != null) {
                currentReading.setReading(currentValue.toString());
                currentReading.setKanji(currentKanji);

                currentKanji.getReadingList().add(currentReading);
            }
        }

        if (qName.equals("meaning")) {
            currentMeaning.setMeaning(currentValue.toString());
            currentMeaning.setKanji(currentKanji);

            currentKanji.getMeaningList().add(currentMeaning);
        }

        if (qName.equals("nanori")) {
            currentKanji.getNanori().add(currentValue.toString());
        }

        // End analyze kanji
        if (qName.equals("character")) {
            result.add(currentKanji);
        }
    }

    @Override
    public void characters(char ch[], int start, int length) {
        currentValue.append(ch, start, length);
    }
}
