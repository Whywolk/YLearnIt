/*
 * License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 * Copyright (c) 2022  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.japanese.entity.kanji;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;

import java.io.Serializable;

@Entity
@Table(name = "DICTIONARY")
public class Dictionary implements Serializable {
    private static final long serialVersionUID = 2L;

    public enum Type {
        nelson_c,
        nelson_n,
        halpern_njecd,
        halpern_kkd,
        halpern_kkld,
        halpern_kkld_2ed,
        heisig,
        heisig6,
        gakken,
        oneill_names,
        oneill_kk,
        moro,
        henshall,
        sh_kk,
        sh_kk2,
        sakade,
        jf_cards,
        henshall3,
        tutt_cards,
        crowley,
        kanji_in_context,
        busy_people,
        kodansha_compact,
        maniette
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Enumerated
    @Column(name = "TYPE_ID", columnDefinition = "TINYINT")
    private Type type;

    @Column(name = "REFFERENCE", columnDefinition = "INTEGER")
    private Integer refference;

    @Column(name = "EXTRA", length = 20)
    private String extra;

    @ManyToOne
    @JoinColumn(name = "KANJI_ID", columnDefinition = "SMALLINT")
    @JsonBackReference
    private Kanji kanji;

    public Dictionary() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Dictionary(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Integer getRefference() {
        return refference;
    }

    public void setRefference(Integer refference) {
        this.refference = refference;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public Kanji getKanji() {
        return kanji;
    }

    public void setKanji(Kanji kanji) {
        this.kanji = kanji;
    }

    @Override
    public String toString() {
        return "Dictionary{" +
                "id=" + id +
                ", type=" + type +
                ", refference=" + refference +
                ", extra='" + extra + '\'' +
                '}';
    }
}
