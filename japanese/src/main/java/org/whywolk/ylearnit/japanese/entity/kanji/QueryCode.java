/*
 * License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 * Copyright (c) 2022  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.japanese.entity.kanji;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;

import java.io.Serializable;

@Entity
@Table(name = "QUERY_CODE")
public class QueryCode implements Serializable {
    private static final long serialVersionUID = 1L;

    public enum Type {
        skip,
        sh_desc,
        four_corner,
        deroo,
        skip_misclass_posn,
        skip_misclass_stroke_count,
        skip_misclass_stroke_and_posn,
        skip_misclass_stroke_diff
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Enumerated
    @Column(name = "TYPE_ID", columnDefinition = "TINYINT")
    private Type type;

    @Column(name = "CODE", length = 10)
    private String code;

    @ManyToOne
    @JoinColumn(name = "KANJI_ID", columnDefinition = "SMALLINT")
    @JsonBackReference
    private Kanji kanji;

    public QueryCode() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String value) {
        this.code = value;
    }

    public Kanji getKanji() {
        return kanji;
    }

    public void setKanji(Kanji kanji) {
        this.kanji = kanji;
    }

    @Override
    public String toString() {
        return "QueryCode{" +
                "id=" + id +
                ", type=" + type +
                ", code='" + code + '\'' +
                '}';
    }
}
