/*
 * License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 * Copyright (c) 2022  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.japanese.entity.kana;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "KANA")
public class Kana implements Serializable {
    private static final long serialVersionUID = 1L;

    public enum Type {
        hiragana,
        katakana
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", columnDefinition = "SMALLINT")
    private Integer id;

    @Column(name = "LITERAL", length = 2)
    private String literal;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "TYPE_ID", columnDefinition = "TINYINT")
    private Type type;

    @Column(name = "STROKE_COUNT", columnDefinition = "TINYINT")
    private Integer strokeCount;

    @Column(name = "UNICODE", length = 10)
    private String unicode;

    @JsonManagedReference
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "KANA_TRANSLITERATION_RELATION",
            joinColumns = @JoinColumn(name = "KANA_ID"),
            inverseJoinColumns = @JoinColumn(name = "TRANSLITERATION_ID"))
    private List<Transliteration> transliterations;

    public Kana() {
        transliterations = new ArrayList<>();
    }

    public String getLiteral() {
        return literal;
    }

    public void setLiteral(String literal) {
        this.literal = literal;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Integer getStrokeCount() {
        return strokeCount;
    }

    public void setStrokeCount(Integer strokeCount) {
        this.strokeCount = strokeCount;
    }

    public String getUnicode() {
        return unicode;
    }

    public void setUnicode(String unicode) {
        this.unicode = unicode;
    }

    public List<Transliteration> getTransliterations() {
        return transliterations;
    }

    public void setTransliterations(List<Transliteration> transliterations) {
        this.transliterations = transliterations;
    }

    public Transliteration getTransliteration(Transliteration.SystemName systemName) {
        for (Transliteration tr : transliterations){
            if (tr.getSystemName().equals(systemName)) {
                return tr;
            }
        }
        throw new RuntimeException("Unknown system name");
    }

    public Transliteration getTransliteration(String locale) {
        switch (locale) {
            case "ru_RU":
                return getTransliteration(Transliteration.SystemName.polivanov);
            default:
                return getTransliteration(Transliteration.SystemName.hepburn);
        }
    }
}
