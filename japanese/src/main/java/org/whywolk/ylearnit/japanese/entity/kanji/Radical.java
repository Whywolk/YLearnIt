/*
 * License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 * Copyright (c) 2022  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.japanese.entity.kanji;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "RADICAL")
public class Radical implements Serializable {
    private static final long serialVersionUID = 2L;

    @Id
    @Column(name = "ID", columnDefinition = "SMALLINT")
    private Integer classicalId;

    @OneToOne
    @JoinColumn(name = "KANJI_ID", columnDefinition = "SMALLINT")
    @JsonBackReference
    private Kanji kanji;

    @Column(name = "LITERAL", length = 2)
    private String literal;

    @OneToMany(mappedBy = "radical")
    private List<Kanji> kanjiList;

    private List<String> nameList;

    public Radical() {
        nameList = new ArrayList<>();
        kanjiList = new ArrayList<>();
    }

    public Integer getClassicalId() {
        return classicalId;
    }

    public void setClassicalId(Integer classicalId) {
        this.classicalId = classicalId;
    }

    public Kanji getKanji() {
        return kanji;
    }

    public void setKanji(Kanji kanji) {
        this.kanji = kanji;
    }

    public String getLiteral() {
        return literal;
    }

    public void setLiteral(String literal) {
        this.literal = literal;
    }

    public void setNameList(List<String> nameList) {
        this.nameList = nameList;
    }

    public List<String> getNameList() {
        return nameList;
    }

    public List<Kanji> getKanjiList() {
        return kanjiList;
    }

    public void setKanjiList(List<Kanji> kanjiList) {
        this.kanjiList = kanjiList;
    }
}
