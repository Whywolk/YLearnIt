/*
 * License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 * Copyright (c) 2022-2023  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.japanese.repository;

import jakarta.persistence.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.whywolk.ylearnit.japanese.entity.kanji.Kanji;
import org.whywolk.ylearnit.japanese.entity.kanji.Meaning;

import java.util.List;

public class KanjiRepository {
    private static final Logger log = LogManager.getLogger(KanjiRepository.class);

    private static final EntityManagerFactory emf;
    private static final ThreadLocal<EntityManager> threadLocal;

    static {
        log.info("Initialization Kanji repository started");
        emf = Persistence.createEntityManagerFactory("kanji-unit");
        threadLocal = new ThreadLocal<EntityManager>();
        log.info("Initialization Kanji repository finished");
    }

    public KanjiRepository() {
    }

    private static EntityManager getEntityManager() {
        EntityManager em = threadLocal.get();
        if (em == null) {
            em = emf.createEntityManager();
            threadLocal.set(em);
        }
        return em;
    }

    private static void closeEntityManager() {
        EntityManager em = threadLocal.get();
        if (em != null) {
            em.close();
            threadLocal.set(null);
        }
    }

    public Kanji getById(Integer id) {
        EntityManager em = getEntityManager();
        TypedQuery<Kanji> query = em.createQuery("SELECT k FROM Kanji k WHERE k.id = :id", Kanji.class)
                .setParameter("id", id);
        Kanji kanji = query.getSingleResult();
        closeEntityManager();
        return kanji;
    }

    public Kanji getByLiteral(String literal) {
        EntityManager em = getEntityManager();
        TypedQuery<Kanji> query = em.createQuery("SELECT k FROM Kanji k" +
                                                 " WHERE k.literal = :literal\n", Kanji.class)
                .setParameter("literal", literal);
        Kanji kanji = query.getSingleResult();
        closeEntityManager();
        return kanji;
    }

    public Kanji getByLiteral(String literal, Meaning.Lang lang) {
        EntityManager em = getEntityManager();
        TypedQuery<Kanji> query = em.createQuery("SELECT k FROM Kanji k" +
                                                 "  JOIN FETCH k.meaningList m" +
                                                 " WHERE k.literal = :literal" +
                                                 "   AND m.lang = :lang", Kanji.class)
                .setParameter("literal", literal)
                .setParameter("lang", lang);
        Kanji kanji = query.getSingleResult();
        closeEntityManager();
        return kanji;
    }

    public List<Kanji> getByMeaning(String meaning) {
        EntityManager em = getEntityManager();
        TypedQuery<Kanji> query = em.createQuery("SELECT k FROM Kanji k" +
                                                 "  JOIN FETCH k.meaningList m" +
                                                 " WHERE UPPER(m.meaning) LIKE UPPER(:meaning)", Kanji.class)
                .setParameter("meaning", "%" + meaning + "%");
        List<Kanji> kanjiList = query.getResultList();
        closeEntityManager();
        return kanjiList;
    }

    public List<Kanji> getByRadical(String literal) {
        EntityManager em = getEntityManager();
        TypedQuery<Kanji> query = em.createQuery("SELECT k FROM Kanji k" +
                                                 "  JOIN FETCH k.radical r" +
                                                 " WHERE r.literal = :literal", Kanji.class)
                .setParameter("literal", literal);
        List<Kanji> kanjiList = query.getResultList();
        closeEntityManager();
        return kanjiList;
    }

    public List<Kanji> getByGrade(Integer grade) {
        EntityManager em = getEntityManager();
        TypedQuery<Kanji> query = em.createQuery("SELECT k FROM Kanji k WHERE k.grade = :grade\n", Kanji.class)
                .setParameter("grade", grade);
        List<Kanji> kanjiList = query.getResultList();
        closeEntityManager();
        return kanjiList;
    }
}
