/*
 * License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 * Copyright (c) 2022  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.japanese.dto.kana;

import org.whywolk.ylearnit.japanese.entity.kana.Kana;

import java.io.Serializable;

public class SimpleKanaDto implements Serializable {

    private String literal;
    private String type;

    public SimpleKanaDto(Kana k) {
        this.literal = k.getLiteral();
        this.type = k.getType().name();
    }

    public String getLiteral() {
        return literal;
    }

    public void setLiteral(String literal) {
        this.literal = literal;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "SimpleKanaDto{" +
                "literal='" + literal + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
