/*
 * License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 * Copyright (c) 2022  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.japanese.entity.kanji;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "KANJI")
public class Kanji implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", columnDefinition = "SMALLINT")
    private Integer id;

    @Column(name = "LITERAL", length = 2)
    private String literal;

    @Column(name = "GRADE", columnDefinition = "TINYINT")
    private Integer grade;

    @Column(name = "STROKE_COUNT", columnDefinition = "TINYINT")
    private Integer strokeCount;

    @Column(name = "FREQUENCY", columnDefinition = "SMALLINT")
    private Integer frequency;

    @Column(name = "JLPT", columnDefinition = "TINYINT")
    private Integer jlpt;

    @ManyToOne
    @JoinColumn(name = "RADICAL_ID", columnDefinition = "SMALLINT")
    @JsonManagedReference
    private Radical radical;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "kanji", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Codepoint> codepoints;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "kanji", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Dictionary> dictionaryList;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "kanji", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<QueryCode> queryCodeList;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "kanji", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Reading> readingList;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "kanji", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Meaning> meaningList;

    private List<String> nanori;

    public Kanji() {
        codepoints = new ArrayList<>();
        dictionaryList = new ArrayList<>();
        queryCodeList = new ArrayList<>();
        readingList = new ArrayList<>();
        meaningList = new ArrayList<>();
        nanori = new ArrayList<>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLiteral() {
        return literal;
    }

    public void setLiteral(String literal) {
        this.literal = literal;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Integer getStrokeCount() {
        return strokeCount;
    }

    public void setStrokeCount(Integer strokeCount) {
        this.strokeCount = strokeCount;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    public Integer getJlpt() {
        return jlpt;
    }

    public void setJlpt(Integer jlpt) {
        this.jlpt = jlpt;
    }

    public Radical getRadical() {
        return radical;
    }

    public void setRadical(Radical radical) {
        this.radical = radical;
    }

    public List<Codepoint> getCodepoints() {
        return codepoints;
    }

    public void setCodepoints(List<Codepoint> codepoints) {
        this.codepoints = codepoints;
    }

    public List<Dictionary> getDictionaryList() {
        return dictionaryList;
    }

    public void setDictionaryList(List<Dictionary> dictionaryList) {
        this.dictionaryList = dictionaryList;
    }

    public List<QueryCode> getQueryCodeList() {
        return queryCodeList;
    }

    public void setQueryCodeList(List<QueryCode> queryCodeList) {
        this.queryCodeList = queryCodeList;
    }

    public List<Reading> getReadingList() {
        return readingList;
    }

    public void setReadingList(List<Reading> readingList) {
        this.readingList = readingList;
    }

    public List<Meaning> getMeaningList() {
        return meaningList;
    }

    public void setMeaningList(List<Meaning> meaningList) {
        this.meaningList = meaningList;
    }

    public List<String> getNanori() {
        return nanori;
    }

    public void setNanori(List<String> nanori) {
        this.nanori = nanori;
    }

    @Override
    public String toString() {
        return "Kanji{" +
                "literal='" + literal + '\'' +
                ", grade=" + grade +
                ", strokeCount=" + strokeCount +
                ", frequency=" + frequency +
                ", jlpt=" + jlpt +
                ", radical=" + radical.getClassicalId() +
                ", codepoints=" + codepoints +
                ", dictionaryList=" + dictionaryList +
                ", queryCodeList=" + queryCodeList +
                ", readingList=" + readingList +
                ", meaningList=" + meaningList +
                ", nanori=" + nanori +
                '}';
    }
}
