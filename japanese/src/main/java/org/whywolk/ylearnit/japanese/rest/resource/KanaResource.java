/*
 * License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 * Copyright (c) 2022  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.japanese.rest.resource;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import org.whywolk.ylearnit.japanese.dto.kana.DetailedKanaDto;
import org.whywolk.ylearnit.japanese.dto.kana.SimpleKanaDto;
import org.whywolk.ylearnit.japanese.entity.kana.Kana;
import org.whywolk.ylearnit.japanese.service.KanaService;

import java.util.ArrayList;
import java.util.List;

@Tag(name = "Kana resource")
@Path("/kana")
public class KanaResource {

    private final KanaService kanaService = KanaService.getInstance();

    @GET
    @Path("/{literal}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get kana by literal")
    public DetailedKanaDto getByLiteral(@PathParam("literal") String literal) {
        Kana kana = kanaService.getByLiteral(literal);
        return new DetailedKanaDto(kana);
    }

    @GET
    @Path("/hiragana")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get hiragana list")
    public List<SimpleKanaDto> getAllHiragana() {
        List<Kana> kanaList = kanaService.getAllHiragana();
        List<SimpleKanaDto> kanaDtoList = new ArrayList<>();
        for (Kana k: kanaList) {
            kanaDtoList.add(new SimpleKanaDto(k));
        }
        return kanaDtoList;
    }

    @GET
    @Path("/katakana")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get katakana list")
    public List<SimpleKanaDto> getAllKatakana() {
        List<Kana> kanaList = kanaService.getAllKatakana();
        List<SimpleKanaDto> kanaDtoList = new ArrayList<>();
        for (Kana k: kanaList) {
            kanaDtoList.add(new SimpleKanaDto(k));
        }
        return kanaDtoList;
    }
}
