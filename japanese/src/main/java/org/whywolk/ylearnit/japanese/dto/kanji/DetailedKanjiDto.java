/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.japanese.dto.kanji;

import org.whywolk.ylearnit.japanese.entity.kanji.Kanji;
import org.whywolk.ylearnit.japanese.entity.kanji.Meaning;
import org.whywolk.ylearnit.japanese.entity.kanji.Reading;

import java.io.Serializable;
import java.util.List;

public class DetailedKanjiDto implements Serializable {

    private String literal;
    private Integer grade;
    private Integer strokeCount;
    private Integer jlpt;

    private List<Meaning> meaningList;
    private List<Reading> readingList;

    public DetailedKanjiDto(Kanji k) {
        this.literal = k.getLiteral();
        this.grade = k.getGrade();
        this.strokeCount = k.getStrokeCount();
        this.jlpt = k.getJlpt();
        this.meaningList = k.getMeaningList();
        this.readingList = k.getReadingList();
    }

    public String getLiteral() {
        return literal;
    }

    public void setLiteral(String literal) {
        this.literal = literal;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Integer getStrokeCount() {
        return strokeCount;
    }

    public void setStrokeCount(Integer strokeCount) {
        this.strokeCount = strokeCount;
    }

    public Integer getJlpt() {
        return jlpt;
    }

    public void setJlpt(Integer jlpt) {
        this.jlpt = jlpt;
    }

    public List<Meaning> getMeaningList() {
        return meaningList;
    }

    public void setMeaningList(List<Meaning> meaningList) {
        this.meaningList = meaningList;
    }

    public List<Reading> getReadingList() {
        return readingList;
    }

    public void setReadingList(List<Reading> readingList) {
        this.readingList = readingList;
    }

    @Override
    public String toString() {
        return "DetailedKanjiDto{" +
                "literal='" + literal + '\'' +
                ", grade=" + grade +
                ", strokeCount=" + strokeCount +
                ", jlpt=" + jlpt +
                ", meaningList=" + meaningList +
                ", readingList=" + readingList +
                '}';
    }
}
