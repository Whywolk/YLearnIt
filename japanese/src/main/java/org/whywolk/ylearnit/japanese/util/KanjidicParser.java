/*
 * License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 * Copyright (c) 2022  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.japanese.util;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import org.whywolk.ylearnit.japanese.entity.kanji.Kanji;
import org.whywolk.ylearnit.japanese.entity.kanji.Radical;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.List;

public class KanjidicParser {

    private static List<Radical> radicalList;
    private static List<Kanji> kanjiList;

    public static void run() {
        try {
            KanjiHandler handler = new KanjiHandler();
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse("./kanjidic2.xml", handler);

            kanjiList = handler.getResult();
            radicalList = handler.getRadicalList();

            EntityManagerFactory emf = Persistence.createEntityManagerFactory("kanji-unit");
            EntityManager em = emf.createEntityManager();

            em.getTransaction().begin();
            for (Radical radical : radicalList) {
                em.persist(radical);
            }

            for (Kanji kanji : kanjiList) {
                em.persist(kanji);
            }
            em.getTransaction().commit();

        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
        }
    }
}
