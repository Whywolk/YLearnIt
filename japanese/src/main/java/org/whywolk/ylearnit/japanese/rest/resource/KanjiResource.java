/*
 * License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 * Copyright (c) 2022  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.japanese.rest.resource;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import org.whywolk.ylearnit.japanese.dto.kanji.DetailedKanjiDto;
import org.whywolk.ylearnit.japanese.dto.kanji.SimpleKanjiDto;
import org.whywolk.ylearnit.japanese.entity.kanji.Kanji;
import org.whywolk.ylearnit.japanese.service.KanjiService;

import java.util.ArrayList;
import java.util.List;

@Tag(name = "Kanji resource")
@Path("/kanji")
public class KanjiResource {

    private final KanjiService kanjiService = KanjiService.getInstance();

    @GET
    @Path("/{literal}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get kanji by literal")
    public DetailedKanjiDto getByLiteral(@PathParam("literal") String literal) {
        Kanji kanji = kanjiService.getByLiteral(literal);
        return new DetailedKanjiDto(kanji);
    }

    @GET
    @Path("/grade/{grade}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get list of kanji by grade")
    public List<SimpleKanjiDto> getByGrade(@PathParam("grade") Integer grade) {
        List<Kanji> kanjiList = kanjiService.getByGrade(grade);
        List<SimpleKanjiDto> simpleKanjiDtoList = new ArrayList<>();
        for (Kanji k: kanjiList) {
            simpleKanjiDtoList.add(new SimpleKanjiDto(k));
        }
        return simpleKanjiDtoList;
    }

}
