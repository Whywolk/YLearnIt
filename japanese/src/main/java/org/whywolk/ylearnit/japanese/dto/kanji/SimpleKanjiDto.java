/*
 * License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 * Copyright (c) 2022  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.japanese.dto.kanji;

import org.whywolk.ylearnit.japanese.entity.kanji.Kanji;

import java.io.Serializable;

public class SimpleKanjiDto implements Serializable {

    private String literal;
    private Integer grade;
    private Integer strokeCount;
    private Integer jlpt;

    public SimpleKanjiDto(Kanji k) {
        this.literal = k.getLiteral();
        this.grade = k.getGrade();
        this.strokeCount = k.getStrokeCount();
        this.jlpt = k.getJlpt();
    }

    public String getLiteral() {
        return literal;
    }

    public void setLiteral(String literal) {
        this.literal = literal;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Integer getStrokeCount() {
        return strokeCount;
    }

    public void setStrokeCount(Integer strokeCount) {
        this.strokeCount = strokeCount;
    }

    public Integer getJlpt() {
        return jlpt;
    }

    public void setJlpt(Integer jlpt) {
        this.jlpt = jlpt;
    }
}
