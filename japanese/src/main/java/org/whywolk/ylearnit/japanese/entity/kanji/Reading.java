/*
 * License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 * Copyright (c) 2022  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.japanese.entity.kanji;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;

import java.io.Serializable;

@Entity
@Table(name = "READING")
public class Reading implements Serializable {
    private static final long serialVersionUID = 1L;

    public enum Type {
        on_yomi,
        kun_yomi
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Enumerated
    @Column(name = "TYPE_ID", columnDefinition = "TINYINT")
    private Type type;

    @Column(name = "READING", length = 20)
    private String reading;

    @ManyToOne
    @JoinColumn(name = "KANJI_ID", columnDefinition = "SMALLINT")
    @JsonBackReference
    private Kanji kanji;

    public Reading() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getReading() {
        return reading;
    }

    public void setReading(String reading) {
        this.reading = reading;
    }

    public Kanji getKanji() {
        return kanji;
    }

    public void setKanji(Kanji kanji) {
        this.kanji = kanji;
    }

    @Override
    public String toString() {
        return "Reading{" +
                "id=" + id +
                ", type=" + type +
                ", reading='" + reading + '\'' +
                '}';
    }
}
