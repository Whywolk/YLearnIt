/*
 * License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 * Copyright (c) 2022  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.japanese.entity.kana;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "TRANSLITERATION")
public class Transliteration implements Serializable {
    private static final long serialVersionUID = 1L;

    public enum Type {
		roman,
		cyrillic
	}

    public enum SystemName {
		hepburn,
		kunreishiki,
		nihonshiki,
		polivanov
	}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", columnDefinition = "SMALLINT")
    private Integer id;

    @Enumerated(EnumType.ORDINAL)
	@Column(name = "TYPE_ID", columnDefinition = "TINYINT")
    private	Type type;

    @Enumerated(EnumType.ORDINAL)
	@Column(name = "SYSTEM_NAME", columnDefinition = "TINYINT")
	private SystemName systemName;

    @Column(name = "VOWEL", length = 5)
    private String vowel;

    @Column(name = "CONSONANT", length = 5)
    private String consonant;

    @Column(name = "TRANSL_VALUE", length = 10)
    private String value;

    @JsonBackReference
    @ManyToMany(mappedBy = "transliterations")
    private List<Kana> kanaList;

    public Transliteration() {
        kanaList = new ArrayList<>();
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public SystemName getSystemName() {
        return systemName;
    }

    public void setSystemName(SystemName systemName) {
        this.systemName = systemName;
    }

    public String getVowel() {
        return vowel;
    }

    public void setVowel(String vowel) {
        this.vowel = vowel;
    }

    public String getConsonant() {
        return consonant;
    }

    public void setConsonant(String consonant) {
        this.consonant = consonant;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<Kana> getKanaList() {
        return kanaList;
    }

    public void setKanaList(List<Kana> kanaList) {
        this.kanaList = kanaList;
    }
}
