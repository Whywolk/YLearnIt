/*
 * License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 * Copyright (c) 2022-2023  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.japanese.service;

import org.whywolk.ylearnit.japanese.entity.kanji.Kanji;
import org.whywolk.ylearnit.japanese.repository.KanjiRepository;

import java.util.List;

public class KanjiService {

    private static volatile KanjiService instance;

    private final KanjiRepository kanjiRepository;

    private KanjiService() {
        kanjiRepository = new KanjiRepository();
    }

    public static KanjiService getInstance() {
        KanjiService localInstance = instance;

        if (localInstance == null) {
            synchronized (KanaService.class) {
                localInstance = instance;

                if (localInstance == null) {
                    instance = localInstance = new KanjiService();
                }
            }
        }
        return instance;
    }

    public Kanji getById(Integer id) {
        return kanjiRepository.getById(id);
    }

    public Kanji getByLiteral(String literal) {
        return kanjiRepository.getByLiteral(literal);
    }

    public Kanji getByRadical(String literal) {
        return kanjiRepository.getByLiteral(literal);
    }

    public List<Kanji> getByGrade(Integer grade) {
        return kanjiRepository.getByGrade(grade);
    }
}
