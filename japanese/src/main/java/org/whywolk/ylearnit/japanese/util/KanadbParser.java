/*
 * License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 * Copyright (c) 2022  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.japanese.util;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import org.whywolk.ylearnit.japanese.entity.kana.Kana;
import org.xml.sax.SAXException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.List;

public class KanadbParser {

    private static List<Kana> kanaList;

    public static void main(String[] args) {
        try {
            KanaHandler handler = new KanaHandler();
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse("./kanadb.xml", handler);

            kanaList = handler.getResult();

            EntityManagerFactory emf = Persistence.createEntityManagerFactory("kana-unit");
            EntityManager em = emf.createEntityManager();

            em.getTransaction().begin();
            for (Kana kana : kanaList) {
                em.persist(kana);
            }
            em.getTransaction().commit();

        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
        }
    }

}
