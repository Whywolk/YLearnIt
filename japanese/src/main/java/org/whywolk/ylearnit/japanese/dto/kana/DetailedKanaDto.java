package org.whywolk.ylearnit.japanese.dto.kana;

import org.whywolk.ylearnit.japanese.entity.kana.Kana;
import org.whywolk.ylearnit.japanese.entity.kana.Transliteration;

import java.io.Serializable;

public class DetailedKanaDto implements Serializable {

    private String literal;
    private String type;
    private Integer strokeCount;
    private Transliteration transliteration;

    public DetailedKanaDto(Kana k) {
        this.literal = k.getLiteral();
        this.type = k.getType().name();
        this.strokeCount = k.getStrokeCount();
        this.transliteration = k.getTransliterations().get(0);
    }

    public String getLiteral() {
        return literal;
    }

    public void setLiteral(String literal) {
        this.literal = literal;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getStrokeCount() {
        return strokeCount;
    }

    public void setStrokeCount(Integer strokeCount) {
        this.strokeCount = strokeCount;
    }

    public Transliteration getTransliteration() {
        return transliteration;
    }

    public void setTransliteration(Transliteration transliteration) {
        this.transliteration = transliteration;
    }

    @Override
    public String toString() {
        return "DetailedKanaDto{" +
                "literal='" + literal + '\'' +
                ", type='" + type + '\'' +
                ", strokeCount=" + strokeCount +
                ", transliteration=" + transliteration +
                '}';
    }
}
