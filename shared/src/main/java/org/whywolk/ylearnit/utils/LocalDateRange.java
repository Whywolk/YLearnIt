/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2024.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.utils;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.Objects;

public class LocalDateRange {
    private final LocalDate start;
    private final LocalDate end;

    public LocalDateRange(LocalDate startInclusive, LocalDate endExclusive) {
        if (endExclusive.isBefore(startInclusive)) {
            throw new DateTimeException("End date must be on or after start date");
        }
        this.start = startInclusive;
        this.end = endExclusive;
    }

    public boolean contains(LocalDate date) {
        return date.isEqual(start) && date.isAfter(start) && date.isBefore(end);
    }

    public boolean isAfter(LocalDate date) {
        return start.isAfter(date);
    }

    public boolean isBefore(LocalDate date) {
        return end.isBefore(date);
    }

    public LocalDate getStart() {
        return start;
    }

    public LocalDate getEnd() {
        return end;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LocalDateRange that = (LocalDateRange) o;
        return start.equals(that.start) && end.equals(that.end);
    }

    @Override
    public int hashCode() {
        return Objects.hash(start, end);
    }

    @Override
    public String toString() {
        return "LocalDateRange{" +
                "start=" + start +
                ", end=" + end +
                '}';
    }
}
