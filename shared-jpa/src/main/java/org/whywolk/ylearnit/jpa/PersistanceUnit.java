package org.whywolk.ylearnit.jpa;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PersistanceUnit {
    private static final Logger log = LogManager.getLogger(PersistanceUnit.class);
    private final String persistanceUnitName;
    private EntityManagerFactory emf;

    public PersistanceUnit(String persistanceUnitName) {
        this.persistanceUnitName = persistanceUnitName;

        log.info("Creating '{}' EntityManagerFactory", this.persistanceUnitName);
        this.emf = this.createEntityManagerFactory(this.persistanceUnitName);
        log.info("Created '{}' EntityManagerFactory", this.persistanceUnitName);
    }

    public EntityManager createEntityManager() {
        if (this.emf == null) {
            throw new IllegalStateException("Unable to create EntityManager for '" + this.persistanceUnitName
                    + "' because it's closed");
        }

        return this.emf.createEntityManager();
    }

    private EntityManagerFactory createEntityManagerFactory(String persistenceUnitName) {
        if (this.emf != null) {
            this.emf.close();
        }
        return Persistence.createEntityManagerFactory(persistenceUnitName);
    }

    public void closeEntityManagerFactory() {
        if (this.emf != null) {
            this.emf.close();
        }
    }
}
