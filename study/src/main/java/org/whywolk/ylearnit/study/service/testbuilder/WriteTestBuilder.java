/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.service.testbuilder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.whywolk.ylearnit.study.domain.*;
import org.whywolk.ylearnit.study.service.CardUtils;

import java.util.*;

public class WriteTestBuilder extends TestBuilder {
    private static final Logger log = LogManager.getLogger(WriteTestBuilder.class);
    private final Random random;

    public WriteTestBuilder() {
        this.random = new Random();
    }

    @Override
    public String getType() {
        return "WRITE";
    }

    @Override
    public StudyTest buildTest(Card card) {
        Map<String, List<Side>> splited = CardUtils.splitByLang(card.getTestSides());
        StudyTest test = new StudyTest();

        String q = "";
        for (String lang : splited.keySet()) {
            if (splited.get(lang).size() == 1) {
                q = lang;
            }
        }
        StringJoiner stringJoiner = new StringJoiner("\n");
        splited.get(q).forEach(side -> stringJoiner.add(side.getContent()));
        Side question = new Side("", stringJoiner.toString(), ContentType.TEXT_PLAIN, q, SideType.QUESTION);
        splited.remove(q);

        String a = CardUtils.getRandomKey(splited.keySet(), this.random);
        Side answer = CardUtils.getRandomSide(splited.get(a), this.random);
        answer.setSideType(SideType.ANSWER);
        answer.getVariants().clear();
        CardUtils.formatContent(answer);
        splited.remove(a);

        test.setQuestion(question);
        test.addAnswer(answer);

        for (Side alternative : CardUtils.getAlternatives(answer, card.getTestSides())) {
            alternative.setSideType(SideType.ANSWER);
            alternative.getVariants().clear();
            CardUtils.formatContent(alternative);
            if (!test.getAnswers().contains(alternative)) {
                test.addAnswer(alternative);
            }
        }

        return test;
    }
}
