package org.whywolk.ylearnit.study;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.multibindings.ProvidesIntoMap;
import com.google.inject.multibindings.StringMapKey;
import jakarta.inject.Named;
import jakarta.inject.Singleton;
import org.whywolk.ylearnit.jpa.PersistanceUnit;
import org.whywolk.ylearnit.openrussian.service.OpenRussianService;
import org.whywolk.ylearnit.security.service.UserService;
import org.whywolk.ylearnit.study.factory.CardBuilderFactory;
import org.whywolk.ylearnit.study.factory.DbProviderFactory;
import org.whywolk.ylearnit.study.service.*;
import org.whywolk.ylearnit.study.repository.*;
import org.whywolk.ylearnit.study.service.provider.DbProvider;
import org.whywolk.ylearnit.study.service.provider.OpenRussianDbProvider;
import org.whywolk.ylearnit.study.service.provider.XmlDbProvider;

import java.util.Map;

public class StudyModule extends AbstractModule {

    // -----Services-----

    @Provides @Singleton
    public StudyService provideStudyService(StudyRepository studyRepository, DbProviderFactory dbProviderFactory,
                                            CardBuilderFactory cardBuilderFactory, UserService userService) {
        StudyService studyService = new StudyService(studyRepository, dbProviderFactory, cardBuilderFactory);
        userService.addListener(studyService);
        return studyService;
    }

    @Provides @Singleton
    public StatsService provideStatsService(StudyRepository studyRepository) {
        return new StatsService(studyRepository);
    }

    @Provides @Singleton
    public CourseService provideCourseService(CourseRepository courseRepository) {
        return new CourseService(courseRepository);
    }

    //-----Factories-----

    @Provides @Singleton
    public DbProviderFactory provideDbProviderFactory(Map<String, DbProvider> dbProviders) {
        DbProviderFactory dbProviderFactory = new DbProviderFactory();
        dbProviders.forEach((k, v) -> dbProviderFactory.put(k ,v));
        return dbProviderFactory;
    }

    @Provides @Singleton
    public CardBuilderFactory provideCardBuilderFactory(DbProviderFactory dbProviderFactory) {
        return new CardBuilderFactory(dbProviderFactory);
    }

    // -----Repositories-----

    @Provides @Singleton
    public StudyRepository provideStudyRepository(@Named("study-unit") PersistanceUnit studyUnit, CourseRepository courseRepository) {
        return new StudyRepository(studyUnit, courseRepository);
    }

    @Provides @Singleton
    public CourseRepository provideCourseRepository(@Named("study-unit") PersistanceUnit studyUnit) {
        return new CourseRepository(studyUnit);
    }

    @Provides @Singleton
    public StatsRepository provideStatsRepository(@Named("study-unit") PersistanceUnit studyUnit) {
        return new StatsRepository(studyUnit);
    }

    @Provides @Singleton @Named("study-unit")
    public PersistanceUnit provideStudyPersistanceUnit() {
        return new PersistanceUnit("study-unit");
    }

    // -----DbProviders-----

    @StringMapKey("openrussian")
    @ProvidesIntoMap @Singleton
    public DbProvider provideOpenRussianDbProvider(OpenRussianService openRussianService, StudyRepository studyRepository) {
        return new OpenRussianDbProvider(openRussianService, studyRepository);
    }

    @StringMapKey("kana")
    @ProvidesIntoMap @Singleton
    public DbProvider provideKanaXmlDbProvider() {
        return new XmlDbProvider("./db/kana.yli.xml");
    }
}
