/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.service.evaluator;

import org.whywolk.ylearnit.study.domain.UserAnswer;
import org.whywolk.ylearnit.study.entity.Answer;

public interface AnswerEvaluator {

    Answer evaluate(UserAnswer userAnswer);
}
