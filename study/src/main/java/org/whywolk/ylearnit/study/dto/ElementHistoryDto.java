/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2024.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.dto;

import org.whywolk.ylearnit.study.entity.Answer;
import org.whywolk.ylearnit.study.entity.LearningElement;
import org.whywolk.ylearnit.study.entity.Repetition;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

public class ElementHistoryDto {
    private final Map<LocalDateTime, Integer> history = new LinkedHashMap<>();
    private final LocalDate nextRepetitionDate;

    public ElementHistoryDto(LearningElement element) {
        for (Repetition repetition : element.getRepetitions()) {
            if (repetition.hasAnswer()) {
                Answer answer = repetition.getAnswer();
                history.put(answer.getDate(), answer.getRetrievability());
            }
        }
        nextRepetitionDate = element.getNextRepetition().getDate();
    }

    public Map<LocalDateTime, Integer> getHistory() {
        return history;
    }

    public LocalDate getNextRepetitionDate() {
        return nextRepetitionDate;
    }
}
