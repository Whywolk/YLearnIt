/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022-2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.service.scheduler;

import org.whywolk.ylearnit.study.domain.SM2Attributes;
import org.whywolk.ylearnit.study.entity.*;

import java.util.List;

public class SM2Scheduler implements Scheduler {
    private SM2Algorithm algorithm;

    public SM2Scheduler() {
        algorithm = new SM2Algorithm();
    }

    public String getType() {
        return "SM2";
    }

    @Override
    public Repetition initRepetition() {
        SM2Algorithm a = new SM2Algorithm();
        Repetition r = new Repetition();
        r.setInterval(a.getInterval());

        SM2Attributes sm2Attrs = new SM2Attributes(a.getEF());
        SchedulerAttributes attrs = new SchedulerAttributes(getType(), SM2Attributes.toJson(sm2Attrs));
        r.setSchedulerAttributes(attrs);

        return r;
    }

    @Override
    public void predictInterval(LearningElement element) {
        Repetition lastRep = element.getNextRepetition();
        Answer answer = lastRep.getAnswer();
        SM2Attributes lastAttrs = SM2Attributes.fromJson(lastRep.getSchedulerAttributes().getAttributes());
        int quality = matchQuality(answer.getRetrievability());

        algorithm.setInterval(lastRep.getInterval());
        algorithm.setEF(lastAttrs.getEasinessFactor());
        algorithm.calcInterval(quality);

        Repetition nextRep = new Repetition();
        if (quality > 2) {
            nextRep.setLoop(lastRep.getLoop());
            nextRep.setSerialNumber(lastRep.getSerialNumber() + 1);
        } else {
            nextRep.setLoop(lastRep.getLoop() + 1);
            nextRep.setSerialNumber(1);
        }
        nextRep.setInterval(algorithm.getInterval());
        nextRep.setDate(answer.getDate().plusDays(nextRep.getInterval()).toLocalDate());

        SM2Attributes sm2Attrs = new SM2Attributes(algorithm.getEF());
        SchedulerAttributes attrs = new SchedulerAttributes(getType(), SM2Attributes.toJson(sm2Attrs));
        nextRep.setSchedulerAttributes(attrs);

        element.addRepetition(nextRep);
    }

    @Override
    public void predictIntervals(List<LearningElement> elements) {
        for (LearningElement element : elements) {
            predictInterval(element);
        }
    }

    public int matchQuality(Integer recall) {
        if (recall > 93) {
            return 5;
        }
        if (recall > 80) {
            return 4;
        }
        if (recall > 60) {
            return 3;
        }
        if (recall > 35) {
            return 2;
        }
        if (recall > 15) {
            return 1;
        }
        return 0;
    }
}
