/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.dto;

import org.whywolk.ylearnit.study.domain.Card;
import org.whywolk.ylearnit.study.entity.LearningElement;

public class DetailedLearningCardDto {
    private DetailedLearningElementDto learningElement;
    private CardDto card;

    public DetailedLearningCardDto(LearningElement learningElement, Card card) {
        this.learningElement = new DetailedLearningElementDto(learningElement);
        this.card = new CardDto(card);
    }

    public DetailedLearningElementDto getLearningElement() {
        return learningElement;
    }

    public void setLearningElement(DetailedLearningElementDto learningElement) {
        this.learningElement = learningElement;
    }

    public CardDto getCard() {
        return card;
    }

    public void setCard(CardDto card) {
        this.card = card;
    }
}
