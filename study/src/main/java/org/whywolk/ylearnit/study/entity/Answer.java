/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022-2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.entity;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Objects;

@Entity
@Table(name = "ANSWER")
public class Answer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "ACCURACY")
    private Integer accuracy;

    @Column(name = "RETRIEVABILITY")
    private Integer retrievability;

    @Column(name = "THINKING_TIME")
    private Integer thinkingTime;

    @Column(name = "TOTAL_TIME")
    private Integer totalTime;

    @Column(name = "REP_DATE", columnDefinition = "TIMESTAMP")
    private LocalDateTime date;

    @OneToOne
    @JoinColumn(name = "REPETITION_ID")
    private Repetition repetition;

    @Column(name = "ANSWER_TYPE")
    private String type;

    @Column(name = "DATA")
    private String data;

    public Answer() {
        accuracy = 0;
        retrievability = 0;
        thinkingTime = 0;
        totalTime = 0;
        date = LocalDateTime.now(ZoneOffset.UTC);
        data = "";
    }

    public Integer getId() {
        return id;
    }

    public Integer getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Integer accuracy) {
        this.accuracy = accuracy;
    }

    public Integer getRetrievability() {
        return retrievability;
    }

    public void setRetrievability(Integer retrievability) {
        this.retrievability = retrievability;
    }

    public Integer getThinkingTime() {
        return thinkingTime;
    }

    public void setThinkingTime(Integer thinkingTime) {
        this.thinkingTime = thinkingTime;
    }

    public Integer getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(Integer totalTime) {
        this.totalTime = totalTime;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public void setRepetition(Repetition repetition) {
        this.repetition = repetition;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Answer{" +
                "id=" + id +
                ", accuracy=" + accuracy +
                ", retrievability=" + retrievability +
                ", thinkingTime=" + thinkingTime +
                ", totalTime=" + totalTime +
                ", date=" + date +
                ", type='" + type + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Answer answer = (Answer) o;
        return accuracy.equals(answer.accuracy)
                && retrievability.equals(answer.retrievability)
                && Objects.equals(thinkingTime, answer.thinkingTime)
                && Objects.equals(totalTime, answer.totalTime)
                && date.equals(answer.date)
                && type.equals(answer.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accuracy, retrievability, thinkingTime, totalTime, date, type);
    }
}
