/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "DB_ELEMENT")
public class DbElement implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @NotNull
    @Column(name = "DB_NAME", length = 20)
    private String dbName;

    @NotNull
    @Column(name = "EL_ID_DB")
    private Integer elementIdDb;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "dbElement", cascade = CascadeType.ALL)
    private Set<Element> elements = new HashSet<>();

    public DbElement() {
    }

    public DbElement(String dbName, Integer elementIdDb) {
        this.elementIdDb = elementIdDb;
        this.dbName = dbName;
    }

    public Integer getId() {
        return id;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public Integer getElementIdDb() {
        return elementIdDb;
    }

    public void setElementIdDb(Integer elementIdDb) {
        this.elementIdDb = elementIdDb;
    }

    public Set<Element> getElements() {
        return elements;
    }

    public void setElements(Set<Element> elements) {
        elements.forEach(e -> addElement(e));
    }

    public void addElement(Element element) {
        element.setDbElement(this);
        this.elements.add(element);
    }

    @Override
    public String toString() {
        return "DbElement{" +
                "id=" + id +
                ", dbName='" + dbName + '\'' +
                ", elementIdDb=" + elementIdDb +
                ", elements=" + elements +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DbElement dbElement = (DbElement) o;
        return dbName.equals(dbElement.dbName)
                && elementIdDb.equals(dbElement.elementIdDb);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dbName, elementIdDb);
    }
}
