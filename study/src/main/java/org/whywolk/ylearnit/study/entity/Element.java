/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022-2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "ELEMENT")
public class Element implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "TARGET_LANG", length = 20)
    private String targetLang;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "ELEMENT_SOURCE_LANGS", joinColumns = @JoinColumn(name = "ELEMENT_ID"))
    @Column(name = "SOURCE_LANGS")
    private Set<String> sourceLangs;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "DB_ELEMENT_ID")
    private DbElement dbElement;

    // Only for cascade remove
    @OneToMany(mappedBy = "element", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private Set<TopicElement> topicElements = new HashSet<>();

    public Element() {
        this.targetLang = "";
        this.sourceLangs = new HashSet<>();
    }

    public Element(String targetLang, Set<String> sourceLangs) {
        this.targetLang = targetLang;
        this.sourceLangs = sourceLangs;
    }

    public Element(String targetLang, Set<String> sourceLangs, DbElement dbElement) {
        this(targetLang, sourceLangs);
        this.dbElement = dbElement;
    }

    public Integer getId() {
        return id;
    }

    public String getTargetLang() {
        return targetLang;
    }

    public void setTargetLang(String targetLang) {
        this.targetLang = targetLang;
    }

    public Set<String> getSourceLangs() {
        return sourceLangs;
    }

    public void setSourceLangs(Set<String> sourceLangs) {
        this.sourceLangs = sourceLangs;
    }

    public void addSourceLang(String sourceLang) {
        sourceLangs.add(sourceLang);
    }

    public void removeSourceLang(String sourceLang) {
        sourceLangs.remove(sourceLang);
    }

    public DbElement getDbElement() {
        return dbElement;
    }

    public void setDbElement(DbElement dbElement) {
        this.dbElement = dbElement;
    }

    @Override
    public String toString() {
        return "Element{" +
                "id=" + id +
                ", targetLang='" + targetLang + '\'' +
                ", sourceLangs=" + sourceLangs +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Element element = (Element) o;
        return targetLang.equals(element.targetLang)
                && sourceLangs.equals(element.sourceLangs)
                && dbElement.equals(element.dbElement);
    }

    @Override
    public int hashCode() {
        return Objects.hash(targetLang, sourceLangs, dbElement);
    }
}
