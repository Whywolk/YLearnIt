package org.whywolk.ylearnit.study.domain;

import java.util.List;

public class ReviewParams {
    private String targetLang;
    private String sourceLang;
    private String testType;
    private boolean shuffle;
    private String courseTitle;
    private List<String> topicTitles;

    public ReviewParams() {
    }

    public String getTargetLang() {
        return targetLang;
    }

    public void setTargetLang(String targetLang) {
        this.targetLang = targetLang;
    }

    public String getSourceLang() {
        return sourceLang;
    }

    public void setSourceLang(String sourceLang) {
        this.sourceLang = sourceLang;
    }

    public String getTestType() {
        return testType;
    }

    public void setTestType(String testType) {
        this.testType = testType;
    }

    public boolean isShuffle() {
        return shuffle;
    }

    public void setShuffle(boolean shuffle) {
        this.shuffle = shuffle;
    }

    public String getCourseTitle() {
        return courseTitle;
    }

    public void setCourseTitle(String courseTitle) {
        this.courseTitle = courseTitle;
    }

    public List<String> getTopicTitles() {
        return topicTitles;
    }

    public void setTopicTitles(List<String> topicTitles) {
        this.topicTitles = topicTitles;
    }
}
