/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022-2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.service.cardbuilder;

import org.whywolk.ylearnit.japanese.entity.kana.Kana;
import org.whywolk.ylearnit.japanese.entity.kanji.Kanji;
import org.whywolk.ylearnit.japanese.service.KanaService;
import org.whywolk.ylearnit.japanese.service.KanjiService;
import org.whywolk.ylearnit.study.domain.Card;
import org.whywolk.ylearnit.study.entity.DbElement;
import org.whywolk.ylearnit.study.entity.Element;
import org.whywolk.ylearnit.study.domain.Side;

import java.util.ArrayList;
import java.util.List;

public class JapaneseCardBuilder extends CardBuilder {
    private KanaService kanaService = KanaService.getInstance();
    private KanjiService kanjiService = KanjiService.getInstance();

    public JapaneseCardBuilder() {
    }

    @Override
    public String getDbName() {
        return "";
    }

    @Override
    public List<Card> getCards(List<DbElement> elements) {
        return null;
    }

    @Override
    public List<Card> getCards(List<Element> elements, String sourceLang) {
        // TODO: fetch user settings (language for kana transliteration for example)
        // TODO: it will be better to create map of idxs
        List<Card> cards = new ArrayList<>();

        for (Element el : elements) {
            Card card;
            switch (el.getDbElement().getDbName()) {
                case "kana" -> {
                    Kana k = kanaService.getById(el.getDbElement().getElementIdDb());
                    card = buildCard(k);
                }
                case "kanjidic" -> {
                    Kanji k = kanjiService.getById(el.getDbElement().getElementIdDb());
                    card = buildCard(k);
                }
                default -> throw new IllegalStateException("Unexpected database: " + el.getDbElement().getDbName());
            }
            if (card != null) {
                card.setElementIdDb(el.getId());
                cards.add(card);
            }
        }

        return cards;
    }

    @Override
    public List<Card> getCards(List<DbElement> elements, String targetLang, String sourceLang) {
        return null;
    }



    private Card buildCard(Kana k) {
        Card c = new Card();
        List<Side> sides = new ArrayList<>();
        // TODO: sides
//        Side literal = new Side(0, k.getLiteral(), Side.ContentType.TEXT_PLAIN, Side.SideType.TEST);
//        Side transliteration = new Side(1, k.getTransliteration(l.getCountry()).getValue(),
//                Side.ContentType.TEXT_PLAIN, Side.SideType.TEST);
//        sides.add(literal);
//        sides.add(transliteration);
//        c.setSides(sides);

        return c;
    }

    private Card buildCard(Kanji k) {
        Card c = new Card();

        return c;
    }
}
