/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "LEARNING_COURSE")
public class LearningCourse implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "COURSE_ID", referencedColumnName = "ID")
    private Course course;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "learningCourse", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<LearningTopic> topics;

    @Column(name = "STUDYING")
    private Boolean studying;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "USER_DATA_ID")
    private UserData userData;

    @NotNull
    @Column(name = "SETTINGS")
    private String settings;

    public LearningCourse() {
        this.studying = true;
        this.topics = new HashSet<>();
    }

    public LearningCourse(Course course, UserData userData) {
        this();
        this.course = course;
        this.userData = userData;
    }

    public Integer getId() {
        return id;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Set<LearningTopic> getTopics() {
        return topics;
    }

    public void addTopic(LearningTopic topic) {
        topic.setLearningCourse(this);
        this.topics.add(topic);
    }

    public LearningTopic getTopic(String title) {
        for (LearningTopic lt : topics) {
            if (lt.getTopic().getTitle().equals(title)) {
                return lt;
            }
        }
        throw new RuntimeException("Topic '" + title + "' not found");
    }

    public boolean containsTopic(Topic topic) {
        return topics.stream().anyMatch(lt -> lt.getTopic().equals(topic));
    }

    public boolean containsTopic(String title) {
        return topics.stream().anyMatch(lt -> lt.getTopic().getTitle().equals(title));
    }

    public Boolean isStudying() {
        return studying;
    }

    public void setStudying(Boolean studying) {
        this.studying = studying;
    }

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }

    public String getSettings() {
        return settings;
    }

    public void setSettings(String settings) {
        this.settings = settings;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LearningCourse that = (LearningCourse) o;
        return course.equals(that.course)
                && Objects.equals(topics, that.topics)
                && studying.equals(that.studying)
                && userData.equals(that.userData)
                && settings.equals(that.settings);
    }

    @Override
    public int hashCode() {
        return Objects.hash(course, topics, studying, userData, settings);
    }
}
