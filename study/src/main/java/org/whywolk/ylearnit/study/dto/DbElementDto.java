/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.dto;

import org.whywolk.ylearnit.study.entity.DbElement;

public class DbElementDto {
    private Integer id;
    private Integer elementIdDb;
    private String dbName;

    public DbElementDto(DbElement dbElement) {
        this.id = dbElement.getId();
        this.elementIdDb = dbElement.getElementIdDb();
        this.dbName = dbElement.getDbName();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getElementIdDb() {
        return elementIdDb;
    }

    public void setElementIdDb(Integer elementIdDb) {
        this.elementIdDb = elementIdDb;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }
}
