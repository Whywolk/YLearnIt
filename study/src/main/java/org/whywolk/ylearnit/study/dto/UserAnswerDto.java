/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.dto;

import org.whywolk.ylearnit.study.domain.Card;
import org.whywolk.ylearnit.study.domain.UserAnswer;

import java.util.ArrayList;
import java.util.List;

public class UserAnswerDto {
    private List<SideDto> testAnswers;
    private SideDto userAnswer;
    private String type;
    private Integer thinkingTime;
    private Integer totalTime;
    private String dbName;
    private Integer elementIdDb;
    private Integer learningElementId;
    private Integer repetitionId;
    private String data;

    public UserAnswerDto() {
        testAnswers = new ArrayList<>();
    }

    public UserAnswer toModel() {
        Card card = new Card(dbName, elementIdDb);
        testAnswers.forEach(side -> card.addSide(side.toModel()));
        card.addSide(this.userAnswer.toModel());

        UserAnswer userAnswer = new UserAnswer(card, type, thinkingTime, totalTime, learningElementId, repetitionId);
        userAnswer.setData(this.data);
        return userAnswer;
    }

    public List<SideDto> getTestAnswers() {
        return testAnswers;
    }

    public void setTestAnswers(List<SideDto> testAnswers) {
        this.testAnswers = testAnswers;
    }

    public void addAnswer(SideDto testAnswer) {
        this.testAnswers.add(testAnswer);
    }

    public SideDto getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(SideDto userAnswer) {
        this.userAnswer = userAnswer;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getThinkingTime() {
        return thinkingTime;
    }

    public void setThinkingTime(Integer thinkingTime) {
        this.thinkingTime = thinkingTime;
    }

    public Integer getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(Integer totalTime) {
        this.totalTime = totalTime;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public Integer getElementIdDb() {
        return elementIdDb;
    }

    public void setElementIdDb(Integer elementIdDb) {
        this.elementIdDb = elementIdDb;
    }

    public Integer getLearningElementId() {
        return learningElementId;
    }

    public void setLearningElementId(Integer learningElementId) {
        this.learningElementId = learningElementId;
    }

    public Integer getRepetitionId() {
        return repetitionId;
    }

    public void setRepetitionId(Integer repetitionId) {
        this.repetitionId = repetitionId;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
