/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2024.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.domain;

public enum SideType {
    TEST("T"),
    INFO("I"),
    SERVICE("SRV"),
    QUESTION("Q"),
    ANSWER("A"),
    USER_ANSWER("U");

    private final String code;

    SideType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
