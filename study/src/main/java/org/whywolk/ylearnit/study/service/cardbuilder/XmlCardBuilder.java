package org.whywolk.ylearnit.study.service.cardbuilder;

import org.whywolk.ylearnit.study.dao.XmlCardDao;
import org.whywolk.ylearnit.study.domain.Card;
import org.whywolk.ylearnit.study.entity.DbElement;
import org.whywolk.ylearnit.study.entity.Element;

import java.util.List;
import java.util.stream.Collectors;

public class XmlCardBuilder extends CardBuilder {
    private final XmlCardDao cardDao;

    public XmlCardBuilder(XmlCardDao cardDao) {
        this.cardDao = cardDao;
    }

    @Override
    public String getDbName() {
        return cardDao.getDbName();
    }

    @Override
    public List<Card> getCards(List<DbElement> elements) {
        List<Integer> ids = getIds(elements);
        List<Card> cards = ids.stream()
                .map(id -> cardDao.getById(id))
                .collect(Collectors.toList());
        return cards;
    }

    @Override
    public List<Card> getCards(List<Element> elements, String sourceLang) {
        List<Integer> ids = getIds(elements, sourceLang);
        List<Card> cards = ids.stream()
                .map(id -> cardDao.getById(id))
                .peek(card -> card.filter(elements.get(0).getTargetLang(), sourceLang))
                .collect(Collectors.toList());
        return cards;
    }

    @Override
    public List<Card> getCards(List<DbElement> elements, String targetLang, String sourceLang) {
        List<Integer> ids = getIds(elements, targetLang, sourceLang);
        List<Card> cards = ids.stream()
                .map(id -> cardDao.getById(id))
                .peek(card -> card.filter(targetLang, sourceLang))
                .collect(Collectors.toList());;
        return cards;
    }
}
