/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.dto;

import org.whywolk.ylearnit.study.domain.ContentType;
import org.whywolk.ylearnit.study.domain.Side;
import org.whywolk.ylearnit.study.domain.SideType;

import java.util.Set;

public class SideDto {
    private String tag;
    private String content;
    private ContentType contentType;
    private String lang;
    private SideType sideType;
    private Set<String> variants;

    public SideDto() {
    }

    public SideDto(Side side) {
        this.tag = side.getTag();
        this.content = side.getContent();
        this.contentType = side.getContentType();
        this.lang = side.getLang();
        this.sideType = side.getSideType();
        this.variants = side.getVariants();
    }

    public Side toModel() {
        return new Side(tag, content, contentType, lang, sideType);
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ContentType getContentType() {
        return contentType;
    }

    public void setContentType(ContentType contentType) {
        this.contentType = contentType;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public SideType getSideType() {
        return sideType;
    }

    public void setSideType(SideType sideType) {
        this.sideType = sideType;
    }

    public Set<String> getVariants() {
        return variants;
    }

    public void setVariants(Set<String> variants) {
        this.variants = variants;
    }
}
