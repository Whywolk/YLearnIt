/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.whywolk.ylearnit.study.entity.Course;
import org.whywolk.ylearnit.study.entity.Topic;
import org.whywolk.ylearnit.study.entity.TopicElement;
import org.whywolk.ylearnit.study.repository.CourseRepository;

import java.util.List;

public class CourseService {
    private static final Logger log = LogManager.getLogger(StudyService.class);
    private final CourseRepository repository;

    public CourseService(CourseRepository repository) {
        this.repository = repository;
    }

    public Course createCourse(Course course) {
        return repository.createCourse(course);
    }

    public Course updateCourse(Course oldCourse, Course newCourse) {
        oldCourse.setTitle(newCourse.getTitle());
        oldCourse.setDescription(newCourse.getDescription());
//            course.setTargetLang(newCourse.getTargetLang());
//            course.setSourceLang(newCourse.getSourceLang());
        oldCourse.setDifficult(newCourse.getDifficult());
        return repository.updateCourse(oldCourse);
    }

    public Course updateCourse(Course course) {
        return repository.updateCourse(course);
    }

    public void deleteCourse(Course course) {
        repository.deleteCourse(course.getId());
    }

    public Course getCourse(Integer id) {
        return repository.getCourse(id);
    }

    public Course getCourse(String title, String targetLang, String sourceLang) {
        return repository.getCourse(title, targetLang, sourceLang);
    }

    public List<Course> getCourses(String targetLang) {
        return repository.getCourses(targetLang);
    }

    public List<Course> getCourses(String targetLang, String sourceLang) {
        return repository.getCourses(targetLang, sourceLang);
    }


    public Topic createTopic(Topic topic) {
        return repository.createTopic(topic);
    }

    public Topic updateTopic(Topic oldTopic, Topic newTopic) {
        oldTopic.setTitle(newTopic.getTitle());
        oldTopic.setDescription(newTopic.getDescription());
        oldTopic.setNumber(newTopic.getNumber());
        oldTopic.setDifficult(newTopic.getDifficult());
        return repository.updateTopic(oldTopic);
    }

    public void deleteTopic(Topic topic) {
        repository.deleteTopic(topic.getId());
    }

    public Topic getTopic(Integer id) {
        return repository.getTopic(id);
    }

    public Topic getTopic(String courseTitle, String topicTitle, String targetLang, String sourceLang) {
        Course course = repository.getCourse(courseTitle, targetLang, sourceLang);
        return course.getTopic(topicTitle);
    }


    public void updateTopicElements(Topic topic, List<TopicElement> elements) {
        repository.updateTopicElements(topic, elements);
    }

    public void deleteTopicElements(Integer topicId, List<Integer> elementIds) {
        repository.deleteTopicElements(topicId, elementIds);
    }

}
