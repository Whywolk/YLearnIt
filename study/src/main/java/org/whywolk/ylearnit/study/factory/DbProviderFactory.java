/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.factory;

import org.whywolk.ylearnit.study.service.provider.DbProvider;

import java.util.HashMap;
import java.util.Map;

public class DbProviderFactory {
    private final Map<String, DbProvider> providers = new HashMap<>();

    public DbProvider get(String name) {
        DbProvider provider = providers.get(name);
        if (provider != null) {
            return provider;
        }
        String msg = "DbProvider='" + name + "' not found";
        throw new RuntimeException(msg);
    }

    public void put(String name, DbProvider provider) {
        this.providers.put(name, provider);
    }
}
