/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.dto;

import org.whywolk.ylearnit.study.entity.Topic;

public class TopicUserDto {
    private Integer id;
    private String title;
    private String description;
    private Integer number;
    private Integer difficult;
    private Boolean isAdded;
    private Integer lTopicId;

    public TopicUserDto(Topic t) {
        this.id = t.getId();
        this.title = t.getTitle();
        this.description = t.getDescription();
        this.number = t.getNumber();
        this.difficult = t.getDifficult();
        this.isAdded = false;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getDifficult() {
        return difficult;
    }

    public void setDifficult(Integer difficult) {
        this.difficult = difficult;
    }

    public Boolean getAdded() {
        return isAdded;
    }

    public void setAdded(Boolean added) {
        isAdded = added;
    }

    public Integer getlTopicId() {
        return lTopicId;
    }

    public void setlTopicId(Integer lTopicId) {
        this.lTopicId = lTopicId;
    }
}
