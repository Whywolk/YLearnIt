/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import org.whywolk.ylearnit.study.entity.Answer;

import java.time.LocalDateTime;

public class AnswerDto {
    private Integer id;
    private Integer accuracy;
    private Integer retrievability;
    private Integer idleSeconds;
    private Integer totalSeconds;
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss'Z'")
    private LocalDateTime date;

    public AnswerDto(Answer answer) {
        this.id = answer.getId();
        this.accuracy = answer.getAccuracy();
        this.retrievability = answer.getRetrievability();
        this.idleSeconds = answer.getThinkingTime();
        this.totalSeconds = answer.getTotalTime();
        this.date = answer.getDate();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Integer accuracy) {
        this.accuracy = accuracy;
    }

    public Integer getRetrievability() {
        return retrievability;
    }

    public void setRetrievability(Integer retrievability) {
        this.retrievability = retrievability;
    }

    public Integer getIdleSeconds() {
        return idleSeconds;
    }

    public void setIdleSeconds(Integer idleSeconds) {
        this.idleSeconds = idleSeconds;
    }

    public Integer getTotalSeconds() {
        return totalSeconds;
    }

    public void setTotalSeconds(Integer totalSeconds) {
        this.totalSeconds = totalSeconds;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }
}
