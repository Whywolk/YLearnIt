/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.dto;

import org.whywolk.ylearnit.study.domain.Card;

import java.util.List;

public class CardDto {
    private String dbName;
    private Integer elementIdDb;
    private List<SideDto> sides;

    public CardDto(Card card) {
        this.dbName = card.getDbName();
        this.elementIdDb = card.getElementIdDb();
        this.sides = card.getSides().stream().map(side -> new SideDto(side)).toList();
    }

    public List<SideDto> getSides() {
        return sides;
    }

    public void setSides(List<SideDto> sides) {
        this.sides = sides;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public Integer getElementIdDb() {
        return elementIdDb;
    }

    public void setElementIdDb(Integer elementIdDb) {
        this.elementIdDb = elementIdDb;
    }
}
