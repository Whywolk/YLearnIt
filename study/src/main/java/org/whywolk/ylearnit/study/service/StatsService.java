/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2024.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.service;

import org.whywolk.ylearnit.study.dto.ElementHistoryDto;
import org.whywolk.ylearnit.study.dto.HistoryDto;
import org.whywolk.ylearnit.study.entity.LearningCourse;
import org.whywolk.ylearnit.study.entity.LearningElement;
import org.whywolk.ylearnit.study.entity.LearningTopic;
import org.whywolk.ylearnit.study.repository.StudyRepository;
import org.whywolk.ylearnit.utils.LocalDateRange;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class StatsService {
    private final StudyRepository repository;

    public StatsService(StudyRepository repository) {
        this.repository = repository;
    }

    public ElementHistoryDto getHistory(LearningElement element) {
        return new ElementHistoryDto(element);
    }

    public HistoryDto getHistory(LearningTopic topic, LocalDateRange range) {
        List<LearningElement> elements = repository.getLearningElements(topic);
        return new HistoryDto(elements, range);
    }

    public HistoryDto getHistory(LearningCourse course, LocalDateRange range) {
        Set<LearningElement> allElements = new HashSet<>();
        for (LearningTopic topic : course.getTopics()) {
            allElements.addAll(repository.getLearningElements(topic));
        }

        return new HistoryDto(allElements.stream().toList(), range);
    }

//    public void getStats()
}
