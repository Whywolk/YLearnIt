/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022-2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.Serializable;
import java.util.Objects;

public class SM2Attributes implements Serializable {
    private Integer easinessFactor;

    public SM2Attributes() {
        easinessFactor = 2500;
    }

    public SM2Attributes(Integer easinessFactor) {
        this.easinessFactor = easinessFactor;
    }

    @JsonProperty("ef")
    public Integer getEasinessFactor() {
        return easinessFactor;
    }

    @JsonProperty("ef")
    public void setEasinessFactor(Integer easinessFactor) {
        this.easinessFactor = easinessFactor;
    }

    public static String toJson(SM2Attributes attrs) {
        try {
            ObjectMapper om = new ObjectMapper();
            return om.writeValueAsString(attrs);
        } catch (JsonProcessingException e) {
            return "";
        }
    }

    public static SM2Attributes fromJson(String json) {
        try {
            ObjectMapper om = new ObjectMapper();
            return om.readValue(json, SM2Attributes.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String toString() {
        return "SM2Attributes{" +
                "easinessFactor=" + easinessFactor +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SM2Attributes that = (SM2Attributes) o;
        return Objects.equals(easinessFactor, that.easinessFactor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(easinessFactor);
    }
}
