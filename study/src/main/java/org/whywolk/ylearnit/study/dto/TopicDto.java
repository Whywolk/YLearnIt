/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.whywolk.ylearnit.study.entity.Topic;

public class TopicDto {
    private Integer id;
    private String title;
    private String description;
    private Integer number;
    private Integer difficult;

    public TopicDto() {
        this.title = "";
        this.description = "";
        this.number = 1;
        this.difficult = 1;
    }

    public TopicDto(Topic topic) {
        this.id = topic.getId();
        this.title = topic.getTitle();
        this.description = topic.getDescription();
        this.number = topic.getNumber();
        this.difficult = topic.getDifficult();
    }

    public Topic toModel() {
        Topic topic = new Topic();
        topic.setId(id);
        topic.setTitle(title);
        topic.setDescription(description);
        topic.setNumber(number);
        topic.setDifficult(difficult);
        return topic;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("num")
    public Integer getNumber() {
        return number;
    }

    @JsonProperty("num")
    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getDifficult() {
        return difficult;
    }

    public void setDifficult(Integer difficult) {
        this.difficult = difficult;
    }
}
