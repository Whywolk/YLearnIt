/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.factory;

import org.whywolk.ylearnit.study.service.testbuilder.SM2TestBuilder;
import org.whywolk.ylearnit.study.service.testbuilder.TestBuilder;
import org.whywolk.ylearnit.study.service.testbuilder.WriteTestBuilder;

public class TestBuilderFactory {

    public static TestBuilder get(String name) {
        if (name.equals("SM2")) {
            return new SM2TestBuilder();
        }
        if (name.equals("WRITE")) {
            return new WriteTestBuilder();
        }
        String msg = "TestBuilder=" + "'" + name + "' not found";
        throw new RuntimeException(msg);
    }
}
