/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.factory;

import org.whywolk.ylearnit.study.service.scheduler.SM2Scheduler;
import org.whywolk.ylearnit.study.service.scheduler.Scheduler;

public class SchedulerFactory {

    public static Scheduler get(String name) {
        if (name.equals("SM2")) {
            return new SM2Scheduler();
        }
        String msg = "Scheduler=" + "'" + name + "' not found";
        throw new RuntimeException(msg);
    }
}
