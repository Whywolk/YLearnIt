/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.dto;

import org.whywolk.ylearnit.study.domain.Card;
import org.whywolk.ylearnit.study.entity.Element;

public class ElementCardDto {
    private CardDto card;
    private Integer elementId;

    public ElementCardDto(Card card, Element element) {
        this.card = new CardDto(card);
        this.elementId = element.getId();
    }

    public CardDto getCard() {
        return card;
    }

    public void setCard(CardDto cardDto) {
        this.card = cardDto;
    }

    public Integer getElementId() {
        return elementId;
    }

    public void setElementId(Integer elementId) {
        this.elementId = elementId;
    }
}
