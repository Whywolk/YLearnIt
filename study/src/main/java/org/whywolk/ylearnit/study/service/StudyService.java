/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022-2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.service;

import jakarta.persistence.NoResultException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.whywolk.ylearnit.security.domain.User;
import org.whywolk.ylearnit.security.observer.UserServiceListener;
import org.whywolk.ylearnit.study.domain.Card;
import org.whywolk.ylearnit.study.domain.LangPair;
import org.whywolk.ylearnit.study.entity.*;
import org.whywolk.ylearnit.study.factory.CardBuilderFactory;
import org.whywolk.ylearnit.study.factory.DbProviderFactory;
import org.whywolk.ylearnit.study.factory.SchedulerFactory;
import org.whywolk.ylearnit.study.repository.StudyRepository;
import org.whywolk.ylearnit.study.service.cardbuilder.CardBuilder;
import org.whywolk.ylearnit.study.service.provider.DbProvider;
import org.whywolk.ylearnit.study.service.scheduler.*;

import java.util.*;
import java.util.stream.Collectors;

public class StudyService implements UserServiceListener {
    private static final Logger log = LogManager.getLogger(StudyService.class);
    private static final String DEFAULT_SCHEDULER_TYPE = "SM2";
    private final StudyRepository repository;
    private final DbProviderFactory dbProviderFactory;
    private final CardBuilderFactory cardBuilderFactory;

    public StudyService(StudyRepository repository, DbProviderFactory dbProviderFactory,
                        CardBuilderFactory cardBuilderFactory) {
        this.repository = repository;
        this.dbProviderFactory = dbProviderFactory;
        this.cardBuilderFactory = cardBuilderFactory;
    }

    @Override
    public void onUserCreate(User user) {
        createUserData(user);
        log.info("Study UserData registered");
    }

    public UserData createUserData(User user) {
        UserData userData = new UserData(user.getLogin());
        return repository.createUserData(userData);
    }

    public void addLearningCourse(String username, String title, String targetLang, String sourceLang) {
        UserData userData = repository.getUserData(username);
        Course course = repository.getCourse(title, targetLang, sourceLang);
        addLearningCourse(userData, course);
    }

    public void addLearningCourse(UserData userData, Course course) {
        // check course is not added
        try {
            repository.getLearningCourse(userData.getName(), course);

            String msg = String.format("User='%s' Course with id=%d already added", userData.getName(), course.getId());
            log.warn(msg);
            throw new RuntimeException(msg);
        } catch (NoResultException e) {
            LearningCourse lc = new LearningCourse(course, userData);
            lc = repository.createLearningCourse(lc);
            if (! course.getTopics().isEmpty()) {
                addTopicForLearningCourse(lc, course.getTopics().iterator().next());
            }

            log.info(String.format("User='%s' Course with id=%d added", userData.getName(), course.getId()));
        }
    }

    public void addTopicForLearningCourse(String username, String courseTitle, String targetLang, String sourceLang, String topicTitle) {
        LearningCourse learningCourse = repository.getLearningCourse(username, courseTitle, targetLang, sourceLang);
        Topic topic = learningCourse.getCourse().getTopic(topicTitle);
        addTopicForLearningCourse(learningCourse, topic);
    }

    public void addTopicForLearningCourse(LearningCourse learningCourse, Topic topic) {
        if (isTopicAddedInLearningCourse(topic, learningCourse)) {
            String msg = String.format("Topic id=%d already added in LearningCourse id=%d", topic.getId(), learningCourse.getId());
            log.warn(msg);
            throw new RuntimeException(msg);
        }

        LearningTopic lt = new LearningTopic(topic);
        learningCourse.addTopic(lt);
        learningCourse = repository.updateLearningCourse(learningCourse);

        lt = learningCourse.getTopic(topic.getTitle());
        addLearningElements(lt);
    }

    public void updateLearningTopicElements(String username, String courseTitle, String targetLang, String sourceLang, String topicTitle) {
        LearningCourse learningCourse = repository.getLearningCourse(username, courseTitle, targetLang, sourceLang);
        LearningTopic lt = learningCourse.getTopic(topicTitle);
        addLearningElements(lt);
    }

    public void addLearningElements(LearningTopic learningTopic) {
        UserData userData = learningTopic.getLearningCourse().getUserData();

        List<Element> notAddedElements = getNotAddedElementsByLearningTopic(learningTopic);

        Scheduler scheduler = SchedulerFactory.get(DEFAULT_SCHEDULER_TYPE);

        List<LearningElement> newLearningElements = notAddedElements.stream()
                .map(e -> new LearningElement(e, userData, scheduler.initRepetition()))
                .collect(Collectors.toList());

        repository.createLearningElements(newLearningElements);
    }

    public List<Element> getNotAddedElementsByLearningTopic(LearningTopic learningTopic) {
        List<LearningElement> addedLearningElements = getLearningElements(learningTopic);
        List<Element> topicElements = getElementsByTopic(learningTopic.getTopic());

        Set<Element> addedElements = addedLearningElements.stream()
                .map(le -> le.getElement())
                .collect(Collectors.toSet());

        List<Element> notAddedElements = topicElements.stream()
                .filter(e -> ! addedElements.contains(e))
                .collect(Collectors.toList());

        return notAddedElements;
    }

    public boolean isTopicAddedInLearningCourse(Topic topic, LearningCourse lCourse) {
        if (! lCourse.getCourse().containsTopic(topic)) {
            String msg = String.format("Course=%d doesn't contain Topic=%d", lCourse.getCourse().getId(), topic.getId());
            log.warn(msg);
            throw new RuntimeException(msg);
        }

        return lCourse.containsTopic(topic);
    }

    public Map<Element, Card> getCardsByElements(List<Element> elements, String sourceLang) {
        Map<String, List<Element>> splitedElements = splitElementsByDb(elements);
        Map<Element, Card> allCards = new HashMap<>();

        for (String dbName : splitedElements.keySet()) {
            CardBuilder cardBuilder = this.cardBuilderFactory.get(dbName);
            List<Element> elementsDb = splitedElements.get(dbName);
            Map<Integer, Element> idToElement = elementsDb.stream()
                    .collect(Collectors.toMap(e -> e.getDbElement().getElementIdDb(),e -> e));
            List<Card> cards = cardBuilder.getCards(elementsDb, sourceLang);
            cards.forEach(c -> allCards.put(idToElement.get(c.getElementIdDb()), c));
        }

        return orderCards(elements, allCards);
    }

    public Map<Element, Card> orderCards(List<Element> elements, Map<Element, Card> elementToCard) {
        Map<Element, Card> orderedCards = new LinkedHashMap<>();
        for (Element element : elements) {
            Card card = elementToCard.get(element);
            if (card != null) {
                orderedCards.put(element, card);
            }
        }
        return orderedCards;
    }

    public Map<String, List<Element>> splitElementsByDb(List<Element> elements) {
        Map<String, List<Element>> elementsByDbName = new HashMap<>();
        for (Element element : elements) {
            String dbName = element.getDbElement().getDbName();
            if (! elementsByDbName.containsKey(dbName)) {
                elementsByDbName.put(dbName, new ArrayList<>());
            }
            elementsByDbName.get(dbName).add(element);
        }
        return elementsByDbName;
    }

    public Map<Element, Card> find(String q, String targetLang, String sourceLang) {
        Set<String> dbNames = getDbNames(targetLang, sourceLang);
        Map<Element, Card> allCards = new HashMap<>();

        for (String dbName : dbNames) {
            DbProvider provider = this.dbProviderFactory.get(dbName);
            List<Card> cards = provider.find(q, targetLang, sourceLang);

            List<Integer> elementIdsDb = cards.stream()
                    .map(card -> card.getElementIdDb())
                    .collect(Collectors.toList());

            Map<DbElement, Card> dbElementMap = cards.stream()
                    .collect(Collectors.toMap(c -> new DbElement(c.getDbName(), c.getElementIdDb()), c -> c));

            List<DbElement> dbElements = repository.getDbElements(dbName, elementIdsDb);

            Map<DbElement, Element> persisted = new HashMap<>();
            dbElements.forEach(de -> {
                for (Element e : de.getElements()) {
                    if (e.getTargetLang().equals(targetLang) && e.getSourceLangs().contains(sourceLang)) {
                        persisted.put(de, e);
                    }
                }
            });

            Map<Element, Card> cardMap = persisted.keySet().stream()
                    .collect(Collectors.toMap(de -> persisted.get(de), de -> dbElementMap.get(de)));

            allCards.putAll(cardMap);
        }

        return allCards;
    }


    public List<Element> getElements(Collection<Integer> ids) {
        return repository.getElements(ids);
    }

    public List<Element> getElementsByTopic(Topic topic) {
        return repository.getElementsByTopic(topic);
    }

    public List<TopicElement> getTopicElementsByTopic(Topic topic) {
        return repository.getTopicElementsByTopic(topic);
    }

    public List<LearningElement> getLearningElements(LearningTopic lt) {
        return repository.getLearningElements(lt);
    }

    public List<LearningElement> getLearningElements(LearningTopic lt, int page) {
        return repository.getLearningElements(lt, page);
    }

    public List<LearningElement> getLearningElements(String username, Collection<Integer> ids) {
        return repository.getLearningElements(username, ids);
    }

    public LearningElement getLearningElement(String username, Integer id) {
        return repository.getLearningElement(username, id);
    }


    public LearningCourse getLearningCourse(String username, String courseTitle, String targetLang, String sourcelang) {
        return repository.getLearningCourse(username, courseTitle, targetLang, sourcelang);
    }

    public LearningCourse getLearningCourse(String username, Course course) {
        return repository.getLearningCourse(username, course);
    }

    public List<LearningCourse> getLearningCourses(String username) {
        return repository.getLearningCourses(username);
    }

    public List<LearningCourse> getLearningCourses(String username, String targetLang) {
        return repository.getLearningCourses(username, targetLang);
    }

    public List<LearningCourse> getLearningCourses(String username, String targetLang, String sourceLang) {
        return repository.getLearningCourses(username, targetLang, sourceLang);
    }


    public Set<String> getDbNames(String targetLang) {
        return repository.getDbNames(targetLang);
    }

    public Set<String> getDbNames(String targetLang, String sourceLang) {
        return repository.getDbNames(targetLang, sourceLang);
    }

    public List<LangPair> getLangPairs() {
        return repository.getLangPairs();
    }

    public List<LangPair> getLangPairs(String username) {
        return repository.getLangPairs(username);
    }
}
