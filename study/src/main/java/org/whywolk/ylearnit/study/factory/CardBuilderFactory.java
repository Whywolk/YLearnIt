/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.factory;

import org.whywolk.ylearnit.study.service.provider.DbProvider;
import org.whywolk.ylearnit.study.service.cardbuilder.CardBuilder;

public class CardBuilderFactory {
    private final DbProviderFactory dbProviderFactory;

    public CardBuilderFactory(DbProviderFactory dbProviderFactory) {
        this.dbProviderFactory = dbProviderFactory;
    }

    public CardBuilder get(String name) {
        try {
            DbProvider provider = this.dbProviderFactory.get(name);
            return provider.getCardBuilder();
        } catch (Exception e) {
            String msg = "CardBuilder='" + name + "' not found";
            throw new RuntimeException(msg);
        }
    }
}
