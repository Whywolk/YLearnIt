/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.rest.resource;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import org.whywolk.ylearnit.security.rest.Secured;
import org.whywolk.ylearnit.study.domain.Card;
import org.whywolk.ylearnit.study.dto.*;
import org.whywolk.ylearnit.study.entity.*;
import org.whywolk.ylearnit.study.service.CourseService;
import org.whywolk.ylearnit.study.service.StudyService;

import java.util.*;
import java.util.stream.Collectors;

@Tag(name = "Course resource")
@Path("/courses")
public class CourseResource {
    private final CourseService courseService;
    private final StudyService studyService;

    @Inject
    public CourseResource(CourseService courseService, StudyService studyService) {
        this.courseService = courseService;
        this.studyService = studyService;
    }

    @GET
    @Path("/{targetLang}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get courses by target and source langs")
    public List<CourseDto> getCourses(@PathParam("targetLang") String targetLang) {
        List<Course> courses = courseService.getCourses(targetLang);
        List<CourseDto> courseDtos = courses.stream().map(c -> new CourseDto(c)).toList();
        return courseDtos;
    }

    @GET
    @Path("/{targetLang}/{sourceLang}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get courses by target and source langs")
    public List<CourseDto> getCourses(@PathParam("targetLang") String targetLang,
                                      @PathParam("sourceLang") String sourceLang) {
        List<Course> courses = courseService.getCourses(targetLang, sourceLang);
        List<CourseDto> courseDtos = courses.stream().map(c -> new CourseDto(c)).toList();
        return courseDtos;
    }

    @GET
    @Path("/by-id/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get course by id")
    public CourseDto getCourse(@PathParam("id") Integer id) {
        Course course = courseService.getCourse(id);
        return new CourseDto(course);
    }

    @GET
    @Path("/{targetLang}/{sourceLang}/{course}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get course by title")
    public CourseDto getCourse(@PathParam("course") String title,
                               @PathParam("targetLang") String targetLang,
                               @PathParam("sourceLang") String sourceLang) {
        Course course = courseService.getCourse(title, targetLang, sourceLang);
        return new CourseDto(course);
    }

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Create course")
    @Secured(requiredPermissions = "admin")
    @SecurityRequirement(name = "JWT")
    public CourseDto createCourse(CourseDto courseDto) {
        Course course = courseDto.toModel();
        course = courseService.createCourse(course);
        return new CourseDto(course);
    }

    @PUT
    @Path("/{targetLang}/{sourceLang}/{course}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Update course")
    @Secured(requiredPermissions = "admin")
    @SecurityRequirement(name = "JWT")
    public CourseDto updateCourse(@PathParam("course") String title,
                                  @PathParam("targetLang") String targetLang,
                                  @PathParam("sourceLang") String sourceLang,
                                  CourseDto courseDto) {
        try {
            Course oldCourse = courseService.getCourse(title, targetLang, sourceLang);
            Course newCourse = courseDto.toModel();
            newCourse = courseService.updateCourse(oldCourse, newCourse);
            return new CourseDto(newCourse);
        } catch (Exception e) {
            return createCourse(courseDto);
        }
    }

    @DELETE
    @Path("/{targetLang}/{sourceLang}/{course}")
    @Operation(summary = "Delete course")
    @Secured(requiredPermissions = "admin")
    @SecurityRequirement(name = "JWT")
    public void deleteCourse(@PathParam("course") String title,
                             @PathParam("targetLang") String targetLang,
                             @PathParam("sourceLang") String sourceLang) {
        Course course = courseService.getCourse(title, targetLang, sourceLang);
        courseService.deleteCourse(course);
    }


    @GET
    @Path("/topic/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get topic by id")
    public TopicDto getTopic(@PathParam("id") Integer id) {
        Topic topic = courseService.getTopic(id);
        return new TopicDto(topic);
    }

    @GET
    @Path("/{targetLang}/{sourceLang}/{course}/{topic}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get topic by id")
    public TopicDto getTopic(@PathParam("course") String courseTitle,
                             @PathParam("topic") String topicTitle,
                             @PathParam("targetLang") String targetLang,
                             @PathParam("sourceLang") String sourceLang) {
        Topic topic = courseService.getTopic(courseTitle, topicTitle, targetLang, sourceLang);
        return new TopicDto(topic);
    }

    @POST
    @Path("/{targetLang}/{sourceLang}/{course}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Create topic")
    @Secured(requiredPermissions = "admin")
    @SecurityRequirement(name = "JWT")
    public CourseDto createTopic(@PathParam("course") String title,
                                 @PathParam("targetLang") String targetLang,
                                 @PathParam("sourceLang") String sourceLang,
                                 TopicDto topicDto) {
        Course course = courseService.getCourse(title, targetLang, sourceLang);
        Topic topic = topicDto.toModel();
        course.addTopic(topic);
        topic = courseService.createTopic(topic);
        return new CourseDto(topic.getCourse());
    }

    @PUT
    @Path("/{targetLang}/{sourceLang}/{course}/{topic}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Update topic")
    @Secured(requiredPermissions = "admin")
    @SecurityRequirement(name = "JWT")
    public CourseDto updateTopic(@PathParam("course") String courseTitle,
                                 @PathParam("topic") String topicTitle,
                                 @PathParam("targetLang") String targetLang,
                                 @PathParam("sourceLang") String sourceLang,
                                 TopicDto topicDto) {
        Topic oldTopic = courseService.getTopic(courseTitle, topicTitle, targetLang, sourceLang);
        Topic newTopic = topicDto.toModel();
        newTopic = courseService.updateTopic(oldTopic, newTopic);
        return new CourseDto(newTopic.getCourse());
    }

    @DELETE
    @Path("/{targetLang}/{sourceLang}/{course}/{topic}")
    @Operation(summary = "Delete topic")
    @Secured(requiredPermissions = "admin")
    @SecurityRequirement(name = "JWT")
    public CourseDto deleteTopic(@PathParam("course") String courseTitle,
                                 @PathParam("topic") String topicTitle,
                                 @PathParam("targetLang") String targetLang,
                                 @PathParam("sourceLang") String sourceLang) {
        Course course = courseService.getCourse(courseTitle, targetLang, sourceLang);
        Topic topic = course.getTopic(topicTitle);
        course.removeTopic(topic);
        course = courseService.updateCourse(course);
        return new CourseDto(course);
    }

    @GET
    @Path("/{targetLang}/{sourceLang}/{course}/{topic}/elements")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get topic elements as cards")
    public List<TopicElementCardDto> getTopicElements(@PathParam("course") String courseTitle,
                                                      @PathParam("topic") String topicTitle,
                                                      @PathParam("targetLang") String targetLang,
                                                      @PathParam("sourceLang") String sourceLang) {
        Course course = courseService.getCourse(courseTitle, targetLang, sourceLang);
        Topic topic = course.getTopic(topicTitle);

        List<TopicElement> topicElements = studyService.getTopicElementsByTopic(topic);

        Map<Element, TopicElement> topicElementMap = topicElements.stream()
                .collect(Collectors.toMap(te -> te.getElement(), te -> te));
        List<Element> elements = new ArrayList<>(topicElementMap.keySet());

        Map<Element, Card> cardMap = studyService.getCardsByElements(elements, course.getSourceLang());

        List<TopicElementCardDto> topicElementCardDtos = cardMap.keySet().stream()
                .map(e -> new TopicElementCardDto(cardMap.get(e), topicElementMap.get(e)))
                .collect(Collectors.toList());

        return topicElementCardDtos;
    }

    @PUT
    @Path("/{targetLang}/{sourceLang}/{course}/{topic}/elements")
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "Add/Update topic elements")
    @Secured(requiredPermissions = "admin")
    @SecurityRequirement(name = "JWT")
    public void updateTopicElements(@PathParam("course") String courseTitle,
                                    @PathParam("topic") String topicTitle,
                                    @PathParam("targetLang") String targetLang,
                                    @PathParam("sourceLang") String sourceLang,
                                    Set<TopicElementDto> topicElementDtos) {
        Course course = courseService.getCourse(courseTitle, targetLang, sourceLang);
        Topic topic = course.getTopic(topicTitle);

        Map<Integer, TopicElementDto> topicElementMap = topicElementDtos.stream()
                .collect(Collectors.toMap(te -> te.getElementId(), te -> te));

        List<Element> elements = studyService.getElements(topicElementMap.keySet());

        List<TopicElement> topicElements = elements.stream()
                .filter(e -> e.getTargetLang().equals(course.getTargetLang())
                        && e.getSourceLangs().contains(course.getSourceLang()))
                .map(e -> new TopicElement(topic, e, topicElementMap.get(e.getId()).getNumber()))
                .collect(Collectors.toList());

        courseService.updateTopicElements(topic, topicElements);
    }

    @DELETE
    @Path("/{targetLang}/{sourceLang}/{course}/{topic}/elements")
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "Delete topic elements")
    @Secured(requiredPermissions = "admin")
    @SecurityRequirement(name = "JWT")
    public void deleteTopicElements(@PathParam("course") String courseTitle,
                                    @PathParam("topic") String topicTitle,
                                    @PathParam("targetLang") String targetLang,
                                    @PathParam("sourceLang") String sourceLang,
                                    Set<TopicElementDto> topicElementDtos) {
        Course course = courseService.getCourse(courseTitle, targetLang, sourceLang);
        Topic topic = course.getTopic(topicTitle);

        List<Integer> elementIds = topicElementDtos.stream()
                .map(te -> te.getElementId()).collect(Collectors.toList());
        courseService.deleteTopicElements(topic.getId(), elementIds);
    }

    @GET
    @Path("/find")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Find elements and cards by target lang and source lang")
    public List<ElementCardDto> find(@QueryParam("q") String q,
                                     @QueryParam("targetLang") String targetLang,
                                     @QueryParam("sourceLang") String sourceLang) {
        Map<Element, Card> cardMap = studyService.find(q, targetLang, sourceLang);
        List<ElementCardDto> elementCardDtos = cardMap.keySet().stream()
                .map(element -> new ElementCardDto(cardMap.get(element), element))
                .collect(Collectors.toList());
        return elementCardDtos;
    }
}
