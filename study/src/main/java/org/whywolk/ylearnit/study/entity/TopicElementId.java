/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;

import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class TopicElementId implements Serializable {

    @Column(name = "TOPIC_ID")
    private Integer topicId;

    @Column(name = "ELEMENT_ID")
    private Integer elementId;

    public TopicElementId() {
    }

    public TopicElementId(Integer topicId, Integer elementId) {
        this.topicId = topicId;
        this.elementId = elementId;
    }

    public Integer getTopicId() {
        return topicId;
    }

    public void setTopicId(Integer topicId) {
        this.topicId = topicId;
    }

    public Integer getElementId() {
        return elementId;
    }

    public void setElementId(Integer elementId) {
        this.elementId = elementId;
    }

    @Override
    public String toString() {
        return "TopicElementId{" +
                "topicId=" + topicId +
                ", elementId=" + elementId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TopicElementId that = (TopicElementId) o;
        return topicId.equals(that.topicId)
                && elementId.equals(that.elementId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(topicId, elementId);
    }
}
