/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022-2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.service.evaluator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.whywolk.ylearnit.study.domain.UserAnswer;
import org.whywolk.ylearnit.study.entity.Answer;

public class SM2Evaluator implements AnswerEvaluator {
    private static final Logger log = LogManager.getLogger(SM2Evaluator.class);

    public SM2Evaluator() {
    }

    @Override
    public Answer evaluate(UserAnswer userAnswer) {
        if (! userAnswer.getType().equals("SM2")) {
            String msg = "Expected answer type='SM2', not '" + userAnswer.getType() + "'";
            log.warn(msg);
            throw new RuntimeException(msg);
        }

        Answer answer = new Answer();
        switch (userAnswer.getUserAnswer().getContent()) {
            case "5" -> {
                answer.setAccuracy(100);
                answer.setRetrievability(100);
            }
            case "4" -> {
                answer.setAccuracy(90);
                answer.setRetrievability(90);
            }
            case "3" -> {
                answer.setAccuracy(85);
                answer.setRetrievability(75);
            }
            case "2" -> {
                answer.setAccuracy(20);
                answer.setRetrievability(50);
            }
            case "1" -> {
                answer.setAccuracy(10);
                answer.setRetrievability(25);
            }
            case "0" -> {
                answer.setAccuracy(0);
                answer.setRetrievability(0);
            }
            default -> throw new IllegalStateException("Unexpected userAnswer content='" + userAnswer.getUserAnswer().getContent() + "'");
        }
        return answer;
    }
}
