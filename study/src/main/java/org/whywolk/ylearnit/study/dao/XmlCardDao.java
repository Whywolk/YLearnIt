/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2024.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.dao;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.whywolk.ylearnit.study.domain.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class XmlCardDao {
    private final File file;
    private final Document doc;
    private Map<String, Tag> tagMap = new HashMap<>();

    public XmlCardDao(File file) {
        try {
            this.file = file;
            InputStream is = new FileInputStream(this.file);
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            this.doc = builder.parse(is);

            fetchTags();
        } catch (ParserConfigurationException | IOException | SAXException e) {
            throw new RuntimeException(e);
        }
    }

    public XmlCardDao(String xmlFilePath) {
        this(new File(xmlFilePath));
    }


    public List<Card> getAll() {
        NodeList nodeList = doc.getElementsByTagName("card");
        return mapCards(nodeList);
    }

    public Card getById(int id) {
        String expr = "/db/cards/card[@id='" + id + "']";
        try {
            XPath xPath = XPathFactory.newInstance().newXPath();
            XPathExpression xPathExpr = xPath.compile(expr);
            Node node = (Node) xPathExpr.evaluate(doc, XPathConstants.NODE);
            return mapCard((Element) node);
        } catch (XPathExpressionException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Card> get(String str, String targetLang) {
        List<Tag> tags = getTags(targetLang, ContentType.TEXT_PLAIN);
        List<Card> cards = new ArrayList<>();
        for (Card card : getAll()) {
            for (Tag tag : tags) {
                if (card.equalsContent(str, tag)) {
                    cards.add(card);
                    break;
                }
            }
        }
        return cards;
    }

    public List<Card> find(String str, String targetLang) {
        List<Tag> tags = getTags(targetLang, ContentType.TEXT_PLAIN);
        List<Card> cards = new ArrayList<>();
        for (Card card : getAll()) {
            for (Tag tag : tags) {
                if (card.containsContent(str, tag)) {
                    cards.add(card);
                    break;
                }
            }
        }
        return cards;
    }

    public List<Card> find(String str, String targetLang, String sourceLang) {
        List<Card> cards = find(str, targetLang);
        cards.forEach(card -> card.filter(targetLang, sourceLang));
        return cards;
    }

    public String getDbName() {
        return doc.getDocumentElement().getAttribute("name");
    }


    private void fetchTags() {
        NodeList tags = doc.getDocumentElement().getElementsByTagName("tag");
        for (int i = 0; i < tags.getLength(); i++) {
            Element tagElement = (Element) tags.item(i);
            Tag tag = new Tag(tagElement.getAttribute("name"));
            tagMap.put(tag.getName(), tag);

            NodeList tagElements = tagElement.getChildNodes();
            for (int j = 0; j < tagElements.getLength(); j++) {
                Node n = tagElements.item(j);
                if (n.getNodeType() == Node.ELEMENT_NODE) {
                    Element e = (Element) n;
                    switch (e.getTagName()) {
                        case "lang" -> tag.setLang(e.getTextContent());
                        case "content_type" -> tag.setContentType(ContentType.valueOfCode(e.getTextContent()));
                    }
                }
            }
        }
    }

    private List<Tag> getTags(String lang) {
        return tagMap.values().stream()
                .filter(tag -> tag.getLang().equals(lang)).toList();
    }

    private List<Tag> getTags(String lang, ContentType contentType) {
        return tagMap.values().stream()
                .filter(tag -> tag.getLang().equals(lang) && tag.getContentType() == contentType).toList();
    }

    private List<Card> mapCards(NodeList nodeList) {
        List<Card> cards = new ArrayList<>();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element e = (Element) nodeList.item(i);
            Card card = mapCard(e);
            cards.add(card);
        }
        return cards;
    }

    private Card mapCard(Element e) {
        if (! e.getTagName().equals("card")) {
            throw new RuntimeException();
        }
        Card card = new Card(
                getDbName(),
                Integer.valueOf(e.getAttribute("id"))
        );

        NodeList sideNodes = e.getChildNodes();
        for (int i = 0; i < sideNodes.getLength(); i++) {
            Node sideNode = sideNodes.item(i);
            if (sideNode.getNodeType() == Node.ELEMENT_NODE) {
                Side side = mapSide((Element) sideNode);
                card.addSide(side);
            }
        }
        return card;
    }

    private Side mapSide(Element el) {
        if (! el.getTagName().equals("s")) {
            throw new RuntimeException();
        }
        Side side = new Side(
                tagMap.get(el.getAttribute("tag")),
                el.getTextContent());
        return side;
    }
}
