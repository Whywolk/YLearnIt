/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2024.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.whywolk.ylearnit.study.entity.Answer;
import org.whywolk.ylearnit.study.entity.LearningElement;
import org.whywolk.ylearnit.study.entity.Repetition;
import org.whywolk.ylearnit.utils.LocalDateRange;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HistoryDto {
    static class HistoryEntry {
        @JsonProperty("s") int success = 0;
        @JsonProperty("f") int fail = 0;
        @JsonProperty("sn") int successNew = 0;
        @JsonProperty("fn") int failNew = 0;

        public HistoryEntry() {
        }
    }

    private final Map<LocalDate, HistoryEntry> history = new HashMap<>();
    private final LocalDateRange range;

    public HistoryDto(List<LearningElement> elements, LocalDateRange range) {
        this.range = range;
        elements.forEach(e -> analyze(e));
    }

    private void analyze(LearningElement element) {
        if (element.getRepetitions().size() == 2) {
            analyzeNewElement(element);
        } else {
            analyzeOldElement(element);
        }
    }

    private void analyzeNewElement(LearningElement element) {
        Answer answer = element.getLastRepetition().getAnswer();
        LocalDate date = answer.getDate().toLocalDate();

        if (!history.containsKey(date)) {
            createHistoryEntry(date);
        }

        if (range.contains(date)) {
            if (answer.getRetrievability() >= 50) {
                incrementSuccessNew(date);
            } else {
                incrementFailNew(date);
            }
        }
    }

    private void analyzeOldElement(LearningElement element) {
        for (Repetition repetition : element.getRepetitions()) {
            if (repetition.hasAnswer()) {
                Answer answer = repetition.getAnswer();
                LocalDate date = answer.getDate().toLocalDate();

                if (! history.containsKey(date)) {
                    createHistoryEntry(date);
                }

                if (range.contains(date)) {
                    if (answer.getRetrievability() >= 50) {
                        incrementSuccess(date);
                    } else {
                        incrementFail(date);
                    }
                }
            }
        }
    }

    private void createHistoryEntry(LocalDate date) {
        history.put(date, new HistoryEntry());
    }

    private void incrementSuccess(LocalDate date) {
        history.get(date).success++;
    }

    private void incrementFail(LocalDate date) {
        history.get(date).fail++;
    }

    private void incrementSuccessNew(LocalDate date) {
        history.get(date).successNew++;
    }

    private void incrementFailNew(LocalDate date) {
        history.get(date).failNew++;
    }
}
