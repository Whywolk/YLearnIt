/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022-2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "LEARNING_ELEMENT")
public class LearningElement implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "ELEMENT_ID")
    private Element element;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private UserData userData;

    @OneToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinColumn(name = "NEXT_REP_ID", referencedColumnName = "ID")
    private Repetition nextRepetition;

    @OneToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinColumn(name = "LAST_REP_ID", referencedColumnName = "ID")
    private Repetition lastRepetition;

    @OrderBy(value = "loop ASC, serialNumber ASC")
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "learningElement", cascade = CascadeType.ALL)
    private Set<Repetition> repetitions = new LinkedHashSet<>();

    public LearningElement() {
    }

    public LearningElement(Element element, UserData userData) {
        this.element = element;
        this.userData = userData;
    }

    public LearningElement(Element element, UserData userData, Repetition repetition) {
        this.element = element;
        this.userData = userData;
        addRepetition(repetition);
    }

    public Integer getId() {
        return id;
    }

    public Element getElement() {
        return element;
    }

    public void setElement(Element element) {
        this.element = element;
    }

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }

    public Repetition getLastRepetition() {
        return lastRepetition;
    }

    public void setLastRepetition(Repetition lastRepetition) {
        lastRepetition.setLearningElement(this);
        this.lastRepetition = lastRepetition;
    }

    public Repetition getNextRepetition() {
        return nextRepetition;
    }

    public void setNextRepetition(Repetition nextRepetition) {
        this.nextRepetition = nextRepetition;
    }

    public Set<Repetition> getRepetitions() {
        return repetitions;
    }

    public void setRepetitions(Set<Repetition> repetitions) {
        this.repetitions = repetitions;
    }

    public void addRepetition(Repetition repetition) {
        if (this.repetitions.add(repetition)) {
            repetition.setLearningElement(this);
            this.lastRepetition = this.nextRepetition;
            this.nextRepetition = repetition;
        }
    }

    @Override
    public String toString() {
        return "LearningElement{" +
                "id=" + id +
                ", element=" + element +
                ", userData=" + userData +
                ", nextRepetition=" + nextRepetition +
                ", lastRepetition=" + lastRepetition +
                ", repetitions=" + repetitions +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LearningElement element1 = (LearningElement) o;
        return element.equals(element1.element)
                && userData.equals(element1.userData)
                && Objects.equals(lastRepetition, element1.lastRepetition)
                && Objects.equals(nextRepetition, element1.nextRepetition)
                && Objects.equals(repetitions, element1.repetitions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(element, userData, nextRepetition, lastRepetition, repetitions);
    }
}
