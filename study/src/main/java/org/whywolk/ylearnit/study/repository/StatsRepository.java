/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.repository;

import jakarta.persistence.EntityManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.whywolk.ylearnit.jpa.PersistanceUnit;

public class StatsRepository {
    private static final Logger log = LogManager.getLogger(StatsRepository.class);
    private static final ThreadLocal<EntityManager> threadLocal = new ThreadLocal<>();
    private final PersistanceUnit studyUnit;

    public StatsRepository(PersistanceUnit studyUnit) {
        this.studyUnit = studyUnit;
    }

    public EntityManager getEntityManager() {
        EntityManager em = threadLocal.get();
        if (em == null) {
            em = this.studyUnit.createEntityManager();
            threadLocal.set(em);
        }
        return em;
    }

    public void closeEntityManager() {
        EntityManager em = threadLocal.get();
        if (em != null) {
            em.close();
            threadLocal.set(null);
        }
    }



}
