package org.whywolk.ylearnit.study.dto;

import org.whywolk.ylearnit.study.domain.Card;
import org.whywolk.ylearnit.study.entity.TopicElement;

public class TopicElementCardDto {
    private CardDto card;
    private TopicElementDto topicElement;

    public TopicElementCardDto() {
    }

    public TopicElementCardDto(Card card, TopicElement topicElement) {
        this.card = new CardDto(card);
        this.topicElement = new TopicElementDto(topicElement);
    }

    public CardDto getCard() {
        return card;
    }

    public void setCard(CardDto card) {
        this.card = card;
    }

    public TopicElementDto getTopicElement() {
        return topicElement;
    }

    public void setTopicElement(TopicElementDto topicElement) {
        this.topicElement = topicElement;
    }
}
