/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022-2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.domain;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Side implements Cloneable {
    private String tag;
    private String content;
    private ContentType contentType;
    private String lang;
    private SideType sideType;
    private Set<String> variants;

    public Side() {
        this("", "", ContentType.TEXT_PLAIN, "", SideType.TEST);
    }

    public Side(Tag tag, String content) {
        this(tag.getName(), content, tag.getContentType(), tag.getLang(), SideType.TEST);
    }

    public Side(String tag, String content, ContentType contentType, String lang, SideType sideType) {
        this.tag = tag;
        this.content = content;
        this.contentType = contentType;
        this.lang = lang;
        this.sideType = sideType;
        this.variants = new HashSet<>();
    }

    public boolean equalsTag(Tag tag) {
        return this.tag.equals(tag.getName())
                && this.lang.equals(tag.getLang())
                && this.contentType.equals(tag.getContentType());
    }

    public boolean containsContent(String content, Tag tag) {
        return equalsTag(tag) && this.content.contains(content);
    }

    public boolean equalsContent(String content, Tag tag) {
        return equalsTag(tag) && this.content.equals(content);
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ContentType getContentType() {
        return contentType;
    }

    public void setContentType(ContentType contentType) {
        this.contentType = contentType;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public SideType getSideType() {
        return sideType;
    }

    public void setSideType(SideType sideType) {
        this.sideType = sideType;
    }

    public Set<String> getVariants() {
        return variants;
    }

    public void setVariants(Set<String> variants) {
        this.variants = variants;
    }

    public void addVariant(String variant) {
        variants.add(variant);
    }

    @Override
    public Side clone() {
        try {
            Side clone = (Side) super.clone();
            // TODO: copy mutable state here, so the clone can't change the internals of the original
            return clone;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }

    @Override
    public String toString() {
        return "Side{" +
                "tag=" + tag +
                ", content='" + content + '\'' +
                ", contentType=" + contentType +
                ", lang='" + lang + '\'' +
                ", sideType=" + sideType +
                ", variants=" + variants +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Side side = (Side) o;
        return Objects.equals(tag, side.tag)
                && Objects.equals(content, side.content)
                && contentType == side.contentType
                && Objects.equals(lang, side.lang)
                && sideType == side.sideType
                && variants.equals(side.variants);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tag, content, contentType, lang, sideType, variants);
    }
}
