/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022-2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.domain;

import java.util.ArrayList;
import java.util.List;

public class StudyTest {
    private Side question;
    private List<Side> answers;
    private String type;
    private String dbName;
    private Integer elementIdDb;
    private Integer learningElementId;
    private Integer repetitionId;

    public StudyTest() {
        this.question = new Side();
        this.answers = new ArrayList<>();
        this.type = "";
        this.dbName = "";
        this.elementIdDb = 0;
        this.learningElementId = 0;
        this.repetitionId = 0;
    }

    public Side getQuestion() {
        return question;
    }

    public void setQuestion(Side question) {
        this.question = question;
    }

    public List<Side> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Side> answers) {
        this.answers = answers;
    }

    public void addAnswer(Side answer) {
        this.answers.add(answer);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public Integer getElementIdDb() {
        return elementIdDb;
    }

    public void setElementIdDb(Integer elementIdDb) {
        this.elementIdDb = elementIdDb;
    }

    public Integer getLearningElementId() {
        return learningElementId;
    }

    public void setLearningElementId(Integer learningElementId) {
        this.learningElementId = learningElementId;
    }

    public Integer getRepetitionId() {
        return repetitionId;
    }

    public void setRepetitionId(Integer repetitionId) {
        this.repetitionId = repetitionId;
    }
}
