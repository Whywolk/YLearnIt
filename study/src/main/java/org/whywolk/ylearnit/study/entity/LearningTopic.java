/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "LEARNING_TOPIC")
public class LearningTopic implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "TOPIC_ID", referencedColumnName = "ID")
    private Topic topic;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "L_COURSE_ID")
    private LearningCourse learningCourse;

    @Column(name = "STUDYING")
    private Boolean studying;

    public LearningTopic() {
        this.studying = true;
    }

    public LearningTopic(Topic topic) {
        this();
        this.topic = topic;
    }

    public Integer getId() {
        return id;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public LearningCourse getLearningCourse() {
        return learningCourse;
    }

    public void setLearningCourse(LearningCourse learningCourse) {
        this.learningCourse = learningCourse;
    }

    public Boolean isStudying() {
        return studying;
    }

    public void setStudying(Boolean studying) {
        this.studying = studying;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LearningTopic that = (LearningTopic) o;
        return topic.equals(that.topic)
                && studying.equals(that.studying);
    }

    @Override
    public int hashCode() {
        return Objects.hash(topic, studying);
    }
}
