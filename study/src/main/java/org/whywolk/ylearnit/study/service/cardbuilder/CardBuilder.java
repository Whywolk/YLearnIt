/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022-2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.service.cardbuilder;

import org.whywolk.ylearnit.study.domain.Card;
import org.whywolk.ylearnit.study.entity.DbElement;
import org.whywolk.ylearnit.study.entity.Element;

import java.util.ArrayList;
import java.util.List;

public abstract class CardBuilder {

    public abstract String getDbName();

    public abstract List<Card> getCards(List<DbElement> elements);

    public abstract List<Card> getCards(List<Element> elements, String sourceLang);

    public abstract List<Card> getCards(List<DbElement> elements, String targetLang, String sourceLang);


    protected List<Integer> getIds(List<DbElement> elements) {
        List<Integer> idsDb = new ArrayList<>();
        for (DbElement de : elements) {
            idsDb.add(de.getElementIdDb());
        }
        return idsDb;
    }

    protected List<Integer> getIds(List<Element> elements, String sourceLang) {
        List<Integer> idsDb = new ArrayList<>();
        for (Element e : elements) {
            if (e.getDbElement().getDbName().equals(getDbName()) && e.getSourceLangs().contains(sourceLang)) {
                idsDb.add(e.getDbElement().getElementIdDb());
            }
        }
        return idsDb;
    }

    protected List<Integer> getIds(List<DbElement> elements, String targetLang, String sourceLang) {
        List<Integer> idsDb = new ArrayList<>();
        for (DbElement de : elements) {
            if (de.getDbName().equals(getDbName())) {
                for (Element e : de.getElements()) {
                    if (e.getTargetLang().equals(targetLang) && e.getSourceLangs().contains(sourceLang)) {
                        idsDb.add(de.getElementIdDb());
                        break;
                    }
                }
            }
        }
        return idsDb;
    }
}
