/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.factory;

import org.whywolk.ylearnit.study.service.evaluator.AnswerEvaluator;
import org.whywolk.ylearnit.study.service.evaluator.SM2Evaluator;
import org.whywolk.ylearnit.study.service.evaluator.WriteEvaluator;

public class AnswerEvaluatorFactory {

    public static AnswerEvaluator get(String name) {
        if (name.equals("SM2")) {
            return new SM2Evaluator();
        }
        if (name.equals("WRITE")) {
            return new WriteEvaluator();
        }
        String msg = "AnswerEvaluator=" + "'" + name + "' not found";
        throw new RuntimeException(msg);
    }
}
