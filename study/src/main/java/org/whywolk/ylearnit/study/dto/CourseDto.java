/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.dto;

import org.whywolk.ylearnit.study.entity.Course;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CourseDto {
    private Integer id;
    private String title;
    private String description;
    private String targetLang;
    private String sourceLang;
    private Integer difficult;
    private List<TopicDto> topics;

    public CourseDto() {
    }

    public CourseDto(Course course) {
        this.id = course.getId();
        this.title = course.getTitle();
        this.description = course.getDescription();
        this.targetLang = course.getTargetLang();
        this.sourceLang = course.getSourceLang();
        this.difficult = course.getDifficult();
        this.topics = course.getTopics().stream().map(topic -> new TopicDto(topic)).collect(Collectors.toList());
    }

    public Course toModel() {
        Course course = new Course();
        course.setId(id);
        course.setTitle(title);
        course.setDescription(description);
        course.setTargetLang(targetLang);
        course.setSourceLang(sourceLang);
        course.setDifficult(difficult);
        topics.forEach(topic -> course.addTopic(topic.toModel()));
        return course;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTargetLang() {
        return targetLang;
    }

    public void setTargetLang(String targetLang) {
        this.targetLang = targetLang;
    }

    public String getSourceLang() {
        return sourceLang;
    }

    public void setSourceLang(String sourceLang) {
        this.sourceLang = sourceLang;
    }

    public Integer getDifficult() {
        return difficult;
    }

    public void setDifficult(Integer difficult) {
        this.difficult = difficult;
    }

    public List<TopicDto> getTopics() {
        return topics;
    }

    public void setTopics(List<TopicDto> topics) {
        this.topics = topics;
    }
}
