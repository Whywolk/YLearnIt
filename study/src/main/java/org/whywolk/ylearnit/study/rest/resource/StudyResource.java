/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022-2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.rest.resource;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.inject.Inject;
import jakarta.persistence.NoResultException;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.SecurityContext;
import org.whywolk.ylearnit.security.dto.UserPrincipal;
import org.whywolk.ylearnit.security.rest.Secured;
import org.whywolk.ylearnit.study.domain.*;
import org.whywolk.ylearnit.study.dto.CourseUserDto;
import org.whywolk.ylearnit.study.dto.DetailedLearningCardDto;
import org.whywolk.ylearnit.study.dto.LearningCardDto;
import org.whywolk.ylearnit.study.entity.*;
import org.whywolk.ylearnit.study.service.CourseService;
import org.whywolk.ylearnit.study.service.StudyService;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Tag(name = "Study resource")
@Path("/study")
public class StudyResource {
    private final CourseService courseService;
    private final StudyService studyService;

    @Inject
    public StudyResource(CourseService courseService, StudyService studyService) {
        this.courseService = courseService;
        this.studyService = studyService;
    }

    @GET
    @Path("/stats/{targetLang}/{sourceLang}/{course}/{topic}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get statistics by learning topic")
    @Secured()
    @SecurityRequirement(name = "JWT")
    public List<LearningCardDto> getStats(@Context SecurityContext securityContext,
                                          @PathParam("course") String courseTitle,
                                          @PathParam("topic") String topicTitle,
                                          @PathParam("targetLang") String targetLang,
                                          @PathParam("sourceLang") String sourceLang,
                                          @DefaultValue("0") @QueryParam("page") int page) {
        UserPrincipal userPrincipal = (UserPrincipal) securityContext.getUserPrincipal();
        Integer userId = userPrincipal.getId();
        String username = userPrincipal.getName();

        LearningCourse learningCourse = studyService.getLearningCourse(username, courseTitle, targetLang, sourceLang);
        LearningTopic learningTopic = learningCourse.getTopic(topicTitle);

        List<LearningElement> les = studyService.getLearningElements(learningTopic, page);
        List<Element> elements = les.stream().map(le -> le.getElement()).toList();

        Map<Element, Card> cardMap = studyService.getCardsByElements(elements, sourceLang);

        List<LearningCardDto> lCardsDto = les.stream()
                .filter(le -> cardMap.get(le.getElement()) != null)
                .map(le -> new LearningCardDto(le, cardMap.get(le.getElement())))
                .collect(Collectors.toList());

        return lCardsDto;
    }

    @GET
    @Path("/stats")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get statistics by learning elements ids")
    @Secured()
    @SecurityRequirement(name = "JWT")
    public List<DetailedLearningCardDto> getStats(@Context SecurityContext securityContext,
                                                  @QueryParam("sourceLang") String sourceLang,
                                                  @QueryParam("leIds") List<Integer> leIds) {
        UserPrincipal userPrincipal = (UserPrincipal) securityContext.getUserPrincipal();
        Integer userId = userPrincipal.getId();
        String username = userPrincipal.getName();

        List<LearningElement> les = studyService.getLearningElements(username, leIds);
        List<Element> elements = les.stream().map(le -> le.getElement()).toList();

        Map<Element, Card> cardMap = studyService.getCardsByElements(elements, sourceLang);

        List<DetailedLearningCardDto> lCardsDto = les.stream()
                .filter(le -> cardMap.get(le.getElement()) != null)
                .map(le -> new DetailedLearningCardDto(le, cardMap.get(le.getElement())))
                .collect(Collectors.toList());

        return lCardsDto;
    }


    @GET
    @Path("/courses")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get courses by target lang")
    @Secured()
    @SecurityRequirement(name = "JWT")
    public List<CourseUserDto> getCourses(@Context SecurityContext securityContext,
                                          @DefaultValue("false") @QueryParam("learning") boolean learning) {
        UserPrincipal userPrincipal = (UserPrincipal) securityContext.getUserPrincipal();
        Integer userId = userPrincipal.getId();
        String username = userPrincipal.getName();

        List<LearningCourse> learningCourses = studyService.getLearningCourses(username);
        return CourseUserDto.toDto(learningCourses);
    }

    @GET
    @Path("/courses/{targetLang}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get courses by target lang")
    @Secured()
    @SecurityRequirement(name = "JWT")
    public List<CourseUserDto> getCourses(@Context SecurityContext securityContext,
                                          @PathParam("targetLang") String targetLang,
                                          @DefaultValue("false") @QueryParam("learning") boolean learning) {
        UserPrincipal userPrincipal = (UserPrincipal) securityContext.getUserPrincipal();
        Integer userId = userPrincipal.getId();
        String username = userPrincipal.getName();

        if (learning) {
            List<LearningCourse> learningCourses = studyService.getLearningCourses(username, targetLang);
            return CourseUserDto.toDto(learningCourses);
        } else {
            List<Course> courses = courseService.getCourses(targetLang);
            List<LearningCourse> learningCourses = studyService.getLearningCourses(username, targetLang);
            return CourseUserDto.toDto(courses, learningCourses);
        }
    }

    @GET
    @Path("/courses/{targetLang}/{sourceLang}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get courses by target and source langs")
    @Secured()
    @SecurityRequirement(name = "JWT")
    public List<CourseUserDto> getCourses(@Context SecurityContext securityContext,
                                          @PathParam("targetLang") String targetLang,
                                          @PathParam("sourceLang") String sourceLang,
                                          @DefaultValue("false") @QueryParam("learning") boolean learning) {
        UserPrincipal userPrincipal = (UserPrincipal) securityContext.getUserPrincipal();
        Integer userId = userPrincipal.getId();
        String username = userPrincipal.getName();

        if (learning) {
            List<LearningCourse> learningCourses = studyService.getLearningCourses(username, targetLang, sourceLang);
            return CourseUserDto.toDto(learningCourses);
        } else {
            List<Course> courses = courseService.getCourses(targetLang, sourceLang);
            List<LearningCourse> learningCourses = studyService.getLearningCourses(username, targetLang, sourceLang);
            return CourseUserDto.toDto(courses, learningCourses);
        }
    }

    @GET
    @Path("/courses/{targetLang}/{sourceLang}/{course}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get course")
    @Secured()
    @SecurityRequirement(name = "JWT")
    public CourseUserDto getCourse(@Context SecurityContext securityContext,
                                   @PathParam("course") String courseTitle,
                                   @PathParam("targetLang") String targetLang,
                                   @PathParam("sourceLang") String sourceLang,
                                   @DefaultValue("false") @QueryParam("learning") boolean learning) {
        UserPrincipal userPrincipal = (UserPrincipal) securityContext.getUserPrincipal();
        Integer userId = userPrincipal.getId();
        String username = userPrincipal.getName();

        if (learning) {
            LearningCourse learningCourse = studyService.getLearningCourse(username, courseTitle, targetLang, sourceLang);
            return new CourseUserDto(learningCourse);
        } else {
            try {
                LearningCourse learningCourse = studyService.getLearningCourse(username, courseTitle, targetLang, sourceLang);
                return new CourseUserDto(learningCourse);
            } catch (NoResultException e) {
                Course course = courseService.getCourse(courseTitle, targetLang, sourceLang);
                return new CourseUserDto(course);
            }
        }
    }



    @POST
    @Path("/courses/{targetLang}/{sourceLang}/{course}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "Add course for user")
    @Secured()
    @SecurityRequirement(name = "JWT")
    public void addCourse(@Context SecurityContext securityContext,
                          @PathParam("course") String title,
                          @PathParam("targetLang") String targetLang,
                          @PathParam("sourceLang") String sourceLang) {
        UserPrincipal userPrincipal = (UserPrincipal) securityContext.getUserPrincipal();
        Integer userId = userPrincipal.getId();
        String username = userPrincipal.getName();

        studyService.addLearningCourse(username, title, targetLang, sourceLang);
    }

    @POST
    @Path("/courses/{targetLang}/{sourceLang}/{course}/{topic}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "Add topic for learning course for user")
    @Secured()
    @SecurityRequirement(name = "JWT")
    public void addLearningTopic(@Context SecurityContext securityContext,
                                 @PathParam("course") String courseTitle,
                                 @PathParam("topic") String topicTitle,
                                 @PathParam("targetLang") String targetLang,
                                 @PathParam("sourceLang") String sourceLang) {
        UserPrincipal userPrincipal = (UserPrincipal) securityContext.getUserPrincipal();
        Integer userId = userPrincipal.getId();
        String username = userPrincipal.getName();

        studyService.addTopicForLearningCourse(username, courseTitle, targetLang, sourceLang, topicTitle);
    }

    @PUT
    @Path("/courses/{targetLang}/{sourceLang}/{course}/{topic}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "Update learning elements for learning topic")
    @Secured()
    @SecurityRequirement(name = "JWT")
    public void updateLearningTopicElements(@Context SecurityContext securityContext,
                                            @PathParam("course") String courseTitle,
                                            @PathParam("topic") String topicTitle,
                                            @PathParam("targetLang") String targetLang,
                                            @PathParam("sourceLang") String sourceLang) {
        UserPrincipal userPrincipal = (UserPrincipal) securityContext.getUserPrincipal();
        Integer userId = userPrincipal.getId();
        String username = userPrincipal.getName();

        studyService.updateLearningTopicElements(username, courseTitle, targetLang, sourceLang, topicTitle);
    }



    @GET
    @Path("/langpairs")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get lang pairs")
    @Secured()
    @SecurityRequirement(name = "JWT")
    public List<LangPair> getLangPairs(@Context SecurityContext securityContext,
                                       @DefaultValue("false") @QueryParam("learning") Boolean learning) {
        UserPrincipal userPrincipal = (UserPrincipal) securityContext.getUserPrincipal();
        Integer userId = userPrincipal.getId();
        String username = userPrincipal.getName();

        if (learning) {
            return studyService.getLangPairs(username);
        } else {
            return studyService.getLangPairs();
        }
    }
}
