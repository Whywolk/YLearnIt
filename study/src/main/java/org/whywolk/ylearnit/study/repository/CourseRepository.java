/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.repository;

import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.persistence.TypedQuery;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.whywolk.ylearnit.jpa.PersistanceUnit;
import org.whywolk.ylearnit.study.domain.LangPair;
import org.whywolk.ylearnit.study.entity.*;

import java.util.List;

public class CourseRepository {
    private static final Logger log = LogManager.getLogger(CourseRepository.class);
    private static final ThreadLocal<EntityManager> threadLocal = new ThreadLocal<>();
    private final PersistanceUnit studyUnit;

    public CourseRepository(PersistanceUnit studyUnit) {
        this.studyUnit = studyUnit;
    }

    private EntityManager getEntityManager() {
        EntityManager em = threadLocal.get();
        if (em == null) {
            em = this.studyUnit.createEntityManager();
            threadLocal.set(em);
        }
        return em;
    }

    private void closeEntityManager() {
        EntityManager em = threadLocal.get();
        if (em != null) {
            em.close();
            threadLocal.set(null);
        }
    }

    // ---------Course---------

    public Course createCourse(Course course) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        em.persist(course);
        em.getTransaction().commit();
        closeEntityManager();
        return course;
    }

    public Course updateCourse(Course course) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        em.merge(course);
        em.getTransaction().commit();
        closeEntityManager();
        return course;
    }

    public void deleteCourse(Integer id) {
        EntityManager em = getEntityManager();
        Course course = em.find(Course.class, id);
        em.getTransaction().begin();
        em.remove(course);
        em.getTransaction().commit();
        closeEntityManager();
    }

    public Course getCourse(Integer id) {
        EntityManager em = getEntityManager();
        TypedQuery<Course> query = em.createQuery("""
                SELECT c FROM Course c
                  LEFT JOIN FETCH c.topics
                 WHERE c.id = :id
                """, Course.class)
                .setParameter("id", id);
        Course course = query.getSingleResult();
        closeEntityManager();
        return course;
    }

    public Course getCourse(String title, String targetLang, String sourceLang) {
        EntityManager em = getEntityManager();
        TypedQuery<Course> query = em.createQuery("""
                SELECT c FROM Course c
                  LEFT JOIN FETCH c.topics
                 WHERE c.title = :title
                   AND c.targetLang = :targetLang
                   AND c.sourceLang = :sourceLang
                """, Course.class)
                .setParameter("title", title)
                .setParameter("targetLang", targetLang)
                .setParameter("sourceLang", sourceLang);
        Course course = query.getSingleResult();
        closeEntityManager();
        return course;
    }

    public List<Course> getCourses(String targetLang) {
        EntityManager em = getEntityManager();
        TypedQuery<Course> query = em.createQuery("""
                SELECT c FROM Course c
                  LEFT JOIN FETCH c.topics
                 WHERE c.targetLang = :targetLang
                """, Course.class)
                .setParameter("targetLang", targetLang);
        List<Course> courses = query.getResultList();
        closeEntityManager();
        return courses;
    }

    public List<Course> getCourses(String targetLang, String sourceLang) {
        EntityManager em = getEntityManager();
        TypedQuery<Course> query = em.createQuery("""
                SELECT c FROM Course c
                  LEFT JOIN FETCH c.topics t
                 WHERE c.targetLang = :targetLang
                   AND c.sourceLang = :sourceLang
                """, Course.class)
                .setParameter("targetLang", targetLang)
                .setParameter("sourceLang", sourceLang);
        List<Course> courses = query.getResultList();
        closeEntityManager();
        return courses;
    }

    // ---------Topic---------

    public Topic createTopic(Topic topic) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        em.persist(topic);
        em.getTransaction().commit();
        closeEntityManager();
        return topic;
    }

    public Topic updateTopic(Topic topic) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        em.merge(topic);
        em.getTransaction().commit();
        closeEntityManager();
        return topic;
    }

    public void deleteTopic(Integer id) {
        EntityManager em = getEntityManager();
        Topic topic = em.find(Topic.class, id);
        em.getTransaction().begin();
        em.remove(topic);
        em.getTransaction().commit();
        closeEntityManager();
    }

    public Topic getTopic(Integer id) {
        EntityManager em = getEntityManager();
        TypedQuery<Topic> query = em.createQuery("""
                SELECT t FROM Topic t
                  JOIN FETCH t.course c
                 WHERE t.id = :id
                """, Topic.class)
                .setParameter("id", id);
        Topic topic = query.getSingleResult();
        closeEntityManager();
        return topic;
    }

    // ---------TopicElement---------

    public void updateTopicElements(Topic topic, List<TopicElement> elements) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        for (TopicElement element : elements) {
            em.merge(element);
        }
        em.getTransaction().commit();
        closeEntityManager();
    }

    public void deleteTopicElements(Integer topicId, List<Integer> elementIds) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        Query query = em.createQuery("""
                DELETE FROM TopicElement te
                 WHERE te.topicElementId.topicId = :topicId
                   AND te.topicElementId.elementId IN :elementIds
                """)
                .setParameter("topicId", topicId)
                .setParameter("elementIds", elementIds);
        query.executeUpdate();
        em.getTransaction().commit();
        closeEntityManager();
    }

    // ---------LearningCourse---------

    public LearningCourse createLearningCourse(LearningCourse course) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        em.persist(course);
        em.getTransaction().commit();
        closeEntityManager();
        return course;
    }

    public LearningCourse updateLearningCourse(LearningCourse course) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        em.merge(course);
        em.getTransaction().commit();
        closeEntityManager();
        return course;
    }

    public LearningCourse getLearningCourse(String username, Course course) {
        return getLearningCourse(username, course.getTitle(), course.getTargetLang(), course.getSourceLang());
    }

    public LearningCourse getLearningCourse(String username, String courseTitle, String targetLang, String sourceLang) {
        EntityManager em = getEntityManager();
        TypedQuery<LearningCourse> query = em.createQuery("""
                SELECT lc FROM LearningCourse lc JOIN FETCH lc.topics
                 WHERE lc.userData.name = :username
                   AND lc.course.title = :courseTitle
                   AND lc.course.targetLang = :targetLang
                   AND lc.course.sourceLang = :sourceLang
                """, LearningCourse.class)
                .setParameter("username", username)
                .setParameter("courseTitle", courseTitle)
                .setParameter("targetLang", targetLang)
                .setParameter("sourceLang", sourceLang);
        LearningCourse learningCourse = query.getSingleResult();
        closeEntityManager();
        return learningCourse;
    }

    public List<LearningCourse> getLearningCourses(String username) {
        EntityManager em = getEntityManager();
        TypedQuery<LearningCourse> query = em.createQuery("""
                SELECT lc FROM LearningCourse lc JOIN FETCH lc.topics
                 WHERE lc.userData.name = :username
                """, LearningCourse.class)
                .setParameter("username", username);
        List<LearningCourse> learningCourses = query.getResultList();
        closeEntityManager();
        return learningCourses;
    }

    public List<LearningCourse> getLearningCourses(String username, String targetLang) {
        EntityManager em = getEntityManager();
        TypedQuery<LearningCourse> query = em.createQuery("""
                SELECT lc FROM LearningCourse lc JOIN FETCH lc.topics
                 WHERE lc.userData.name = :username
                   AND lc.course.targetLang = :targetLang
                """, LearningCourse.class)
                .setParameter("username", username)
                .setParameter("targetLang", targetLang);
        List<LearningCourse> learningCourses = query.getResultList();
        closeEntityManager();
        return learningCourses;
    }

    public List<LearningCourse> getLearningCourses(String username, String targetLang, String sourceLang) {
        EntityManager em = getEntityManager();
        TypedQuery<LearningCourse> query = em.createQuery("""
                SELECT lc FROM LearningCourse lc JOIN FETCH lc.topics
                 WHERE lc.userData.name = :username
                   AND lc.course.targetLang = :targetLang
                   AND lc.course.sourceLang = :sourceLang
                """, LearningCourse.class)
                .setParameter("username", username)
                .setParameter("targetLang", targetLang)
                .setParameter("sourceLang", sourceLang);
        List<LearningCourse> learningCourses = query.getResultList();
        closeEntityManager();
        return learningCourses;
    }

    // ---------Misc---------

    public List<LangPair> getLangPairs() {
        EntityManager em = getEntityManager();
        TypedQuery<LangPair> query = em.createQuery("""
                SELECT NEW org.whywolk.ylearnit.study.domain.LangPair(c.targetLang, c.sourceLang) FROM Course c
                """, LangPair.class);
        List<LangPair> pairs = query.getResultList();
        closeEntityManager();
        return pairs;
    }

    public List<LangPair> getLangPairs(String username) {
        EntityManager em = getEntityManager();
        TypedQuery<LangPair> query = em.createQuery("""
                SELECT NEW org.whywolk.ylearnit.study.domain.LangPair(c.targetLang, c.sourceLang) FROM LearningCourse lc
                  LEFT JOIN lc.course c
                 WHERE lc.userData.name = :username
                """, LangPair.class)
                .setParameter("username", username);
        List<LangPair> pairs = query.getResultList();
        closeEntityManager();
        return pairs;
    }
}
