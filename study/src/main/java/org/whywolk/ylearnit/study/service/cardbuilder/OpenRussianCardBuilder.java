/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022-2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.service.cardbuilder;

import org.whywolk.ylearnit.openrussian.domain.Lang;
import org.whywolk.ylearnit.openrussian.domain.Translation;
import org.whywolk.ylearnit.openrussian.domain.Word;
import org.whywolk.ylearnit.openrussian.service.OpenRussianService;
import org.whywolk.ylearnit.study.domain.*;
import org.whywolk.ylearnit.study.entity.DbElement;
import org.whywolk.ylearnit.study.entity.Element;

import java.util.ArrayList;
import java.util.List;

public class OpenRussianCardBuilder extends CardBuilder {
    private final OpenRussianService service;

    public OpenRussianCardBuilder(OpenRussianService service) {
        this.service = service;
    }

    @Override
    public String getDbName() {
        return "openrussian";
    }

    @Override
    public List<Card> getCards(List<DbElement> elements) {
        List<Integer> idsDb = getIds(elements);
        List<Word> words = service.getByIds(idsDb);
        List<Card> cards = buildCards(words);
        return cards;
    }

    @Override
    public List<Card> getCards(List<Element> elements, String sourceLang) {
        String targetLang = elements.get(0).getTargetLang();
        List<Integer> idsDb = getIds(elements, sourceLang);
        List<Word> words = service.getByIds(idsDb);
        List<Card> cards = buildCards(words, targetLang, sourceLang);
        return cards;
    }

    @Override
    public List<Card> getCards(List<DbElement> elements, String targetLang, String sourceLang) {
        List<Integer> idsDb = getIds(elements, targetLang, sourceLang);
        List<Word> words = service.getByIds(idsDb);
        List<Card> cards = buildCards(words, targetLang, sourceLang);
        return cards;
    }

    public List<Card> buildCards(List<Word> words) {
        List<Card> cards = new ArrayList<>();
        for (Word w : words) {
            Card card = buildCard(w);
            if (card.getSides().size() >= 2) {
                card.setDbName(getDbName());
                card.setElementIdDb(w.getId());
                cards.add(card);
            }
        }
        return cards;
    }

    public List<Card> buildCards(List<Word> words, String targetLang, String sourceLang) {
        List<Card> cards = new ArrayList<>();
        for (Word w : words) {
            Card card = buildCard(w, targetLang, sourceLang);
            if (card.getSides().size() >= 2) {
                card.setDbName(getDbName());
                card.setElementIdDb(w.getId());
                cards.add(card);
            }
        }
        return cards;
    }


    private Card buildCard(Word word) {
        Card c = new Card();
        c.addSide(getOriginalSide(word));
        for (Translation translation : word.getTranslations()) {
            c.addSide(getTranslationSide(translation));
        }
        return c;
    }

    private Card buildCard(Word word, String targetLang, String sourceLang) {
        // ru <--> en
        // ru <--> de
        Card c = new Card();
        // without default
        // if ((targetLang.equals("ru") && Translation.Lang.contains(sourceLang)) ||
        if (targetLang.equals("ru")
                || (sourceLang.equals("ru") && Lang.contains(targetLang))) {
            c.addSide(getOriginalSide(word));

            String translationLang;
            if (Lang.contains(targetLang)) {
                translationLang = targetLang;
            } else if (Lang.contains(sourceLang)) {
                translationLang = sourceLang;
            } else {
                return c;
            }

            for (Translation t : word.getTranslations(translationLang)) {
                c.addSide(getTranslationSide(t));
            }
        }

        return c;
    }

    private Side getOriginalSide(Word w) {
        Tag tag = new Tag("word", "ru", ContentType.TEXT_PLAIN);
        Side side = new Side(tag, w.getBare());
        side.addVariant("WRITE");
        return side;
    }

    private Side getTranslationSide(Translation t) {
        Tag tag = new Tag("translation", t.getLang().getCode(), ContentType.TEXT_PLAIN);
        Side side = new Side(tag, t.getTranslation());
        side.addVariant("WRITE");
        return side;
    }
}
