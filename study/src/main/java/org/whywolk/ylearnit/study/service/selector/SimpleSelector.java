/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022-2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.service.selector;

import org.whywolk.ylearnit.study.entity.LearningElement;

import java.util.ArrayList;
import java.util.List;

public class SimpleSelector implements Selector {

    public SimpleSelector() {
    }

    @Override
    public List<LearningElement> getWeakElements(List<LearningElement> learningElements) {
        List<LearningElement> res = new ArrayList<>();
        learningElements.forEach(learningElement -> res.add(learningElement));
        return res;
    }
}
