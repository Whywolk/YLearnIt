/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022-2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.whywolk.ylearnit.study.domain.Card;
import org.whywolk.ylearnit.study.domain.ReviewParams;
import org.whywolk.ylearnit.study.domain.StudyTest;
import org.whywolk.ylearnit.study.domain.UserAnswer;
import org.whywolk.ylearnit.study.entity.Answer;
import org.whywolk.ylearnit.study.entity.Element;
import org.whywolk.ylearnit.study.entity.LearningElement;
import org.whywolk.ylearnit.study.entity.Repetition;
import org.whywolk.ylearnit.study.factory.AnswerEvaluatorFactory;
import org.whywolk.ylearnit.study.factory.SchedulerFactory;
import org.whywolk.ylearnit.study.factory.TestBuilderFactory;
import org.whywolk.ylearnit.study.repository.StudyRepository;
import org.whywolk.ylearnit.study.service.evaluator.AnswerEvaluator;
import org.whywolk.ylearnit.study.service.scheduler.Scheduler;
import org.whywolk.ylearnit.study.service.selector.Selector;
import org.whywolk.ylearnit.study.service.selector.SimpleSelector;
import org.whywolk.ylearnit.study.service.testbuilder.TestBuilder;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

public class ReviewService {
    private static final Logger log = LogManager.getLogger(ReviewService.class);
    private static final String DEFAULT_TEST_TYPE = "SM2";
    private static final String DEFAULT_SCHEDULER_TYPE = "SM2";
    private StudyService service;
    private StudyRepository repository;
    private String testType;
    private String schedulerType;
    private boolean shuffle;
    private ReviewParams params;

    public ReviewService(StudyService service, StudyRepository repository) {
        this.service = service;
        this.repository = repository;
        this.testType = DEFAULT_TEST_TYPE;
        this.schedulerType = DEFAULT_SCHEDULER_TYPE;
        this.shuffle = false;
    }

    public ReviewService(StudyService service, StudyRepository repository, ReviewParams params) {
        this(service, repository);
        try {
            TestBuilderFactory.get(testType);
            this.testType = params.getTestType();
        } catch (Exception e) {
            this.testType = DEFAULT_TEST_TYPE;
        }
        this.shuffle = params.isShuffle();
        this.params = params;
    }

    public List<StudyTest> getTests(String username) {
        if (params.getTargetLang().equals(params.getSourceLang())) {
            String msg = String.format(
                    "Target lang='%s' and source lang='%s' cannot be equal",
                    params.getTargetLang(), params.getSourceLang());
            log.warn(msg);
            throw new RuntimeException(msg);
        }

        if (params.getCourseTitle() != null && !params.getTopicTitles().isEmpty()) {
            return getTests(username, params.getTargetLang(), params.getSourceLang(), params.getCourseTitle(), params.getTopicTitles());
        } else {
            return getTests(username, params.getTargetLang(), params.getSourceLang());
        }
    }

    public List<StudyTest> getTests(String username, String targetLang, String sourceLang,
                                    String courseTitle, List<String> topicTitles) {
        LocalDate date = LocalDate.now(ZoneOffset.UTC);

        // 0. Get weak elements
        List<LearningElement> learningElements = repository.getWeakElements(date, username, targetLang, sourceLang, courseTitle, topicTitles);

        return getTests(username, targetLang, sourceLang, learningElements);
    }

    public List<StudyTest> getTests(String username, String targetLang, String sourceLang) {
        LocalDate date = LocalDate.now(ZoneOffset.UTC);

        // 0. Get weak elements
        List<LearningElement> learningElements = repository.getWeakElements(date, username, targetLang, sourceLang);

        return getTests(username, targetLang, sourceLang, learningElements);
    }

    public List<StudyTest> getTests(String username, String targetLang, String sourceLang, List<LearningElement> learningElements) {

        // 1. Select weak elements
        Selector selector = new SimpleSelector();
        List<LearningElement> selectedElements = selector.getWeakElements(learningElements);

        if (selectedElements.isEmpty()) {
            return new ArrayList<>();
        }
        List<Element> elements = new ArrayList<>();
        selectedElements.forEach(le -> elements.add(le.getElement()));

        // 2. Build cards
        List<Card> cards = buildCards(targetLang, sourceLang, elements);

        // 3. Build tests
        List<StudyTest> tests = buildTests(cards, learningElements);

        if (shuffle) {
            Collections.shuffle(tests);
        }

        return tests;
    }

    public void endTests(String username, List<UserAnswer> userAnswers) {
        removeDuplicates(userAnswers);

        // 4. Evaluate user answers
        List<LearningElement> learningElements = evaluateAnswers(username, userAnswers);

        // 5. Predict new intervals
        predictIntervals(learningElements);

        repository.updateLearningElements(learningElements);
    }

    public boolean isShuffle() {
        return shuffle;
    }

    public void setShuffle(boolean shuffle) {
        this.shuffle = shuffle;
    }

    private Map<String, List<Element>> splitElementsByDb(List<LearningElement> learningElements) {
        List<Element> elements = learningElements.stream().map(le -> le.getElement()).collect(Collectors.toList());
        return service.splitElementsByDb(elements);
    }

    private List<Card> buildCards(String targetLang, String sourceLang, List<Element> elements) {
        Map<Element, Card> cardMap = service.getCardsByElements(elements, sourceLang);
        List<Card> cards = new ArrayList<>(cardMap.values());
        return cards;
    }

    private List<StudyTest> buildTests(List<Card> cards, List<LearningElement> learningElements) {
        Map<String, Map<Integer, LearningElement>> lesMap = new HashMap<>();
        for (LearningElement learningElement : learningElements) {
            Element e = learningElement.getElement();

            String dbName = e.getDbElement().getDbName();
            Integer elementIdDb = e.getDbElement().getElementIdDb();
            if (! lesMap.containsKey(dbName)) {
                lesMap.put(dbName, new HashMap<>());
            }
            lesMap.get(dbName).put(elementIdDb, learningElement);
        }

        TestBuilder testBuilder = TestBuilderFactory.get(testType);
        List<StudyTest> tests = testBuilder.getTest(cards);
        for (StudyTest test : tests) {
            LearningElement le = lesMap.get(test.getDbName()).get(test.getElementIdDb());

            test.setLearningElementId(le.getId());
            test.setRepetitionId(le.getNextRepetition().getId());
        }
        return tests;
    }



    private void removeDuplicates(List<UserAnswer> answers) {
        HashSet<UserAnswer> seen = new HashSet<>();
        answers.removeIf(e -> ! seen.add(e));
        seen.clear();
    }

    private List<LearningElement> evaluateAnswers(String username, List<UserAnswer> userAnswers) {
        List<Integer> ids = new ArrayList<>();
        Map<Integer, UserAnswer> userAnswersMap = new HashMap<>();
        Map<Integer, Answer> answersMap = new HashMap<>();
        Map<Integer, LearningElement> lesMap = new HashMap<>();

        Map<String, List<UserAnswer>> userAnswerByType = new HashMap<>();
        userAnswers.forEach(u -> {
            if (! userAnswerByType.containsKey(u.getType())) {
                userAnswerByType.put(u.getType(), new ArrayList<>());
            }
            userAnswerByType.get(u.getType()).add(u);
        });

        for (String type : userAnswerByType.keySet()) {
            AnswerEvaluator evaluator = AnswerEvaluatorFactory.get(type);
            for (UserAnswer userAnswer : userAnswers) {
                try {
                    Answer answer = evaluator.evaluate(userAnswer);
                    answer.setThinkingTime(userAnswer.getThinkingTime());
                    answer.setTotalTime(userAnswer.getTotalTime());
                    answer.setType(userAnswer.getType());
                    answer.setData(userAnswer.getData());

                    Integer leId = userAnswer.getLearningElementId();
                    ids.add(leId);
                    userAnswersMap.put(leId, userAnswer);
                    answersMap.put(leId, answer);
                } catch (Exception e) {

                }
            }
        }

        List<LearningElement> les = repository.getLearningElements(username, ids);

        les.forEach(e -> lesMap.put(e.getId(), e));

        for (LearningElement le : lesMap.values()) {
            UserAnswer userAnswer = userAnswersMap.get(le.getId());
            Answer answer = answersMap.get(le.getId());

            if (userAnswer != null && answer != null) {
                Repetition nextRep = le.getNextRepetition();
                if (nextRep.getId().equals(userAnswer.getRepetitionId())) {
                    nextRep.setAnswer(answer);
                    continue;
                }
            }
            les.remove(le);
        }
        return les;
    }

    private void predictIntervals(List<LearningElement> learningElements) {
        Scheduler scheduler = SchedulerFactory.get(schedulerType);
        scheduler.predictIntervals(learningElements);
    }
}
