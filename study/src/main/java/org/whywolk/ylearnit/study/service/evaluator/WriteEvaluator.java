/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.service.evaluator;

import org.apache.commons.text.similarity.LevenshteinDistance;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.whywolk.ylearnit.study.domain.ContentType;
import org.whywolk.ylearnit.study.domain.Side;
import org.whywolk.ylearnit.study.domain.UserAnswer;
import org.whywolk.ylearnit.study.entity.Answer;

import java.util.*;

public class WriteEvaluator implements AnswerEvaluator {
    private static final Logger log = LogManager.getLogger(WriteEvaluator.class);
    private static final String USER_ANSWER_SEPARATOR = ",";

    public WriteEvaluator() {
    }

    @Override
    public Answer evaluate(UserAnswer userAnswer) {
        if (! userAnswer.getType().equals("WRITE")) {
            String msg = "Expected answer type='WRITE', not '" + userAnswer.getType() + "'";
            log.warn(msg);
            throw new RuntimeException(msg);
        }

        Side u = userAnswer.getUserAnswer();
        if (u.getContentType() != ContentType.TEXT_PLAIN) {
            throw new RuntimeException();
        }

        List<Side> answers = userAnswer.getTestAnswers();
        for (Side a : answers) {
            if (a.getContentType() != ContentType.TEXT_PLAIN) {
                throw new RuntimeException();
            }
        }

        // split into correct answers and user answers
        List<String> userAnswers = split(u.getContent());
        List<String> correctAnswers = new ArrayList<>();
        answers.forEach(a -> correctAnswers.addAll(split(a.getContent())));

        int accuracy = calcAccuracy(correctAnswers, userAnswers);
        int recall = calcRecall(accuracy, userAnswer.getThinkingTime());

        Answer answer = new Answer();
        answer.setAccuracy(accuracy);
        answer.setRetrievability(recall);

        return answer;
    }


    private int calcAccuracy(String firstStr, String secondStr) {
        firstStr = firstStr.toLowerCase().trim();
        secondStr = secondStr.toLowerCase().trim();
        if (firstStr.isEmpty() || secondStr.isEmpty()) {
            return 0;
        }

        int accuracy = 0;
        int dist = new LevenshteinDistance().apply(firstStr, secondStr);
        if (dist <= firstStr.length()) {
            int correct = firstStr.length() - dist;
            accuracy = Math.round((1f * correct / firstStr.length()) * 100);
        }

        accuracy = clamp(accuracy, 0, 100);

        return accuracy;
    }

    private int calcAccuracy(List<String> correctAnswers, List<String> userAnswers) {
        Map<String, Map<String, Integer>> accuracyMatrix = calcAccuracyMatrix(correctAnswers, userAnswers);

        List<Integer> allAcc = new ArrayList<>();
        for (String userAnswer : accuracyMatrix.keySet()) {
            allAcc.add(selectBestAccuracy(accuracyMatrix.get(userAnswer).values()));
        }

        int accuracy = (int) Math.round(average(allAcc));
        accuracy = clamp(accuracy, 0, 100);

        return accuracy;
    }

    private int calcRecall(int accuracy, int thinkingTime) {
        thinkingTime = clamp(thinkingTime, 0, 60);
        int timePercent = (int) Math.round((double) thinkingTime/60 * 100);
        return clamp(accuracy - timePercent, 0, 100);
    }

    private Map<String, Map<String, Integer>> calcAccuracyMatrix(List<String> correctAnswers, List<String> userAnswers) {
        Map<String, Map<String, Integer>> accuracyMatrix = new HashMap<>();
        for (String userAnswer : userAnswers) {
            accuracyMatrix.put(userAnswer, new HashMap<>());
            for (String correctAnswer : correctAnswers) {
                accuracyMatrix.get(userAnswer).put(correctAnswer, calcAccuracy(correctAnswer, userAnswer));
            }
        }
        return accuracyMatrix;
    }

    private int selectBestAccuracy(Collection<Integer> accuracy) {
        return Collections.max(accuracy);
    }

    private int clamp(int val, int min, int max) {
        return Math.max(min, Math.min(max, val));
    }

    private List<String> split(String str) {
        return Arrays.asList(str.split(USER_ANSWER_SEPARATOR));
    }

    private double average(List<Integer> list) {
        return list.stream().mapToInt(i -> i).average().orElse(0.0);
    }
}
