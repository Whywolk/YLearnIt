/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022-2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.repository;

import jakarta.persistence.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.whywolk.ylearnit.jpa.PersistanceUnit;
import org.whywolk.ylearnit.study.domain.LangPair;
import org.whywolk.ylearnit.study.entity.*;

import java.time.LocalDate;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class StudyRepository {
    private static final Logger log = LogManager.getLogger(StudyRepository.class);
    private static final ThreadLocal<EntityManager> threadLocal = new ThreadLocal<>();
    private final PersistanceUnit studyUnit;
    private final CourseRepository courseRepository;

    public StudyRepository(PersistanceUnit studyUnit, CourseRepository courseRepository) {
        this.studyUnit = studyUnit;
        this.courseRepository = courseRepository;
    }

    public EntityManager getEntityManager() {
        EntityManager em = threadLocal.get();
        if (em == null) {
            em = this.studyUnit.createEntityManager();
            threadLocal.set(em);
        }
        return em;
    }

    public void closeEntityManager() {
        EntityManager em = threadLocal.get();
        if (em != null) {
            em.close();
            threadLocal.set(null);
        }
    }

    public Set<String> getDbNames(String targetLang) {
        EntityManager em = getEntityManager();
        TypedQuery<String> query = em.createQuery("""
                SELECT DISTINCT de.dbName FROM DbElement de
                  JOIN de.elements e
                 WHERE e.targetLang = :targetLang
                """, String.class)
                .setParameter("targetLang", targetLang);
        List<String> dbNames = query.getResultList();
        closeEntityManager();
        return new HashSet<>(dbNames);
    }

    public Set<String> getDbNames(String targetLang, String sourceLang) {
        EntityManager em = getEntityManager();
        TypedQuery<String> query = em.createQuery("""
                SELECT DISTINCT de.dbName FROM DbElement de
                  JOIN de.elements e
                  JOIN e.sourceLangs sl
                 WHERE e.targetLang = :targetLang
                   AND sl = :sourceLang
                """, String.class)
                .setParameter("targetLang", targetLang)
                .setParameter("sourceLang", sourceLang);
        List<String> dbNames = query.getResultList();
        closeEntityManager();
        return new HashSet<>(dbNames);
    }

    public void createDbElements(List<DbElement> elements) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        for (DbElement e : elements) {
            em.persist(e);
        }
        em.getTransaction().commit();
        closeEntityManager();
    }

    public void updateDbElements(List<DbElement> elements) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        for (DbElement e : elements) {
            em.merge(e);
        }
        em.getTransaction().commit();
        closeEntityManager();
    }

    public List<DbElement> getDbElements(String dbName) {
        EntityManager em = getEntityManager();
        TypedQuery<DbElement> query = em.createQuery("""
                SELECT de FROM DbElement de
                  LEFT JOIN FETCH de.elements e
                  LEFT JOIN FETCH e.sourceLangs sl
                 WHERE de.dbName = :dbName
                """, DbElement.class)
                .setParameter("dbName", dbName);
        List<DbElement> elements = query.getResultList();
        closeEntityManager();
        return elements;
    }

    public List<DbElement> getDbElements(String dbName, Collection<Integer> elementIdsDb) {
        EntityManager em = getEntityManager();
        TypedQuery<DbElement> query = em.createQuery("""
                SELECT de FROM DbElement de
                  LEFT JOIN FETCH de.elements e
                  LEFT JOIN FETCH e.sourceLangs sl
                 WHERE de.dbName = :dbName
                   AND de.elementIdDb IN :elementIdsDb
                """, DbElement.class)
                .setParameter("dbName", dbName)
                .setParameter("elementIdsDb", elementIdsDb);
        List<DbElement> elements = query.getResultList();
        closeEntityManager();
        return elements;
    }


    public void createElements(List<Element> elements) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        for (Element e : elements) {
            em.persist(e);
        }
        em.getTransaction().commit();
        closeEntityManager();
    }

    public void updateElements(List<Element> elements) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        for (Element e : elements) {
            em.merge(e);
        }
        em.getTransaction().commit();
        closeEntityManager();
    }

    public List<Element> getElements(String dbName) {
        EntityManager em = getEntityManager();
        TypedQuery<Element> query = em.createQuery("""
                SELECT e FROM Element e
                  JOIN FETCH e.dbElement de
                  LEFT JOIN FETCH e.sourceLangs sl
                 WHERE de.dbName = :dbName
                """, Element.class)
                .setParameter("dbName", dbName);
        List<Element> elements = query.getResultList();
        closeEntityManager();
        return elements;
    }

    public List<Element> getElements(String dbName, String targetLang) {
        EntityManager em = getEntityManager();
        TypedQuery<Element> query = em.createQuery("""
                SELECT e FROM Element e
                  JOIN FETCH e.dbElement de
                  LEFT JOIN FETCH e.sourceLangs sl
                 WHERE de.dbName = :dbName
                   AND e.targetLang = :targetLang
                """, Element.class)
                .setParameter("dbName", dbName)
                .setParameter("targetLang", targetLang);
        List<Element> elements = query.getResultList();
        closeEntityManager();
        return elements;
    }

    public List<Element> getElements(String dbName, String targetLang, String sourceLang) {
        EntityManager em = getEntityManager();
        TypedQuery<Element> query = em.createQuery("""
                SELECT e FROM Element e
                  JOIN FETCH e.dbElement de
                  LEFT JOIN FETCH e.sourceLangs sl
                 WHERE de.dbName = :dbName
                   AND e.targetLang = :targetLang
                   AND :sourceLang MEMBER OF e.sourceLangs
                """, Element.class)
                .setParameter("dbName", dbName)
                .setParameter("targetLang", targetLang)
                .setParameter("sourceLang", sourceLang);
        List<Element> elements = query.getResultList();
        closeEntityManager();
        return elements;
    }

    public List<Element> getElements(Collection<Integer> ids) {
        EntityManager em = getEntityManager();
        TypedQuery<Element> query = em.createQuery("""
                SELECT e FROM Element e
                  JOIN FETCH e.sourceLangs
                 WHERE e.id IN :ids
                """, Element.class)
                .setParameter("ids", ids);
        List<Element> elements = query.getResultList();
        closeEntityManager();
        return elements;
    }

    public List<Element> getElementsByTopic(Topic topic) {
        EntityManager em = getEntityManager();
        TypedQuery<Element> query = em.createQuery("""
                SELECT te.element FROM TopicElement te
                 WHERE te.topicElementId.topicId = :topicId
                 ORDER BY te.number ASC
                """, Element.class)
                .setParameter("topicId", topic.getId());
        List<Element> elements = query.getResultList();
        closeEntityManager();
        return elements;
    }

    public List<TopicElement> getTopicElementsByTopic(Topic topic) {
        EntityManager em = getEntityManager();
        TypedQuery<TopicElement> query = em.createQuery("""
                SELECT te FROM TopicElement te
                 WHERE te.topicElementId.topicId = :topicId
                 ORDER BY te.number ASC
                """, TopicElement.class)
                .setParameter("topicId", topic.getId());
        List<TopicElement> elements = query.getResultList();
        closeEntityManager();
        return elements;
    }


    public void createLearningElements(List<LearningElement> elements) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        for (LearningElement e : elements) {
            em.persist(e);
        }
        em.getTransaction().commit();
        closeEntityManager();
    }

    public void updateLearningElements(List<LearningElement> elements) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        for (LearningElement e : elements) {
            em.merge(e);
        }
        em.getTransaction().commit();
        closeEntityManager();
    }

    public List<LearningElement> getLearningElements(LearningTopic lt) {
        EntityManager em = getEntityManager();
        TypedQuery<LearningElement> query = getLearningElementsQuery(em, lt);
        List<LearningElement> learningElements = query.getResultList();
        closeEntityManager();
        return learningElements;
    }

    public List<LearningElement> getLearningElements(LearningTopic lt, int page) {
        if (page < 0) {
            String msg = "Page number shouldn't be lesser than 0";
            log.warn(msg);
            throw new RuntimeException(msg);
        }
        int pageSize = 100;

        EntityManager em = getEntityManager();

        TypedQuery<LearningElement> query = getLearningElementsQuery(em, lt);

        query.setFirstResult(page * pageSize);
        query.setMaxResults(pageSize);
        List<LearningElement> learningElements = query.getResultList();
        closeEntityManager();
        return learningElements;
    }

    private TypedQuery<LearningElement> getLearningElementsQuery(EntityManager em, LearningTopic lt) {
        TypedQuery<LearningElement> query = em.createQuery("""
                SELECT DISTINCT le FROM LearningElement le
                  JOIN FETCH le.userData ud
                  JOIN FETCH le.element e
                  JOIN FETCH e.topicElements te
                 WHERE ud.name = :username
                   AND te.topic IN
                       (SELECT DISTINCT t FROM LearningTopic lt
                          JOIN lt.topic t
                         WHERE lt.id = :lTopicId
                           AND lt.learningCourse.userData.name = :username)
                 ORDER BY te.number ASC
                """, LearningElement.class)
                .setParameter("username", lt.getLearningCourse().getUserData().getName())
                .setParameter("lTopicId", lt.getId());
        return query;
    }

    public LearningElement getLearningElement(String username, Integer id) {
        EntityManager em = getEntityManager();
        TypedQuery<LearningElement> query = em.createQuery("""
                SELECT le FROM LearningElement le
                 WHERE le.id = :id
                   AND le.userData.name = :username
                """, LearningElement.class)
                .setParameter("id", id)
                .setParameter("username", username);
        LearningElement learningElement = query.getSingleResult();
        closeEntityManager();
        return learningElement;
    }

    public List<LearningElement> getLearningElements(String username, Collection<Integer> ids) {
        EntityManager em = getEntityManager();
        TypedQuery<LearningElement> query = em.createQuery("""
                SELECT le FROM LearningElement le
                 WHERE le.id IN :ids
                   AND le.userData.name = :username
                """, LearningElement.class)
                .setParameter("ids", ids)
                .setParameter("username", username);
        List<LearningElement> learningElements = query.getResultList();
        closeEntityManager();
        return learningElements;
    }

    public List<LearningElement> getWeakElements(LocalDate date, String username, String targetLang, String sourceLang) {
        EntityManager em = getEntityManager();
        TypedQuery<LearningElement> query = em.createQuery("""
                SELECT le FROM LearningElement le
                  JOIN FETCH le.userData ud
                  JOIN FETCH le.element e
                 WHERE ud.name = :username
                   AND le.nextRepetition.date <= :date
                   AND e.targetLang = :targetLang
                   AND :sourceLang MEMBER OF e.sourceLangs
                """, LearningElement.class)
                .setParameter("username", username)
                .setParameter("date", date)
                .setParameter("targetLang", targetLang)
                .setParameter("sourceLang", sourceLang);
        query.setMaxResults(100);
        List<LearningElement> weakElements = query.getResultList();
        closeEntityManager();
        return weakElements;
    }

    public List<LearningElement> getWeakElements(LocalDate date, String username, String targetLang, String sourceLang,
                                                 String courseTitle, List<String> topicTitles) {
        EntityManager em = getEntityManager();
        TypedQuery<LearningElement> query = em.createQuery("""
                SELECT le FROM LearningElement le
                  JOIN FETCH le.userData ud
                  JOIN FETCH le.element e
                  JOIN FETCH le.element.topicElements te
                 WHERE ud.name = :username
                   AND le.nextRepetition.date <= :date
                   AND e.targetLang = :targetLang
                   AND :sourceLang MEMBER OF e.sourceLangs
                   AND te.topic IN
                       (SELECT DISTINCT t FROM LearningTopic lt
                          JOIN lt.topic t
                         WHERE lt.learningCourse.course.title = :courseTitle
                           AND lt.topic.title IN :topicTitles
                           AND lt.learningCourse.course.targetLang = :targetLang
                           AND lt.learningCourse.course.sourceLang = :sourceLang
                           AND lt.learningCourse.userData.name = :username
                           AND lt.learningCourse.studying = true
                           AND lt.studying = true)
                 ORDER BY te.topicElementId.topicId, te.number
                """, LearningElement.class)
                .setParameter("username", username)
                .setParameter("date", date)
                .setParameter("courseTitle", courseTitle)
                .setParameter("topicTitles", topicTitles)
                .setParameter("targetLang", targetLang)
                .setParameter("sourceLang", sourceLang);
        query.setMaxResults(100);
        List<LearningElement> weakElements = query.getResultList();
        closeEntityManager();
        return weakElements;
    }


    public UserData createUserData(UserData userData) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        em.persist(userData);
        em.getTransaction().commit();
        closeEntityManager();
        return userData;
    }

    public UserData updateUserData(UserData userData) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        em.merge(userData);
        em.getTransaction().commit();
        closeEntityManager();
        return userData;
    }

    public UserData getUserData(Integer id) {
        EntityManager em = getEntityManager();
        TypedQuery<UserData> query = em.createQuery("""
                SELECT ud FROM UserData ud
                 WHERE ud.id = :id
                """, UserData.class)
                .setParameter("id", id);
        UserData user = query.getSingleResult();
        closeEntityManager();
        return user;
    }

    public UserData getUserData(String name) {
        EntityManager em = getEntityManager();
        TypedQuery<UserData> query = em.createQuery("""
                SELECT ud FROM UserData ud
                 WHERE ud.name = :name
                """, UserData.class)
                .setParameter("name", name);
        UserData userData = query.getSingleResult();
        closeEntityManager();
        return userData;
    }


    public Course createCourse(Course course) {
        return courseRepository.createCourse(course);
    }

    public Course updateCourse(Course course) {
        return courseRepository.updateCourse(course);
    }

    public void deleteCourse(Course course) {
        courseRepository.deleteCourse(course.getId());
    }

    public Course getCourse(Integer id) {
        return courseRepository.getCourse(id);
    }

    public Course getCourse(String title, String targetLang, String sourceLang) {
        return courseRepository.getCourse(title, targetLang, sourceLang);
    }

    public List<Course> getCourses(String targetLang) {
        return courseRepository.getCourses(targetLang);
    }

    public List<Course> getCourses(String targetLang, String sourceLang) {
        return courseRepository.getCourses(targetLang, sourceLang);
    }


    public Topic createTopic(Topic topic) {
        return courseRepository.createTopic(topic);
    }

    public Topic updateTopic(Topic topic) {
        return courseRepository.updateTopic(topic);
    }

    public void deleteTopic(Topic topic) {
        courseRepository.deleteTopic(topic.getId());
    }

    public Topic getTopic(Integer id) {
        return courseRepository.getTopic(id);
    }


    public void updateTopicElements(Topic topic, List<TopicElement> elements) {
        courseRepository.updateTopicElements(topic, elements);
    }

    public void deleteTopicElements(Integer topicId, List<Integer> elementIds) {
        courseRepository.deleteTopicElements(topicId, elementIds);
    }


    public LearningCourse createLearningCourse(LearningCourse course) {
        return courseRepository.createLearningCourse(course);
    }

    public LearningCourse updateLearningCourse(LearningCourse course) {
        return courseRepository.updateLearningCourse(course);
    }

    public LearningCourse getLearningCourse(String username, String courseTitle, String targetLang, String sourcelang) {
        return courseRepository.getLearningCourse(username, courseTitle, targetLang, sourcelang);
    }

    public LearningCourse getLearningCourse(String username, Course course) {
        return courseRepository.getLearningCourse(username, course);
    }

    public List<LearningCourse> getLearningCourses(String username) {
        return courseRepository.getLearningCourses(username);
    }

    public List<LearningCourse> getLearningCourses(String username, String targetLang) {
        return courseRepository.getLearningCourses(username, targetLang);
    }

    public List<LearningCourse> getLearningCourses(String username, String targetLang, String sourceLang) {
        return courseRepository.getLearningCourses(username, targetLang, sourceLang);
    }

    public List<LangPair> getLangPairs() {
        return courseRepository.getLangPairs();
    }

    public List<LangPair> getLangPairs(String username) {
        return courseRepository.getLangPairs(username);
    }
}
