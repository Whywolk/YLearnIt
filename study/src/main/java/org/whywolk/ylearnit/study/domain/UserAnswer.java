/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022-2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.domain;

import java.util.List;
import java.util.Objects;

public class UserAnswer {
    private Card card;
    private String type;
    private Integer thinkingTime;
    private Integer totalTime;
    private Integer learningElementId;
    private Integer repetitionId;
    private String data;

    public UserAnswer(Card card, String type, Integer thinkingTime, Integer totalTime, Integer learningElementId, Integer repetitionId) {
        this.card = card;
        this.type = type;
        this.thinkingTime = thinkingTime;
        this.totalTime = totalTime;
        this.learningElementId = learningElementId;
        this.repetitionId = repetitionId;
    }

    public List<Side> getTestAnswers() {
        return card.getSides(SideType.ANSWER);
    }

    public void addAnswer(Side testAnswer) {
        this.card.addSide(testAnswer);
    }

    public Side getUserAnswer() {
        return card.getSides(SideType.USER_ANSWER).get(0);
    }

    public void setUserAnswer(Side userAnswer) {
        this.card.addSide(userAnswer);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getThinkingTime() {
        return thinkingTime;
    }

    public void setThinkingTime(Integer thinkingTime) {
        this.thinkingTime = thinkingTime;
    }

    public Integer getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(Integer totalTime) {
        this.totalTime = totalTime;
    }

    public String getDbName() {
        return card.getDbName();
    }

    public void setDbName(String dbName) {
        this.card.setDbName(dbName);
    }

    public Integer getElementIdDb() {
        return card.getElementIdDb();
    }

    public void setElementIdDb(Integer elementIdDb) {
        this.card.setElementIdDb(elementIdDb);
    }

    public Integer getLearningElementId() {
        return learningElementId;
    }

    public void setLearningElementId(Integer learningElementId) {
        this.learningElementId = learningElementId;
    }

    public Integer getRepetitionId() {
        return repetitionId;
    }

    public void setRepetitionId(Integer repetitionId) {
        this.repetitionId = repetitionId;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserAnswer that = (UserAnswer) o;
        return Objects.equals(card, that.card)
                && Objects.equals(type, that.type)
                && Objects.equals(thinkingTime, that.thinkingTime)
                && Objects.equals(totalTime, that.totalTime)
                && Objects.equals(learningElementId, that.learningElementId)
                && Objects.equals(repetitionId, that.repetitionId)
                && Objects.equals(data, that.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(card, type, thinkingTime, totalTime, learningElementId, repetitionId, data);
    }
}
