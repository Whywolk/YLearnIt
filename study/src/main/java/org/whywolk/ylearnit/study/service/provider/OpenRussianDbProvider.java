/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.service.provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.whywolk.ylearnit.openrussian.domain.Lang;
import org.whywolk.ylearnit.openrussian.domain.Level;
import org.whywolk.ylearnit.openrussian.domain.Word;
import org.whywolk.ylearnit.openrussian.service.OpenRussianService;
import org.whywolk.ylearnit.study.domain.Card;
import org.whywolk.ylearnit.study.entity.*;
import org.whywolk.ylearnit.study.repository.StudyRepository;
import org.whywolk.ylearnit.study.service.cardbuilder.CardBuilder;
import org.whywolk.ylearnit.study.service.cardbuilder.OpenRussianCardBuilder;

import java.util.*;

public class OpenRussianDbProvider implements DbProvider {
    private static final Logger log = LogManager.getLogger(OpenRussianDbProvider.class);
    private final OpenRussianService service;
    private final StudyRepository studyRepository;

    public OpenRussianDbProvider(OpenRussianService openRussianService, StudyRepository studyRepository) {
        this.studyRepository = studyRepository;
        this.service = openRussianService;
    }

    @Override
    public List<DbElement> getDbElements() {
        List<DbElement> dbElements = new ArrayList<>();
        for (Level level : Level.values()) {
            if (level != Level.NO_CATEGORY) {
                log.info(level);
                dbElements.addAll(createDbElements(service.getByLevel(level)));
            }
        }
        return dbElements;
    }

    @Override
    public List<Course> getCourses() {
        List<Course> courses = new ArrayList<>();
        Course c = new Course();
        c.setTitle("English CEFR");
        c.setTargetLang("en");
        c.setSourceLang("ru");

        int i = 1;
        for (Level level : Level.values()) {
            if (level != Level.NO_CATEGORY) {
                Topic t = new Topic();
                t.setTitle(level.getLevel());
                t.setNumber(i);
                t.setDifficult(i);
                c.addTopic(t);
                i++;
            }
        }
        courses.add(c);

        return courses;
    }

    @Override
    public List<TopicElement> getTopicElements(Topic topic) {
        if (topic.getId() == null) {
            String msg = "Topic='" + topic.getTitle() + "' not persisted";
            log.warn(msg);
            throw new RuntimeException(msg);
        }
        List<TopicElement> topicElements = new ArrayList<>();

        Level level = Level.valueOf(topic.getTitle());
        List<Word> words = service.getByLevel(level);
        List<DbElement> dbElements = createDbElements(words);

        List<Integer> ids = new ArrayList<>();
        words.forEach(w -> ids.add(w.getId()));
        List<DbElement> fetchedDbElements = studyRepository.getDbElements("openrussian", ids);

        fetchedDbElements.forEach(fetched -> {
            if (dbElements.contains(fetched)) {
                for (Element e : fetched.getElements()) {
                    if (e.getTargetLang().equals(topic.getCourse().getTargetLang())
                            && e.getSourceLangs().contains(topic.getCourse().getSourceLang())) {
                        topicElements.add(new TopicElement(topic, e, 1));
                    }
                }
            }
        });

        return topicElements;
    }

    @Override
    public CardBuilder getCardBuilder() {
        return new OpenRussianCardBuilder(service);
    }

    @Override
    public List<Card> find(String str, String targetLang) {
        List<Word> words = service.find(str, 0);
        OpenRussianCardBuilder cardBuilder = (OpenRussianCardBuilder) getCardBuilder();
        List<Card> cards = cardBuilder.buildCards(words);
        return cards;
    }

    @Override
    public List<Card> find(String str, String targetLang, String sourceLang) {
        List<Word> words = service.find(str, 0);
        OpenRussianCardBuilder cardBuilder = (OpenRussianCardBuilder) getCardBuilder();
        List<Card> cards = cardBuilder.buildCards(words, targetLang, sourceLang);
        return cards;
    }


    private DbElement createDbElement(Word word) {
        // TODO: fix to all langs (ru, de)
        DbElement dbElement = new DbElement("openrussian", word.getId());
        for (Lang lang : Lang.values()) {
            if (lang == Lang.en) {
                dbElement.addElement(new Element(lang.getCode(), new HashSet<>(Arrays.asList("ru"))));
            }
        }
//        dbElement.addElement(new Element("ru"));
        return dbElement;
    }

    private List<DbElement> createDbElements(List<Word> words) {
        List<DbElement> dbElements = new ArrayList<>();
        words.forEach(w -> dbElements.add(createDbElement(w)));
        return dbElements;
    }
}
