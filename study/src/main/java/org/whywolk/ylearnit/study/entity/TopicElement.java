/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.entity;

import jakarta.persistence.*;

import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "TOPIC_ELEMENT")
public class TopicElement implements Serializable {

    @EmbeddedId
    private TopicElementId topicElementId;

    @ManyToOne
    @MapsId("topicId")
    @JoinColumn(name = "TOPIC_ID")
    private Topic topic;

    @ManyToOne
    @MapsId("elementId")
    @JoinColumn(name = "ELEMENT_ID")
    private Element element;

    @Column(name = "NUMBER")
    private Integer number;

    public TopicElement() {
        this.topicElementId = new TopicElementId();
        this.number = 1;
    }

    public TopicElement(Topic topic, Element element, Integer number) {
        this();
        if (topic.getId() != null && element.getId() != null) {
            this.topicElementId = new TopicElementId(topic.getId(), element.getId());
            this.topic = topic;
            this.element = element;
            this.number = number;
        }
    }

    public TopicElementId getTopicElementId() {
        return topicElementId;
    }

    public void setTopicElementId(TopicElementId topicElementId) {
        this.topicElementId = topicElementId;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public Element getElement() {
        return element;
    }

    public void setElement(Element element) {
        this.element = element;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "TopicElement{" +
                "topicElementId=" + topicElementId +
                ", topic=" + topic +
                ", element=" + element +
                ", number=" + number +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TopicElement that = (TopicElement) o;
        return topicElementId.equals(that.topicElementId) && topic.equals(that.topic) && element.equals(that.element) && number.equals(that.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(topicElementId, topic, element, number);
    }
}
