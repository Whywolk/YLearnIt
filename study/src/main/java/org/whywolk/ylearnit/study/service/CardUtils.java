/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.service;

import org.whywolk.ylearnit.study.domain.ContentType;
import org.whywolk.ylearnit.study.domain.Side;

import java.util.*;

public class CardUtils {
    private static final Random random = new Random();

    public static Map<String, List<Side>> splitByTag(List<Side> sides) {
        Map<String, List<Side>> splited = new HashMap<>();
        for (Side s : sides) {
            if (! splited.containsKey(s.getTag())) {
                splited.put(s.getTag(), new ArrayList<>());
            }
            splited.get(s.getTag()).add(s);
        }
        return splited;
    }

    public static Map<String, List<Side>> splitByLang(List<Side> sides) {
        Map<String, List<Side>> splited = new HashMap<>();
        for (Side s : sides) {
            if (! splited.containsKey(s.getLang())) {
                splited.put(s.getLang(), new ArrayList<>());
            }
            splited.get(s.getLang()).add(s);
        }
        return splited;
    }

    public static List<Side> getAlternatives(Side original, List<Side> sides) {
        List<Side> alternatives = new ArrayList<>();
        for (Side s : sides) {
            if (!s.equals(original) && original.getTag().equals(s.getTag())) {
                alternatives.add(s);
            }
        }
        return alternatives;
    }

    public static void formatContent(Side side) {
        if (side.getContentType() == ContentType.TEXT_PLAIN) {
            String content = side.getContent();
            content = content.replaceAll("\\(.*?\\)", "");
            content = content.trim();
            side.setContent(content);
        }
    }

    public static Side getRandomSide(List<Side> sides) {
        return getRandomSide(sides, random);
    }

    public static Side getRandomSide(List<Side> sides, Random r) {
        return sides.get(r.nextInt(sides.size()));
    }

    public static <T> T getRandomKey(Set<T> keys) {
        return getRandomKey(keys, random);
    }

    public static <T> T getRandomKey(Set<T> keys, Random r) {
        List<T> keysList = keys.stream().toList();
        return keysList.get(r.nextInt(keysList.size()));
    }
}
