/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.whywolk.ylearnit.study.entity.LearningElement;

import java.util.ArrayList;
import java.util.List;

public class DetailedLearningElementDto {
    private Integer id;
    private ElementDto element;
    private RepetitionDto nextRepetition;
    private RepetitionDto lastRepetition;
    private List<RepetitionDto> repetitions;

    public DetailedLearningElementDto(LearningElement learningElement) {
        this.id = learningElement.getId();
        this.element = new ElementDto(learningElement.getElement());
        this.nextRepetition = new RepetitionDto(learningElement.getNextRepetition());
        if (learningElement.getLastRepetition() != null) {
            this.lastRepetition = new RepetitionDto(learningElement.getLastRepetition());
        }
        this.repetitions = new ArrayList<>();
        learningElement.getRepetitions().forEach(r -> this.repetitions.add(new RepetitionDto(r)));
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ElementDto getElement() {
        return element;
    }

    public void setElement(ElementDto element) {
        this.element = element;
    }

    @JsonProperty("nextRep")
    public RepetitionDto getNextRepetition() {
        return nextRepetition;
    }

    @JsonProperty("nextRep")
    public void setNextRepetition(RepetitionDto nextRepetition) {
        this.nextRepetition = nextRepetition;
    }

    @JsonProperty("lastRep")
    public RepetitionDto getLastRepetition() {
        return lastRepetition;
    }

    @JsonProperty("lastRep")
    public void setLastRepetition(RepetitionDto lastRep) {
        this.lastRepetition = lastRep;
    }

    public List<RepetitionDto> getRepetitions() {
        return repetitions;
    }

    public void setRepetitions(List<RepetitionDto> repetitions) {
        this.repetitions = repetitions;
    }
}
