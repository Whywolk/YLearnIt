/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2024.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.rest.resource;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.SecurityContext;
import org.whywolk.ylearnit.security.dto.UserPrincipal;
import org.whywolk.ylearnit.security.rest.Secured;
import org.whywolk.ylearnit.study.dto.ElementHistoryDto;
import org.whywolk.ylearnit.study.dto.HistoryDto;
import org.whywolk.ylearnit.study.entity.LearningCourse;
import org.whywolk.ylearnit.study.entity.LearningElement;
import org.whywolk.ylearnit.study.entity.LearningTopic;
import org.whywolk.ylearnit.study.service.StatsService;
import org.whywolk.ylearnit.study.service.StudyService;
import org.whywolk.ylearnit.utils.LocalDateRange;

import java.time.LocalDate;

@Tag(name = "Stats resource")
@Path("/stats")
public class StatsResource {
    private final StatsService statsService;
    private final StudyService studyService;

    @Inject
    public StatsResource(StatsService statsService, StudyService studyService) {
        this.statsService = statsService;
        this.studyService = studyService;
    }

    @GET
    @Path("/{targetLang}/{sourceLang}/{course}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get statistics by learning course")
    @Secured()
    @SecurityRequirement(name = "JWT")
    public HistoryDto getStats(@Context SecurityContext securityContext,
                               @PathParam("course") String courseTitle,
                               @PathParam("targetLang") String targetLang,
                               @PathParam("sourceLang") String sourceLang,
                               @QueryParam("startDate") String startDate,
                               @QueryParam("endDate") String endDate) {
        UserPrincipal userPrincipal = (UserPrincipal) securityContext.getUserPrincipal();
        Integer userId = userPrincipal.getId();
        String username = userPrincipal.getName();

        LearningCourse learningCourse = studyService.getLearningCourse(username, courseTitle, targetLang, sourceLang);
        LocalDateRange range = new LocalDateRange(LocalDate.parse(startDate), LocalDate.parse(endDate));

        return statsService.getHistory(learningCourse, range);
    }

    @GET
    @Path("/{targetLang}/{sourceLang}/{course}/{topic}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get statistics by learning topic")
    @Secured()
    @SecurityRequirement(name = "JWT")
    public HistoryDto getStats(@Context SecurityContext securityContext,
                               @PathParam("course") String courseTitle,
                               @PathParam("topic") String topicTitle,
                               @PathParam("targetLang") String targetLang,
                               @PathParam("sourceLang") String sourceLang,
                               @QueryParam("startDate") String startDate,
                               @QueryParam("endDate") String endDate) {
        UserPrincipal userPrincipal = (UserPrincipal) securityContext.getUserPrincipal();
        Integer userId = userPrincipal.getId();
        String username = userPrincipal.getName();

        LearningCourse learningCourse = studyService.getLearningCourse(username, courseTitle, targetLang, sourceLang);
        LearningTopic learningTopic = learningCourse.getTopic(topicTitle);
        LocalDateRange range = new LocalDateRange(LocalDate.parse(startDate), LocalDate.parse(endDate));

        return statsService.getHistory(learningTopic, range);
    }

    @GET
    @Path("/element/{leId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get statistics by learning element")
    @Secured()
    @SecurityRequirement(name = "JWT")
    public ElementHistoryDto getStats(@Context SecurityContext securityContext,
                                      @PathParam("leId") Integer leId) {
        UserPrincipal userPrincipal = (UserPrincipal) securityContext.getUserPrincipal();
        Integer userId = userPrincipal.getId();
        String username = userPrincipal.getName();

        LearningElement learningElement = studyService.getLearningElement(username, leId);

        return statsService.getHistory(learningElement);
    }
}
