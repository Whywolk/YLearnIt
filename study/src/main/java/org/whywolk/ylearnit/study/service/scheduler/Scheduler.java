/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022-2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.service.scheduler;

import org.whywolk.ylearnit.study.entity.LearningElement;
import org.whywolk.ylearnit.study.entity.Repetition;

import java.util.List;

public interface Scheduler {

    Repetition initRepetition();

    void predictInterval(LearningElement element);

    void predictIntervals(List<LearningElement> elements);
}
