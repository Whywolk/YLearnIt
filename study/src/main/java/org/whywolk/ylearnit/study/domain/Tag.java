/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2024.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.domain;

import java.util.Objects;

public class Tag {
    private String name;
    private String lang;
    private ContentType contentType;

    public Tag(String name) {
        this(name, "", ContentType.TEXT_PLAIN);
    }

    public Tag(String name, String lang, ContentType contentType) {
        this.name = name;
        this.lang = lang;
        this.contentType = contentType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public ContentType getContentType() {
        return contentType;
    }

    public void setContentType(ContentType contentType) {
        this.contentType = contentType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tag tag = (Tag) o;
        return Objects.equals(name, tag.name)
                && Objects.equals(lang, tag.lang)
                && contentType == tag.contentType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, lang, contentType);
    }
}
