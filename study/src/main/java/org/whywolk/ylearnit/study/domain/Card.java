/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022-2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Card implements Cloneable {
    private String dbName;
    private Integer elementIdDb;
    private List<Side> sides;

    public Card() {
        this("", 0);
    }

    public Card(String dbName, Integer elementIdDb) {
        this.dbName = dbName;
        this.elementIdDb = elementIdDb;
        this.sides = new ArrayList<>();
    }

    public void filter(String targetLang, String sourceLang) {
        List<Side> filteredSides = sides.stream()
                .filter(side -> side.getLang().equals(targetLang) || side.getLang().equals(sourceLang)).toList();
        this.sides = filteredSides;
    }

    public List<Side> getSides() {
        return sides;
    }

    public List<Side> getTestSides() {
        return getSides(SideType.TEST);
    }

    public List<Side> getInfoSides() {
        return getSides(SideType.INFO);
    }

    public List<Side> getSides(SideType sideType) {
        List<Side> sides = new ArrayList<>();
        for (Side s : this.sides) {
            if (s.getSideType() == sideType) {
                sides.add(s);
            }
        }
        return sides;
    }

    public List<Side> getSides(Tag tag) {
        return this.sides.stream().filter(side -> side.equalsTag(tag)).toList();
    }

    public boolean containsContent(String content, Tag tag) {
        return this.sides.stream().anyMatch(side -> side.containsContent(content, tag));
    }

    public boolean equalsContent(String content, Tag tag) {
        return this.sides.stream().anyMatch(side -> side.equalsContent(content, tag));
    }

    public void addSide(Side side) {
        this.sides.add(side);
    }

    public void removeSide(Side side) {
        this.sides.remove(side);
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public Integer getElementIdDb() {
        return elementIdDb;
    }

    public void setElementIdDb(Integer elementIdDb) {
        this.elementIdDb = elementIdDb;
    }

    @Override
    public String toString() {
        return "Card{" +
                "dbName='" + dbName + '\'' +
                ", elementIdDb=" + elementIdDb +
                ", sides=" + sides +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return sides.equals(card.sides)
                && dbName.equals(card.dbName)
                && elementIdDb.equals(card.elementIdDb);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sides, dbName, elementIdDb);
    }
}
