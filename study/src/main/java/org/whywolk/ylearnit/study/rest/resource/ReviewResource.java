/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2024.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.rest.resource;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.SecurityContext;
import org.whywolk.ylearnit.security.dto.UserPrincipal;
import org.whywolk.ylearnit.security.rest.Secured;
import org.whywolk.ylearnit.study.domain.StudyTest;
import org.whywolk.ylearnit.study.domain.UserAnswer;
import org.whywolk.ylearnit.study.domain.ReviewParams;
import org.whywolk.ylearnit.study.dto.UserAnswerDto;
import org.whywolk.ylearnit.study.repository.StudyRepository;
import org.whywolk.ylearnit.study.service.ReviewService;
import org.whywolk.ylearnit.study.service.StudyService;

import java.util.List;
import java.util.stream.Collectors;

@Tag(name = "Review resource")
@Path("/review")
public class ReviewResource {
    private final StudyService studyService;
    private final StudyRepository studyRepository;

    @Inject
    public ReviewResource(StudyService studyService, StudyRepository studyRepository) {
        this.studyService = studyService;
        this.studyRepository = studyRepository;
    }

    @POST
    @Path("/tests")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "Generate tests")
    @Secured()
    @SecurityRequirement(name = "JWT")
    public List<StudyTest> getTests(@Context SecurityContext securityContext,
                                    ReviewParams reviewParams) {
        UserPrincipal userPrincipal = (UserPrincipal) securityContext.getUserPrincipal();
        Integer userId = userPrincipal.getId();
        String username = userPrincipal.getName();

        ReviewService review = new ReviewService(studyService, studyRepository, reviewParams);
        review.setShuffle(reviewParams.isShuffle());

        return review.getTests(username);
    }

    @POST
    @Path("/answers")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "End tests")
    @Secured()
    @SecurityRequirement(name = "JWT")
    public void endTests(@Context SecurityContext securityContext,
                         List<UserAnswerDto> userAnswersDto) {
        UserPrincipal userPrincipal = (UserPrincipal) securityContext.getUserPrincipal();
        Integer userId = userPrincipal.getId();
        String username = userPrincipal.getName();

        List<UserAnswer> userAnswers = userAnswersDto.stream()
                .map(userAnswerDto -> userAnswerDto.toModel())
                .collect(Collectors.toList());

        ReviewService review = new ReviewService(studyService, studyRepository);
        review.endTests(username, userAnswers);
    }
}
