/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022-2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.entity;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Objects;

@Entity
@Table(name = "REPETITION")
public class Repetition implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "LOOP")
    private Integer loop;

    @Column(name = "SERIAL_NUMBER")
    private Integer serialNumber;

    @Column(name = "REP_DATE", columnDefinition = "DATE")
    private LocalDate date;

    @Column(name = "INTERVL", columnDefinition = "SMALLINT")
    private Integer interval;

    @ManyToOne
    @JoinColumn(name = "L_ELEMENT_ID")
    private LearningElement learningElement;

    @OneToOne(fetch = FetchType.EAGER, mappedBy = "repetition", cascade = CascadeType.ALL)
    private Answer answer;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "SCHEDULER_ATTRS_ID", referencedColumnName = "ID")
    private SchedulerAttributes schedulerAttributes;

    public Repetition() {
        loop = 1;
        serialNumber = 1;
        date = LocalDate.now(ZoneOffset.UTC);
        interval = 0;
    }

    public Repetition(Integer loop, Integer serialNumber, LocalDate date, Integer interval) {
        this.loop = loop;
        this.serialNumber = serialNumber;
        this.date = date;
        this.interval = interval;
    }

    public Integer getId() {
        return id;
    }

    public Integer getLoop() {
        return loop;
    }

    public void setLoop(Integer loop) {
        this.loop = loop;
    }

    public Integer getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(Integer serialNumber) {
        this.serialNumber = serialNumber;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Integer getInterval() {
        return interval;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    public LearningElement getLearningElement() {
        return learningElement;
    }

    public void setLearningElement(LearningElement learningElement) {
        this.learningElement = learningElement;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        answer.setRepetition(this);
        this.answer = answer;
    }

    public boolean hasAnswer() {
        return answer != null;
    }

    public SchedulerAttributes getSchedulerAttributes() {
        return schedulerAttributes;
    }

    public void setSchedulerAttributes(SchedulerAttributes schedulerAttributes) {
        this.schedulerAttributes = schedulerAttributes;
    }

    @Override
    public String toString() {
        return "Repetition{" +
                "id=" + id +
                ", loop=" + loop +
                ", serialNumber=" + serialNumber +
                ", date=" + date +
                ", interval=" + interval +
                ", answer=" + answer +
                ", schedulerAttributes=" + schedulerAttributes +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Repetition that = (Repetition) o;
        return Objects.equals(loop, that.loop)
                && Objects.equals(serialNumber, that.serialNumber)
                && Objects.equals(date, that.date)
                && Objects.equals(interval, that.interval)
                && Objects.equals(answer, that.answer)
                && Objects.equals(schedulerAttributes, that.schedulerAttributes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(loop, serialNumber, date, interval, answer, schedulerAttributes);
    }
}
