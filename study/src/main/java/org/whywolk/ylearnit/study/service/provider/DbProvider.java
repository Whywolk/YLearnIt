/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.service.provider;

import org.whywolk.ylearnit.study.domain.Card;
import org.whywolk.ylearnit.study.entity.Course;
import org.whywolk.ylearnit.study.entity.DbElement;
import org.whywolk.ylearnit.study.entity.Topic;
import org.whywolk.ylearnit.study.entity.TopicElement;
import org.whywolk.ylearnit.study.service.cardbuilder.CardBuilder;

import java.util.List;

public interface DbProvider {

    List<DbElement> getDbElements();

    List<Course> getCourses();

    List<TopicElement> getTopicElements(Topic topic);

    CardBuilder getCardBuilder();

    List<Card> find(String str, String targetLang);

    List<Card> find(String str, String targetLang, String sourceLang);
}
