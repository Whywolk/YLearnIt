/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.whywolk.ylearnit.study.entity.TopicElement;

import java.util.Objects;

public class TopicElementDto {
    private Integer elementId;
    private Integer number;

    public TopicElementDto() {
    }

    public TopicElementDto(Integer elementId) {
        this.elementId = elementId;
        this.number = 1;
    }

    public TopicElementDto(Integer elementId, Integer number) {
        this.elementId = elementId;
        this.number = number;
    }

    public TopicElementDto(TopicElement topicElement) {
        this(topicElement.getElement().getId(), topicElement.getNumber());
    }


    public Integer getElementId() {
        return elementId;
    }

    public void setElementId(Integer elementId) {
        this.elementId = elementId;
    }

    @JsonProperty("num")
    public Integer getNumber() {
        return number;
    }

    @JsonProperty("num")
    public void setNumber(Integer number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TopicElementDto that = (TopicElementDto) o;
        return elementId.equals(that.elementId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(elementId);
    }
}
