package org.whywolk.ylearnit.study.service.provider;

import org.whywolk.ylearnit.study.domain.Card;
import org.whywolk.ylearnit.study.entity.Course;
import org.whywolk.ylearnit.study.entity.DbElement;
import org.whywolk.ylearnit.study.entity.Topic;
import org.whywolk.ylearnit.study.entity.TopicElement;
import org.whywolk.ylearnit.study.service.cardbuilder.CardBuilder;
import org.whywolk.ylearnit.study.service.cardbuilder.XmlCardBuilder;
import org.whywolk.ylearnit.study.dao.XmlCardDao;

import java.util.List;

public class XmlDbProvider implements DbProvider {
    private final XmlCardDao cardDao;

    public XmlDbProvider(String filepath) {
        cardDao = new XmlCardDao(filepath);
    }

    @Override
    public List<DbElement> getDbElements() {
        return null;
    }

    @Override
    public List<Course> getCourses() {
        return null;
    }

    @Override
    public List<TopicElement> getTopicElements(Topic topic) {
        return null;
    }

    @Override
    public CardBuilder getCardBuilder() {
        return new XmlCardBuilder(cardDao);
    }

    @Override
    public List<Card> find(String str, String targetLang) {
        return cardDao.find(str, targetLang);
    }

    @Override
    public List<Card> find(String str, String targetLang, String sourceLang) {
        return cardDao.find(str, targetLang, sourceLang);
    }
}
