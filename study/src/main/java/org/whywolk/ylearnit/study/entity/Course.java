/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "COURSE")
public class Course implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @NotNull
    @Column(name = "TITLE")
    private String title;

    @NotNull
    @Column(name = "DESCRIPTION")
    private String description;

    @NotNull
    @Column(name = "TARGET_LANG")
    private String targetLang;

    @NotNull
    @Column(name = "SOURCE_LANG")
    private String sourceLang;

    @NotNull
    @Column(name = "DIFFICULT")
    private Integer difficult;

    @OrderBy(value = "number ASC")
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "course", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Topic> topics;

    public Course() {
        this.title = "";
        this.description = "";
        this.targetLang = "";
        this.sourceLang = "";
        this.difficult = 1;
        this.topics = new LinkedHashSet<>();
    }



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTargetLang() {
        return targetLang;
    }

    public void setTargetLang(String targetLang) {
        this.targetLang = targetLang;
    }

    public String getSourceLang() {
        return sourceLang;
    }

    public void setSourceLang(String sourceLang) {
        this.sourceLang = sourceLang;
    }

    public Integer getDifficult() {
        return difficult;
    }

    public void setDifficult(Integer difficult) {
        this.difficult = difficult;
    }

    public Set<Topic> getTopics() {
        return topics;
    }

    public Topic getTopic(String title) {
        for (Topic topic : topics) {
            if (topic.getTitle().equals(title)) {
                return topic;
            }
        }
        throw new RuntimeException("Topic '" + title + "' not found");
    }

    public boolean containsTopic(Topic topic) {
        return topics.contains(topic);
    }

    public boolean containsTopic(String title) {
        return topics.stream().anyMatch(topic -> topic.getTitle().equals(title));
    }

    public void addTopic(Topic topic) {
        if (topics.add(topic)) {
            topic.setCourse(this);
        }
    }

    public void removeTopic(Topic topic) {
        if (topics.remove(topic)) {
            topic.setCourse(null);
        }
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", targetLang='" + targetLang + '\'' +
                ", sourceLang='" + sourceLang + '\'' +
                ", difficult=" + difficult +
                ", topics=" + topics +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Course course = (Course) o;
        return title.equals(course.title)
                && description.equals(course.description)
                && targetLang.equals(course.targetLang)
                && sourceLang.equals(course.sourceLang)
                && difficult.equals(course.difficult)
                && topics.equals(course.topics);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, description, targetLang, sourceLang, difficult, topics);
    }
}
