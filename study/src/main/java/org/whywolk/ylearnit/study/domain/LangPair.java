/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.domain;

import java.util.Objects;

public class LangPair {
    private String targetLang;
    private String sourceLang;

    public LangPair(String targetLang, String sourceLang) {
        this.targetLang = targetLang;
        this.sourceLang = sourceLang;
    }

    public String getTargetLang() {
        return targetLang;
    }

    public void setTargetLang(String targetLang) {
        this.targetLang = targetLang;
    }

    public String getSourceLang() {
        return sourceLang;
    }

    public void setSourceLang(String sourceLang) {
        this.sourceLang = sourceLang;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LangPair langPair = (LangPair) o;
        return Objects.equals(targetLang, langPair.targetLang) && Objects.equals(sourceLang, langPair.sourceLang);
    }

    @Override
    public int hashCode() {
        return Objects.hash(targetLang, sourceLang);
    }
}
