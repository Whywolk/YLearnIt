/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022-2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.service.testbuilder;

import org.whywolk.ylearnit.study.domain.Card;
import org.whywolk.ylearnit.study.domain.Side;
import org.whywolk.ylearnit.study.domain.StudyTest;
import org.whywolk.ylearnit.study.service.CardUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class TestBuilder {

    public abstract String getType();

    public StudyTest getTest(Card card) {
        List<Side> sides = card.getTestSides();

        if (sides.size() < 2) {
            throw new RuntimeException("Necessary 2 or more TEST sides");
        }

        Map<String, List<Side>> splited = CardUtils.splitByLang(sides);
        if (splited.size() < 2) {
            throw new RuntimeException("Necessary 2 or more TEST unique sides");
        }

        StudyTest test = buildTest(card);

        test.setType(getType());
        test.setDbName(card.getDbName());
        test.setElementIdDb(card.getElementIdDb());

        return test;
    }

    public List<StudyTest> getTest(List<Card> cards) {
        return cards.stream().map(card -> getTest(card)).collect(Collectors.toList());
    }

    protected abstract StudyTest buildTest(Card card);
}
