/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2024.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.domain;

public enum ContentType {
    TEXT_PLAIN("text/plain");

    private final String code;

    ContentType(String code) {
        this.code = code;
    }

    public static ContentType valueOfCode(String code) {
        for (ContentType type : ContentType.values()) {
            if (type.getCode().equals(code)) {
                return type;
            }
        }
        throw new RuntimeException("No type with code " + code);
    }

    public String getCode() {
        return code;
    }
}
