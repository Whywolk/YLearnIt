/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.dto;

import org.whywolk.ylearnit.study.entity.Course;
import org.whywolk.ylearnit.study.entity.LearningCourse;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CourseUserDto {
    private Integer id;
    private String title;
    private String description;
    private String targetLang;
    private String sourceLang;
    private Integer difficult;
    private Boolean isAdded;
    private Integer lCourseId;
    private List<TopicUserDto> topics;

    public CourseUserDto(Course c) {
        this.id = c.getId();
        this.title = c.getTitle();
        this.description = c.getDescription();
        this.targetLang = c.getTargetLang();
        this.sourceLang = c.getSourceLang();
        this.difficult = c.getDifficult();
        this.topics = c.getTopics().stream().map(t -> new TopicUserDto(t)).collect(Collectors.toList());
        this.isAdded = false;
    }

    public CourseUserDto(LearningCourse lc) {
        this(lc.getCourse());
        this.isAdded = true;
        this.lCourseId = lc.getId();

        this.topics.forEach(topicUserDto -> {
            lc.getTopics().forEach(lt -> {
                if (lt.getTopic().getId().equals(topicUserDto.getId())) {
                    topicUserDto.setAdded(true);
                    topicUserDto.setlTopicId(lt.getId());
                }
            });
        });
    }

    public static List<CourseUserDto> toDto(List<Course> courses, List<LearningCourse> learningCourses) {
        Map<Course, LearningCourse> courseMap = learningCourses.stream()
                .collect(Collectors.toMap(lc -> lc.getCourse(), lc -> lc));

        List<CourseUserDto> courseDtos = courses.stream()
                .map(c -> {
                    if (courseMap.containsKey(c)) {
                        return new CourseUserDto(courseMap.get(c));
                    } else {
                        return new CourseUserDto(c);
                    }
                }).collect(Collectors.toList());
        return courseDtos;
    }

    public static List<CourseUserDto> toDto(List<LearningCourse> learningCourses) {
        List<CourseUserDto> courseDtos = learningCourses.stream()
                .map(lc -> new CourseUserDto(lc)).collect(Collectors.toList());
        return courseDtos;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTargetLang() {
        return targetLang;
    }

    public void setTargetLang(String targetLang) {
        this.targetLang = targetLang;
    }

    public String getSourceLang() {
        return sourceLang;
    }

    public void setSourceLang(String sourceLang) {
        this.sourceLang = sourceLang;
    }

    public Integer getDifficult() {
        return difficult;
    }

    public void setDifficult(Integer difficult) {
        this.difficult = difficult;
    }

    public Boolean getAdded() {
        return isAdded;
    }

    public void setAdded(Boolean added) {
        isAdded = added;
    }

    public Integer getlCourseId() {
        return lCourseId;
    }

    public void setlCourseId(Integer lCourseId) {
        this.lCourseId = lCourseId;
    }

    public List<TopicUserDto> getTopics() {
        return topics;
    }

    public void setTopics(List<TopicUserDto> topics) {
        this.topics = topics;
    }
}
