/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022-2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study.service.scheduler;

import java.util.Objects;

public class SM2Algorithm {

    private int prevInterval;
    private int interval;
    private int prevEF;
    private int EF;

    public SM2Algorithm() {
        prevInterval = 0;
        prevEF = 2500;
        interval = prevInterval;
        EF = prevEF;
    }

    public SM2Algorithm(int interval, int EF) {
        this.prevInterval = interval;
        this.prevEF = EF;
        this.interval = interval;
        this.EF = EF;
    }

    public void calcInterval(int quality) {
        prevInterval = interval;
        prevEF = EF;

        EF = calcEasinessFactor(quality);

        if (prevInterval < 0) {
            throw new RuntimeException("Interval cannot be less then 0");
        }
        if (prevInterval == 0 || quality < 3) {
            interval = 1;
            return;
        }
        if (prevInterval == 1) {
            interval = 6;
            return;
        }
        interval = Math.round(prevInterval * EF / 1000f);
    }

    public int calcEasinessFactor(int quality) {
        int delta;
        switch (quality) {
            case 5 -> delta = 100;
            case 4 -> delta = 0;
            case 3 -> delta = -140;
            case 0, 1, 2 -> delta = 0;
            default -> throw new IllegalStateException("Wrong quality=" + quality);
        }
        int curEF = prevEF + delta;
        if (curEF < 1300) {
            curEF = 1300;
        } else if (curEF > 2500) {
            curEF = 2500;
        }
        return curEF;
    }

    public int getPrevInterval() {
        return prevInterval;
    }

    public void setPrevInterval(int prevInterval) {
        this.prevInterval = prevInterval;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public int getPrevEF() {
        return prevEF;
    }

    public void setPrevEF(int prevEF) {
        this.prevEF = prevEF;
    }

    public int getEF() {
        return EF;
    }

    public void setEF(int EF) {
        this.EF = EF;
    }

    @Override
    public String toString() {
        return "SM2Algorithm{" +
                "prevInterval=" + prevInterval +
                ", interval=" + interval +
                ", prevEF=" + prevEF +
                ", EF=" + EF +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SM2Algorithm that = (SM2Algorithm) o;
        return prevInterval == that.prevInterval
                && interval == that.interval
                && prevEF == that.prevEF
                && EF == that.EF;
    }

    @Override
    public int hashCode() {
        return Objects.hash(prevInterval, interval, prevEF, EF);
    }
}
