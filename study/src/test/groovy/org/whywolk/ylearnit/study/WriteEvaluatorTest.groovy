/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.whywolk.ylearnit.study.domain.Card
import org.whywolk.ylearnit.study.domain.ContentType
import org.whywolk.ylearnit.study.domain.Side
import org.whywolk.ylearnit.study.domain.SideType
import org.whywolk.ylearnit.study.domain.UserAnswer
import org.whywolk.ylearnit.study.service.evaluator.AnswerEvaluator
import org.whywolk.ylearnit.study.service.evaluator.WriteEvaluator

import static org.junit.jupiter.api.Assertions.*

class WriteEvaluatorTest {
    private AnswerEvaluator evaluator

    @BeforeEach
    void setup() {
        evaluator = new WriteEvaluator()
    }

    @Test
    void evaluate_oneAnswer_correct() {
        def userAnswer = getUserAnswer("WORLD", ["world"], 5)
        def answer = evaluator.evaluate(userAnswer)
        assertEquals(100, answer.getAccuracy())
        assertEquals(92, answer.getRetrievability())
    }

    @Test
    void evaluate_twoAnswers_correct() {
        def userAnswer = getUserAnswer("WORLD, hello", ["world", "hello"], 10)
        def answer = evaluator.evaluate(userAnswer)
        assertEquals(100, answer.getAccuracy())
        assertEquals(83, answer.getRetrievability())
    }

    @Test
    void evaluate_oneAnswer_twoVariants_correct() {
        def userAnswer = getUserAnswer("live", ["alive", "live"], 10)
        def answer = evaluator.evaluate(userAnswer)
        assertEquals(100, answer.getAccuracy())
        assertEquals(83, answer.getRetrievability())
    }

    @Test
    void evaluate_oneAnswer_incorrect() {
        def userAnswer = getUserAnswer("world", ["hello"], 10)
        def answer = evaluator.evaluate(userAnswer)
        assertEquals(20, answer.getAccuracy())
        assertEquals(3, answer.getRetrievability())
    }

    @Test
    void evaluate_twoAnswers_twoVariants_oneCorrect() {
        def userAnswer = getUserAnswer("one, two", ["one", "six"], 10)
        def answer = evaluator.evaluate(userAnswer)
        assertEquals(50, answer.getAccuracy())
        assertEquals(33, answer.getRetrievability())
    }

    @Test
    void evaluate_longThinkingTime() {
        def userAnswer = getUserAnswer("hello", ["hello"], 90)
        def answer = evaluator.evaluate(userAnswer)
        assertEquals(100, answer.getAccuracy())
        assertEquals(0, answer.getRetrievability())
    }

    @Test
    void evaluate_throwException_onNonWriteTestType() {
        def userAnswer = getUserAnswer("hello", ["hello"], 5)
        userAnswer.setType("other")
        assertThrows(Exception, () -> {
            evaluator.evaluate(userAnswer)
        })
    }


    private UserAnswer getUserAnswer(String userAnswer, List<String> correctAnswers, Integer thinkingTime) {
        def u = new UserAnswer(new Card(), "WRITE", thinkingTime, 0 , 0, 0)
        u.setUserAnswer(getSideWithContent(userAnswer, SideType.USER_ANSWER))
        correctAnswers.forEach(correctAnswer -> u.addAnswer(getSideWithContent(correctAnswer, SideType.ANSWER)))
        return u
    }

    private Side getSideWithContent(String content, SideType sideType) {
        return new Side("tag", content, ContentType.TEXT_PLAIN, "en", sideType)
    }
}
