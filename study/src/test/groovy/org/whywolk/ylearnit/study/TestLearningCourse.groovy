/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.whywolk.ylearnit.study.entity.Course
import org.whywolk.ylearnit.study.entity.DbElement
import org.whywolk.ylearnit.study.entity.Element
import org.whywolk.ylearnit.study.entity.LearningCourse
import org.whywolk.ylearnit.study.entity.Topic
import org.whywolk.ylearnit.study.entity.TopicElement
import org.whywolk.ylearnit.study.entity.UserData
import org.whywolk.ylearnit.study.repository.StudyRepository
import org.whywolk.ylearnit.study.service.StudyService

import static org.junit.jupiter.api.Assertions.*

class TestLearningCourse {
    private static final Logger log = LogManager.getLogger(TestLearningCourse)

    private final StudyRepository repository = new StudyRepository()
    private final StudyService service = new StudyService(repository)
    private UserData user

    @BeforeEach
    void setup() {
        StudyRepository.setEntityManagerFactory("study-unit")

        List dbElements = getDbElements("example_db", ["ru", "en", "de"] as Set)
        Course c = getCourse("some-course-en", "en", "ru")
        Course cCross = getCourse("cross-course-de", "de", "en")

        user = new UserData("test-name")
        repository.createUserData(user)

        repository.createDbElements(dbElements)
        repository.createCourse(c)
        repository.createCourse(cCross)

        List elementsEn = getElementsByTargetLang(dbElements, "en")

        List<List<Element>> elEnParts = [
                elementsEn.subList(0, 3),
                elementsEn.subList(3, 6)
        ]

        List elementsDe = getElementsByTargetLang(dbElements, "de")
        // 0 1 2
        //   1 2 3 4 5
        List<List<Element>> elDePartsCross = [
                elementsDe.subList(0, 3),
                elementsDe.subList(1, 6)
        ]

        for (i in 0..<c.getTopics().size()) {
            def topic = c.getTopics()[i]
            def topicElements = []
            def num = 3
            for (el in elEnParts[i]) {
                topicElements.add(new TopicElement(topic, el, num--))
            }
            repository.updateTopicElements(topic, topicElements)
        }

        for (i in 0..<cCross.getTopics().size()) {
            def topic = cCross.getTopics()[i]
            def topicElements = []
            for (el in elDePartsCross[i]) {
                topicElements.add(new TopicElement(topic, el, 1))
            }
            repository.updateTopicElements(topic, topicElements)
        }
    }

    @Test
    void testAddLearningCourse() {
        Course c = repository.getCourse("some-course-en")
        Course cDe = repository.getCourse("cross-course-de")
        service.addLearningCourse(user, c)
        service.addLearningCourse(user, cDe)

        def lcs = repository.getLearningCoursesByUserAndTargetLang(user.getId(), "en")
        assertEquals(1, lcs.size())

        def lc = lcs[0]
        assertEquals(c, lc.getCourse())
        assertEquals(true, lc.isStudying())
        assertEquals(1, lc.getTopics().size())
        assertEquals(c.getTopics()[0], lc.getTopics()[0].getTopic())

        def les = repository.getLearningElements(user.getId(), lc.getTopics()[0].getId())

        def expectedElements = repository.getElementsByTopic(lc.getTopics()[0].getId())
        def actualElements = []
        les.forEach(e -> actualElements.add(e.getElement()))

        assertEquals(expectedElements, actualElements)
    }

    @Test
    void testAddSameLearningCourse() {
        Course c = repository.getCourse("some-course-en")
        service.addLearningCourse(user.getId(), c.getId())

        Exception e = assertThrows(Exception, () -> {
            service.addLearningCourse(user.getId(), c.getId())
        })

        assertEquals("UserId=${user.getId()} Course with id=${c.getId()} already added" as String, e.getMessage())
    }

    @Test
    void testAddTopicForLearningCourse() {
        Course c = repository.getCourse("some-course-en")
        service.addLearningCourse(user.getId(), c.getId())

        LearningCourse lc = repository.getLearningCourse(user.getId(), c.getId())
        Topic addingTopic = c.getTopics()[1]
        service.addTopicForLearningCourse(lc.getId(), addingTopic.getId())

        lc = repository.getLearningCourse(user.getId(), c.getId())
        assertEquals(c, lc.getCourse())

        for (i in 0..<lc.getTopics().size()) {
            assertEquals(c.getTopics()[i], lc.getTopics()[i].getTopic())

            def les = repository.getLearningElements(user.getId(), lc.getTopics()[i].getId())
            def expectedElements = repository.getElementsByTopic(lc.getTopics()[i].getId())
            def actualElements = []
            les.forEach(e -> actualElements.add(e.getElement()))
            assertEquals(expectedElements, actualElements)
        }
    }

    @Test
    void testAddSameTopicForLearningCourse() {
        Course c = repository.getCourse("some-course-en")
        service.addLearningCourse(user.getId(), c.getId())

        LearningCourse lc = repository.getLearningCourse(user.getId(), c.getId())
        Topic addingTopic = c.getTopics()[0]

        Exception e = assertThrows(Exception, () -> {
            service.addTopicForLearningCourse(lc.getId(), addingTopic.getId())
        })

        assertEquals("Topic id=${addingTopic.getId()} already added in LearningCourse id=${lc.getId()}" as String, e.getMessage())
    }

    @Test
    void testAddTopicWithCrossingElements() {
        Course c = repository.getCourse("cross-course-de")
        service.addLearningCourse(user.getId(), c.getId())

        LearningCourse lc = repository.getLearningCourse(user.getId(), c.getId())
        Topic addingTopic = c.getTopics()[1]

        def lesCourse = repository.getLearningElementsByLearningCourse(user.getId(), lc.getId())
        def lesTopic = repository.getLearningElements(user.getId(), lc.getTopics()[0].getId())
        assertEquals(3, lesCourse.size())
        assertEquals(3, lesTopic.size())

//        def notAddedElements = service.getNotAddedLearningElementsByTopic(lesTopic, addingTopic)
//        assertEquals(4, notAddedElements.size())

        service.addTopicForLearningCourse(lc.getId(), addingTopic.getId())

        lc = repository.getLearningCourse(user.getId(), c.getId())
        assertEquals(c, lc.getCourse())
        assertEquals(true, lc.isStudying())
        assertEquals(2, lc.getTopics().size())
        for (i in 0..<lc.getTopics().size()) {
            assertEquals(c.getTopics()[i], lc.getTopics()[i].getTopic())
        }

        def les = repository.getLearningElementsByLearningCourse(user.getId(), lc.getId())
        def expectedElements = repository.getElements("example_db", "de")

        assertEquals(expectedElements.size(), les.size())
        for (i in 0..<expectedElements.size()) {
            assertEquals(expectedElements[i], les[i].getElement())
        }
    }

    @Test
    void testGetLearningElements() {
        Course c = repository.getCourse("some-course-en")
        service.addLearningCourse(user.getId(), c.getId())

        def lc = repository.getLearningCourse(user, c.getId())
        assertEquals(c, lc.getCourse())
        assertEquals(true, lc.isStudying())
        assertEquals(1, lc.getTopics().size())
        assertEquals(c.getTopics()[0], lc.getTopics()[0].getTopic())

        def lesSizeExpected = c.getTopics()[0].getElements().size()
        def lesCourse = repository.getLearningElementsByLearningCourse(user.getId(), lc.getId())
        def lesTopic = repository.getLearningElements(user.getId(), lc.getTopics()[0].getId())
        assertEquals(lesSizeExpected, lesCourse.size())
        assertEquals(lesSizeExpected, lesTopic.size())
    }

    @Test
    void testGetLearningElements2() {
        Course c = repository.getCourse("cross-course-de")
        service.addLearningCourse(user.getId(), c.getId())

        def lc = repository.getLearningCourse(user.getId(), c.getId())
        Topic addingTopic = c.getTopics()[1]
        service.addTopicForLearningCourse(lc.getId(), addingTopic.getId())
        lc = repository.getLearningCourse(user.getId(), c.getId())

        assertEquals(c, lc.getCourse())
        assertEquals(true, lc.isStudying())
        assertEquals(2, lc.getTopics().size())

        def lesCourseSizeExpected = 7
        def lesCourse = repository.getLearningElementsByLearningCourse(user.getId(), lc.getId())

        assertEquals(lesCourseSizeExpected, lesCourse.size())
        for (i in 0..1) {
            def lesTopic = repository.getLearningElements(user.getId(), lc.getTopics()[i].getId())
            assertEquals(c.getTopics()[i].getElements().size(), lesTopic.size())
        }

    }

    private static List<DbElement> getDbElements(String dbName, Set<String> targetLangs) {
        log.info("Creating DbElements")
        List ids = [1, 2, 3, 10, 11, 15]
        List dbElements = []

        for (id in ids) {
            DbElement dbElement = new DbElement(dbName, id)
            for (targetLang in targetLangs) {
                def sourceLangs = targetLangs.clone() as Set
                sourceLangs.remove(targetLang)
                dbElement.addElement(new Element(targetLang, sourceLangs))
            }
            dbElements.add(dbElement)
        }

        return dbElements
    }

    private static Course getCourse(String name, String targetLang, String sourceLang) {
        log.info("Creating Course")
        Course c = new Course()
        c.setTitle(name)
        c.setDescription(name + "-description")
        c.setTargetLang(targetLang)
        c.setSourceLang(sourceLang)

        for (i in 1..2) {
            Topic t = new Topic()
            t.setTitle(targetLang + "-topic-" + i)
            t.setDescription("topic description")
            t.setNumber(i)
            t.setDifficult(i)
            c.addTopic(t)
        }

        return c
    }

    private static List<Element> getElementsByTargetLang(List<DbElement> dbElements, String targetLang) {
        def elements = []
        for (de in dbElements) {
            for (e in de.getElements()) {
                if (e.getTargetLang() == targetLang) {
                    elements.add(e)
                }
            }
        }
        return elements
    }
}
