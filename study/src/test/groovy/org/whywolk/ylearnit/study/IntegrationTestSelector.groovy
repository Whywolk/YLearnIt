/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.whywolk.ylearnit.study.entity.DbElement
import org.whywolk.ylearnit.study.entity.Element
import org.whywolk.ylearnit.study.entity.LearningElement
import org.whywolk.ylearnit.study.entity.Repetition
import org.whywolk.ylearnit.study.entity.UserData
import org.whywolk.ylearnit.study.repository.StudyRepository

import java.time.LocalDate

import static org.junit.jupiter.api.Assertions.*

class IntegrationTestSelector {
    private static final StudyRepository repository = new StudyRepository()

    private static DbElement dbEl1
    private static Element el11
    private static Element el12
    private static DbElement dbEl2
    private static Element el21
    private static Element el22
    private static UserData user
    private static LocalDate curDate
    private static LocalDate dateBefore
    private static LocalDate dateAfter

    @BeforeAll
    static void setup() {
        curDate = LocalDate.of(2023, 02, 05)
        dateBefore = LocalDate.of(2023, 02, 01)
        dateAfter = LocalDate.of(2023, 02, 10)
    }

    @BeforeEach
    void setupEach() {
        StudyRepository.setEntityManagerFactory("study-unit")

        el11 = new Element("en", ["ru"] as Set)
        el12 = new Element("de", ["ru"] as Set)
        dbEl1 = new DbElement("test_db", 5)
        dbEl1.setElements([el11, el12] as Set)

        el21 = new Element("en", ["ru"] as Set)
        el22 = new Element("de", ["ru"] as Set)
        dbEl2 = new DbElement("test_db", 10)
        dbEl2.setElements([el21, el22] as Set)

        user = new UserData("test_user")

        repository.createDbElements([dbEl1, dbEl2])
        repository.createUserData(user)
    }

    @Test
    void getWeakElements_whenNoWeakElements() {
        def le = getLearningElement(el11, dateAfter)
        repository.createLearningElements([le])

        def weakElements = repository.getWeakElements(curDate, user.getName(), "en", "ru")

        assertTrue(weakElements.isEmpty())
    }

    @Test
    void getWeakElements_whenOneWeakElement() {
        def leBefore = getLearningElement(el11, dateBefore)
        def leAfter = getLearningElement(el21, dateAfter)
        repository.createLearningElements([leBefore, leAfter])

        def weakElements = repository.getWeakElements(curDate, user.getName(), "en", "ru")

        assertEquals(1, weakElements.size())
        assertEquals([leBefore], weakElements)
    }

    @Test
    void getWeakElements_whenTwoWeakElement() {
        def leBefore = getLearningElement(el21, dateBefore)
        def leCur = getLearningElement(el21, curDate)
        repository.createLearningElements([leBefore, leCur])

        def weakElements = repository.getWeakElements(curDate, user.getName(), "en", "ru")

        assertEquals(2, weakElements.size())
        assertEquals([leBefore, leCur], weakElements)
    }


    private LearningElement getLearningElement(Element element, LocalDate date) {
        def le = new LearningElement(element, user)

        def r = new Repetition()
        r.setDate(date)
        le.addRepetition(r)

        return le
    }
}
