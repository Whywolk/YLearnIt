/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.whywolk.ylearnit.study.domain.SM2Attributes
import org.whywolk.ylearnit.study.entity.Answer
import org.whywolk.ylearnit.study.entity.LearningElement
import org.whywolk.ylearnit.study.entity.Repetition
import org.whywolk.ylearnit.study.entity.SchedulerAttributes
import org.whywolk.ylearnit.study.service.scheduler.SM2Scheduler
import org.whywolk.ylearnit.study.service.scheduler.Scheduler

import java.time.LocalDate
import java.time.ZoneOffset

import static org.junit.jupiter.api.Assertions.assertEquals

class SM2SchedulerTest {
    private static final String type = "SM2"
    private Scheduler scheduler

    @BeforeEach
    void setup() {
        scheduler = new SM2Scheduler()
    }

    @Test
    void initRepetition() {
        Repetition expected = new Repetition()
        expected.setSchedulerAttributes(getSchedulerAttributes(2500))
        Repetition actual = scheduler.initRepetition()

        assertEquals(expected, actual)
    }

    @Test
    void predictInterval_whenFirstRepetition() {
        LearningElement le = new LearningElement()
        Repetition r = scheduler.initRepetition()
        Answer answer = new Answer()
        answer.setRetrievability(90)
        r.setAnswer(answer)
        le.addRepetition(r)

        scheduler.predictInterval(le)

        Repetition expected = new Repetition(1, 2, getCurrentDate().plusDays(1), 1)
        expected.setSchedulerAttributes(getSchedulerAttributes(2500))
        Repetition actual = le.getNextRepetition()

        assertEquals(expected, actual)
    }

    @Test
    void predictInterval_whenSecondRepetition() {
        LearningElement le = new LearningElement()
        Repetition r = scheduler.initRepetition()
        Answer answer1 = new Answer()
        answer1.setRetrievability(75)
        r.setAnswer(answer1)
        le.addRepetition(r)

        scheduler.predictInterval(le)

        Repetition expected1 = new Repetition(1, 2, getCurrentDate().plusDays(1), 1)
        expected1.setSchedulerAttributes(getSchedulerAttributes(2360))
        Repetition actual1 = le.getNextRepetition()

        assertEquals(expected1, actual1)

        Answer answer2 = new Answer()
        answer2.setRetrievability(75)
        actual1.setAnswer(answer2)

        scheduler.predictInterval(le)

        Repetition expected2 = new Repetition(1, 3, getCurrentDate().plusDays(6), 6)
        expected2.setSchedulerAttributes(getSchedulerAttributes(2220))
        Repetition actual2 = le.getNextRepetition()

        assertEquals(expected2, actual2)
    }

    @Test
    void predictInterval_whenBadRepetition() {
        LearningElement le = new LearningElement()
        Repetition r = scheduler.initRepetition()
        Answer answer = new Answer()
        answer.setRetrievability(20)
        r.setAnswer(answer)
        le.addRepetition(r)

        scheduler.predictInterval(le)

        Repetition expected = new Repetition(2, 1, getCurrentDate().plusDays(1), 1)
        expected.setSchedulerAttributes(getSchedulerAttributes(2500))
        Repetition actual = le.getNextRepetition()

        assertEquals(expected, actual)
    }

    @Test
    void predictInterval_whenSecondBadRepetition() {
        LearningElement le = new LearningElement()
        Repetition r = scheduler.initRepetition()
        Answer answer1 = new Answer()
        answer1.setRetrievability(75)
        r.setAnswer(answer1)
        le.addRepetition(r)

        scheduler.predictInterval(le)

        Repetition expected1 = new Repetition(1, 2, getCurrentDate().plusDays(1), 1)
        expected1.setSchedulerAttributes(getSchedulerAttributes(2360))
        Repetition actual1 = le.getNextRepetition()

        assertEquals(expected1, actual1)

        Answer answer2 = new Answer()
        answer2.setRetrievability(20)
        actual1.setAnswer(answer2)

        scheduler.predictInterval(le)

        Repetition expected2 = new Repetition(2, 1, getCurrentDate().plusDays(1), 1)
        expected2.setSchedulerAttributes(getSchedulerAttributes(2360))
        Repetition actual2 = le.getNextRepetition()

        assertEquals(expected2, actual2)
    }


    private SchedulerAttributes getSchedulerAttributes(int easinessFactor) {
        return new SchedulerAttributes(type, SM2Attributes.toJson(new SM2Attributes(easinessFactor)))
    }

    private LocalDate getCurrentDate() {
        return LocalDate.now(ZoneOffset.UTC)
    }
}
