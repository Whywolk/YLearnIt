/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.whywolk.ylearnit.study.entity.Course
import org.whywolk.ylearnit.study.entity.DbElement
import org.whywolk.ylearnit.study.entity.Element
import org.whywolk.ylearnit.study.entity.Topic
import org.whywolk.ylearnit.study.entity.TopicElement
import org.whywolk.ylearnit.study.repository.StudyRepository

import static org.junit.jupiter.api.Assertions.*

class TestDbElementsCourse {
    private static final Logger log = LogManager.getLogger(TestDbElementsCourse)

    private static final StudyRepository repository = new StudyRepository()

    @BeforeEach
    void setup() {
        StudyRepository.setEntityManagerFactory("study-unit")
    }

    @Test
    void testCreateDbElements() {
        List<DbElement> expected = getDbElements("example_db", ["ru", "en", "de"] as Set)

        log.info("DbElements persist transaction begin")
        repository.createDbElements(expected)
        log.info("DbElements persist transaction commit")

        log.info("Select DbElements by database name")
        List<DbElement> actual = repository.getDbElements("example_db")
        log.info("DbElements selected")

        assertEquals(expected, actual)
    }

    // Summary: it's better to merge only changed entities
    @Test
    void testUpdateDbElements() {
        List<DbElement> expected = getDbElements("example_db", ["en", "de"] as Set)

        log.info("DbElements persist transaction begin")
        repository.createDbElements(expected)
        log.info("DbElements persist transaction commit")

        log.info("Select DbElements by database name")
        expected = repository.getDbElements("example_db")
        log.info("DbElements selected")

        expected[0].addElement(new Element("ru", ["en"] as Set))
        List<DbElement> changed = [expected[0]]

        log.info("DbElement merge transaction begin")
        repository.updateDbElements(changed)
        log.info("DbElement merge transaction commit")

        log.info("Select DbElements by database name")
        List<DbElement> actual = repository.getDbElements("example_db")
        log.info("DbElements selected")

        assertEquals(expected, actual)
    }

    @Test
    void testCreateCourse() {
        Course expected = getCourse("some-course", "en", "ru")

        log.info("Course persist transaction begin")
        repository.createCourse(expected)
        log.info("Course persist transaction commit")

        log.info("Select Course by name")
        Course actual = repository.getCourse("some-course")
        log.info("Course selected")

        assertEquals(expected, actual)
    }

    @Test
    void testInsertElementsIntoTopics() {
        List<DbElement> dbElements = getDbElements("example_db", ["ru", "en", "de"] as Set)
        Course course = getCourse("some-course-en", "en", "ru")

        log.info("Create DbElements and Courses in DB")
        repository.createDbElements(dbElements)
        repository.createCourse(course)

        log.info("Get Elements from DB")
        List<Element> elEn = repository.getElements("example_db", "en")

        log.info("Get Courses from DB")
        course = repository.getCourse("some-course-en")

        List<List<Element>> elEnParts = [
                elEn.subList(0, 3),
                elEn.subList(3, 6)
        ]

        for (i in 0..<course.getTopics().size()) {
            def topic = course.getTopics()[i]
            def topicElements = []
            def num = 3
            for (element in elEnParts[i]) {
                def topicElement = new TopicElement(topic, element, num--)
                topicElements.add(topicElement)
            }
            log.info("Persist TopicElements")
            repository.updateTopicElements(topic, topicElements)
        }

        for (i in 0..<course.getTopics().size()) {
            def topic = course.getTopics()[i]
            List<Element> expected = elEnParts[i].reverse()
            List<Element> actual = repository.getElementsByTopic(topic.getId())
            assertEquals(expected, actual)
        }
    }


    private static List<DbElement> getDbElements(String dbName, Set<String> targetLangs) {
        log.info("Creating DbElements")
        List ids = [1, 2, 3, 10, 11, 15]
        List dbElements = []

        for (id in ids) {
            DbElement dbElement = new DbElement(dbName, id)
            for (targetLang in targetLangs) {
                def sourceLangs = targetLangs.clone() as Set
                sourceLangs.remove(targetLang)
                dbElement.addElement(new Element(targetLang, sourceLangs))
            }
            dbElements.add(dbElement)
        }

        return dbElements
    }

    private static Course getCourse(String name, String targetLang, String sourceLang) {
        log.info("Creating Course")
        Course c = new Course()
        c.setTitle(name)
        c.setDescription(name + "-description")
        c.setTargetLang(targetLang)
        c.setSourceLang(sourceLang)

        for (i in 1..2) {
            Topic t = new Topic()
            t.setTitle(targetLang + "-topic-" + i)
            t.setDescription("topic description")
            t.setNumber(i)
            t.setDifficult(1)
            c.addTopic(t)
        }

        return c
    }
}
