/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study


import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.whywolk.ylearnit.study.domain.Card
import org.whywolk.ylearnit.study.domain.ContentType
import org.whywolk.ylearnit.study.domain.Side
import org.whywolk.ylearnit.study.domain.SideType
import org.whywolk.ylearnit.study.domain.StudyTest
import org.whywolk.ylearnit.study.service.testbuilder.SM2TestBuilder
import org.whywolk.ylearnit.study.service.testbuilder.TestBuilder

import static org.junit.jupiter.api.Assertions.*

class SM2TestBuilderTest {
    private static TestBuilder testBuilder

    @BeforeEach
    void setup() {
        testBuilder = new SM2TestBuilder()
    }

    @Test
    void twoTestSides() {
        Card c = new Card()
        c.addSide(new Side("tag_1", "test_content_1", ContentType.TEXT_PLAIN, "lang_1", SideType.TEST))
        c.addSide(new Side("tag_2", "test_content_2", ContentType.TEXT_PLAIN, "lang_2", SideType.TEST))

        StudyTest t = testBuilder.getTest(c)

        List<Side> cSides = c.getSides()
        List<Side> tSides = []
        if (new Random(0).nextInt(2) == 0) {
            tSides += [t.getQuestion()] + t.getAnswers()
        } else {
            tSides += t.getAnswers() + [t.getQuestion()]
        }

        for (i in 0..<c.sides.size()) {
            assertEquals(cSides[i].getContent(), tSides[i].getContent())
        }
    }

    @Test
    void threeTestSidesWithTwoAlternativesAreOneQuestion() {
        Card c = new Card()
        c.addSide(new Side("tag_1", "test_content_1", ContentType.TEXT_PLAIN, "lang_1", SideType.TEST))
        c.addSide(new Side("tag_2", "test_content_2", ContentType.TEXT_PLAIN, "lang_2", SideType.TEST))
        c.addSide(new Side("tag_3", "test_content_3", ContentType.TEXT_PLAIN, "lang_3", SideType.TEST))

        StudyTest t = testBuilder.getTest(c)

        List<Side> cSides = c.getSides()
        if (new Random(0).nextInt(2) == 0) {
            assertEquals(cSides[0].getContent(), t.getQuestion().getContent())
            assertEquals(2, t.getAnswers().size())
        } else {
            assertEquals(cSides[0].getContent(), t.getAnswers()[0].getContent())
        }
    }

    @Test
    void threeTestSidesWithTwoAlternativesAreAnswers() {
        Card c = new Card()
        c.addSide(new Side("tag_1", "test_content_1", ContentType.TEXT_PLAIN, "lang_1", SideType.TEST))
        c.addSide(new Side("tag_1", "test_content_2", ContentType.TEXT_PLAIN, "lang_1", SideType.TEST))
        c.addSide(new Side("tag_2", "test_content_3", ContentType.TEXT_PLAIN, "lang_2", SideType.TEST))

        StudyTest t = testBuilder.getTest(c)

        List<Side> cSides = c.getSides()
        if (new Random(0).nextInt(2) == 0) {
            assertEquals(cSides[2].getContent(), t.getAnswers()[0].getContent())
        } else {
            assertEquals(cSides[2].getContent(), t.getQuestion().getContent())
            assertEquals(2, t.getAnswers().size())
        }
    }

    @Test
    void onlyInfoSides() {
        Card c = new Card()
        c.addSide(new Side("tag_1", "test_content_1", ContentType.TEXT_PLAIN, "lang_1", SideType.INFO))
        c.addSide(new Side("tag_2", "test_content_2", ContentType.TEXT_PLAIN, "lang_2", SideType.INFO))

        Exception e = assertThrows(Exception, () -> {
            testBuilder.getTest(c)
        })

        assertEquals("Necessary 2 or more TEST sides", e.getMessage())
    }

    @Test
    void onlyAlternativeTestSideAndSeveralInfoSides() {
        Card c = new Card()
        c.addSide(new Side("tag_1", "test_content_1", ContentType.TEXT_PLAIN, "lang_1", SideType.TEST))
        c.addSide(new Side("tag_1", "test_content_2", ContentType.TEXT_PLAIN, "lang_1", SideType.TEST))
        c.addSide(new Side("tag_2", "test_content_3", ContentType.TEXT_PLAIN, "lang_2", SideType.INFO))
        c.addSide(new Side("tag_3", "test_content_4", ContentType.TEXT_PLAIN, "lang_3", SideType.INFO))

        Exception e = assertThrows(Exception, () -> {
            testBuilder.getTest(c)
        })

        assertEquals("Necessary 2 or more TEST unique sides", e.getMessage())
    }

    @Test
    void getTest_whenZeroTestSides_thenThrowException() {
        Card c = new Card()

        Exception e = assertThrows(Exception, () -> {
            testBuilder.getTest(c)
        })

        assertEquals("Necessary 2 or more TEST sides", e.getMessage())
    }

    @Test
    void getTest_whenOneTestSides_thenThrowException() {
        Card c = new Card()
        c.addSide(new Side("tag_1", "test_content_1", ContentType.TEXT_PLAIN, "lang_1", SideType.TEST))

        Exception e = assertThrows(Exception, () -> {
            testBuilder.getTest(c)
        })

        assertEquals("Necessary 2 or more TEST sides", e.getMessage())
    }

    @Test
    void onlyTestSidesThatAlternative() {
        Card c = new Card()
        c.addSide(new Side("tag_1", "test_content_1", ContentType.TEXT_PLAIN, "lang_1", SideType.TEST))
        c.addSide(new Side("tag_1", "test_content_2", ContentType.TEXT_PLAIN, "lang_2", SideType.TEST))

        Exception e = assertThrows(Exception, () -> {
            testBuilder.getTest(c)
        })

        assertEquals("Necessary 2 or more TEST unique sides", e.getMessage())
    }

//    @Test
//    void lesserThanTwoSides() {
//
//    }

}
