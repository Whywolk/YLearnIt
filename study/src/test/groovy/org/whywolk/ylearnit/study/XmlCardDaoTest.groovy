/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2024.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study

import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.whywolk.ylearnit.study.dao.XmlCardDao
import org.whywolk.ylearnit.study.domain.Card
import org.whywolk.ylearnit.study.domain.ContentType
import org.whywolk.ylearnit.study.domain.Side
import org.whywolk.ylearnit.study.domain.SideType

import static org.junit.jupiter.api.Assertions.*

class XmlCardDaoTest {
    private static final XmlCardDao dao = new XmlCardDao("./src/test/resources/study/cards.yli.xml")
    private static List<Card> cards = []

    @BeforeAll
    static void setup() {
        for (i in 1..3) {
            def card = new Card("test-db", i)
            card.addSide(new Side("tag-1", "side-$i-1", ContentType.TEXT_PLAIN, "lang-1", SideType.TEST))
            card.addSide(new Side("tag-2", "side-$i-2", ContentType.TEXT_PLAIN, "lang-2", SideType.TEST))
            cards.add(card)
        }
        for (i in 4..5) {
            def card = new Card("test-db", i)
            card.addSide(new Side("tag-1", "same-side-$i-1", ContentType.TEXT_PLAIN, "lang-1", SideType.TEST))
            card.addSide(new Side("tag-3", "side-$i-3", ContentType.TEXT_PLAIN, "lang-3", SideType.TEST))
            cards.add(card)
        }
    }

    @Test
    void getAll() {
        def expected = cards
        def actual = dao.getAll()

        assertEquals(expected, actual)
    }

    @Test
    void get() {
        def expected = [cards[1]]
        def actual = dao.get("side-2-1", "lang-1")

        assertEquals(expected, actual)
    }

    @Test
    void getById() {
        def expected = cards[0]
        def actual = dao.getById(1)

        assertEquals(expected, actual)
    }

    @Test
    void find_whenAll() {
        def expected = cards
        def actual = dao.find("side", "lang-1")

        assertEquals(expected, actual)
    }

    @Test
    void find_whenSame() {
        def expected = cards[3..4]
        def actual = dao.find("same", "lang-1")

        assertEquals(expected, actual)
    }

    @Test
    void findByTargetAndSourceLangs() {
        def expected = [cards[3]]
        def actual = dao.find("same-side-4", "lang-1", "lang-3")

        assertEquals(expected, actual)
    }
}
