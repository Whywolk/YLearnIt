/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.whywolk.ylearnit.study.service.scheduler.SM2Algorithm

import static org.junit.jupiter.api.Assertions.*

class SM2AlgorithmTest {

    private SM2Algorithm algorithm

    @BeforeEach
    void setup() {
        algorithm = new SM2Algorithm()
    }

    @Test
    void testInit() {
        int expectedInterval = 0
        int expectedEF = 2500
        String expected = "Interval = ${expectedInterval} EF = ${expectedEF}"
        String actual = "Interval = ${algorithm.getInterval()} EF = ${algorithm.getEF()}"

        assertEquals(expected, actual)
    }

    @Test
    void testFirstRepetition() {
        int expectedInterval = 1
        int expectedEF = 2500 - 140
        int quality = 3
        String expected = "Interval = ${expectedInterval} EF = ${expectedEF}"

        algorithm.setInterval(0)
        algorithm.setEF(2500)
        algorithm.calcInterval(quality)
        String actual = "Interval = ${algorithm.getInterval()} EF = ${algorithm.getEF()}"

        assertEquals(expected, actual)
    }

    @Test
    void testSecondRepetition() {
        int expectedInterval = 6
        int expectedEF = 2500
        int quality = 4
        String expected = "Interval = ${expectedInterval} EF = ${expectedEF}"

        algorithm.setInterval(1)
        algorithm.setEF(2500)
        algorithm.calcInterval(quality)
        String actual = "Interval = ${algorithm.getInterval()} EF = ${algorithm.getEF()}"

        assertEquals(expected, actual)
    }

    @Test
    void testThirdRepetition() {
        int expectedInterval = 14
        int expectedEF = 2200 + 100
        int quality = 5
        String expected = "Interval = ${expectedInterval} EF = ${expectedEF}"

        algorithm.setInterval(6)
        algorithm.setEF(2200)
        algorithm.calcInterval(quality)
        String actual = "Interval = ${algorithm.getInterval()} EF = ${algorithm.getEF()}"

        assertEquals(expected, actual)
    }

    @Test
    void testRepetition() {
        int expectedInterval = 24
        int expectedEF = 1960 - 140
        int quality = 3
        String expected = "Interval = ${expectedInterval} EF = ${expectedEF}"

        algorithm.setInterval(13)
        algorithm.setEF(1960)
        algorithm.calcInterval(quality)
        String actual = "Interval = ${algorithm.getInterval()} EF = ${algorithm.getEF()}"

        assertEquals(expected, actual)
    }

    @Test
    void testUpperEF() {
        int expectedInterval = 50
        int expectedEF = 2500
        int quality = 5
        String expected = "Interval = ${expectedInterval} EF = ${expectedEF}"

        algorithm.setInterval(20)
        algorithm.setEF(2500)
        algorithm.calcInterval(quality)
        String actual = "Interval = ${algorithm.getInterval()} EF = ${algorithm.getEF()}"

        assertEquals(expected, actual)
    }

    @Test
    void testLowerEF() {
        int expectedInterval = 26
        int expectedEF = 1300
        int quality = 3
        String expected = "Interval = ${expectedInterval} EF = ${expectedEF}"

        algorithm.setInterval(20)
        algorithm.setEF(1300)
        algorithm.calcInterval(quality)
        String actual = "Interval = ${algorithm.getInterval()} EF = ${algorithm.getEF()}"

        assertEquals(expected, actual)
    }

    @Test
    void testForgetAnswer() {
        int expectedInterval = 1
        int expectedEF = 2160
        int quality = 1
        String expected = "Interval = " + expectedInterval + " EF = " + expectedEF

        algorithm.setInterval(20)
        algorithm.setEF(2160)
        algorithm.calcInterval(quality)
        String actual = "Interval = " + algorithm.getInterval() + " EF = " + algorithm.getEF()

        assertEquals(expected, actual)
    }

    @Test
    void testWrongQuality() {
        int quality = 20

        Exception e = assertThrows(IllegalStateException, () -> {
            algorithm.calcInterval(quality)
        })

        assertEquals("Wrong quality=" + quality, e.getMessage())
    }
}
