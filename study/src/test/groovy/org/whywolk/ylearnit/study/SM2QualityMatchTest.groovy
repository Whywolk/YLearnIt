/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study

import org.junit.jupiter.api.Test
import org.whywolk.ylearnit.study.domain.Card
import org.whywolk.ylearnit.study.domain.ContentType
import org.whywolk.ylearnit.study.domain.Side
import org.whywolk.ylearnit.study.domain.SideType
import org.whywolk.ylearnit.study.domain.UserAnswer
import org.whywolk.ylearnit.study.entity.Answer
import org.whywolk.ylearnit.study.service.evaluator.SM2Evaluator
import org.whywolk.ylearnit.study.service.scheduler.SM2Scheduler

import static org.junit.jupiter.api.Assertions.*

class SM2QualityMatchTest {
    private final SM2Evaluator evaluator = new SM2Evaluator()
    private final SM2Scheduler scheduler = new SM2Scheduler()

    @Test
    void evaluate_when5() {
        String qualityExpected = "5"
        String qualityActual = getMatchedQuality(qualityExpected)
        assertEquals(qualityExpected, qualityActual)
    }

    @Test
    void evaluate_when4() {
        String qualityExpected = "4"
        String qualityActual = getMatchedQuality(qualityExpected)
        assertEquals(qualityExpected, qualityActual)
    }

    @Test
    void evaluate_when3() {
        String qualityExpected = "3"
        String qualityActual = getMatchedQuality(qualityExpected)
        assertEquals(qualityExpected, qualityActual)
    }

    @Test
    void evaluate_when2() {
        String qualityExpected = "2"
        String qualityActual = getMatchedQuality(qualityExpected)
        assertEquals(qualityExpected, qualityActual)
    }

    @Test
    void evaluate_when1() {
        String qualityExpected = "1"
        String qualityActual = getMatchedQuality(qualityExpected)
        assertEquals(qualityExpected, qualityActual)
    }

    @Test
    void evaluate_when0() {
        String qualityExpected = "0"
        String qualityActual = getMatchedQuality(qualityExpected)
        assertEquals(qualityExpected, qualityActual)
    }

    @Test
    void evaluate_whenIncorrectQuality_then_throwException() {
        String incorrectQuality = "10"
        Exception e = assertThrows(RuntimeException, () -> {
            getMatchedQuality(incorrectQuality)
        })
        assertEquals("Unexpected userAnswer content='" + incorrectQuality + "'", e.getMessage())
    }


    private String getMatchedQuality(String quality) {
        UserAnswer u = getUserAnswer(quality)
        Answer a = evaluator.evaluate(u)
        return Integer.toString(scheduler.matchQuality(a.getRetrievability()))
    }

    private UserAnswer getUserAnswer(String quality) {
        def u = new UserAnswer(new Card(), "SM2", 0, 0, 0 ,0)
        u.setUserAnswer(new Side("tag", quality, ContentType.TEXT_PLAIN, "en", SideType.USER_ANSWER))
        return u
    }
}
