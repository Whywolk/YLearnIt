/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.study

import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.whywolk.ylearnit.openrussian.domain.Translation
import org.whywolk.ylearnit.openrussian.domain.Word
import org.whywolk.ylearnit.openrussian.service.OpenRussianService
import org.whywolk.ylearnit.study.service.cardbuilder.CardBuilder
import org.whywolk.ylearnit.study.service.cardbuilder.OpenRussianCardBuilder

import static org.junit.jupiter.api.Assertions.*

class OpenRussianCardBuilderTest {
    private static CardBuilder cardBuilder
    private static List<Word> words

    @BeforeAll
    static void setup() {
        words = getWords()
        def service = Mockito.mock(OpenRussianService)
        Mockito.when(service.getByIds(Mockito.any())).thenReturn(words)
        cardBuilder = new OpenRussianCardBuilder(service)
    }

    @Test
    void getCards_whenCorrectLangRuEn() {
        def cards = cardBuilder.getCards([], "ru", "en")

        assertEquals(words.size(), cards.size())

        for (i in 0..2) {
            assertEquals(words[i].getBare(), cards[i].getSides()[0].getContent())
            assertEquals(words[i].getTranslations()[0].getTranslation(), cards[i].getSides()[1].getContent())
        }
    }

    @Test
    void getCards_whenCorrectLangRuDe() {
        def cards = cardBuilder.getCards([], "ru", "de")

        assertEquals(words.size(), cards.size())

        for (i in 0..2) {
            assertEquals(words[i].getBare(), cards[i].getSides()[0].getContent())
            assertEquals(words[i].getTranslations()[1].getTranslation(), cards[i].getSides()[1].getContent())
        }
    }

    @Test
    void getCards_whenCorrectLangEnRu() {
        def cards = cardBuilder.getCards([], "en", "ru")

        assertEquals(words.size(), cards.size())

        for (i in 0..2) {
            assertEquals(words[i].getBare(), cards[i].getSides()[0].getContent())
            assertEquals(words[i].getTranslations()[0].getTranslation(), cards[i].getSides()[1].getContent())
        }
    }

    @Test
    void getCards_whenCorrectLangDeRu() {
        def cards = cardBuilder.getCards([], "de", "ru")

        assertEquals(words.size(), cards.size())

        for (i in 0..2) {
            assertEquals(words[i].getBare(), cards[i].getSides()[0].getContent())
            assertEquals(words[i].getTranslations()[1].getTranslation(), cards[i].getSides()[1].getContent())
        }
    }

    @Test
    void getCards_whenIncorrectSourceLang_thenEmpty() {
        def cards = cardBuilder.getCards([], "ru", "wrong")

        assertTrue(cards.isEmpty())
    }

    @Test
    void getCards_whenIncorrectTargetLang_thenEmpty() {
        def cards = cardBuilder.getCards([], "wrong", "en")

        assertTrue(cards.isEmpty())
    }

    @Test
    void getCards_whenIncorrectLangs_thenEmpty() {
        def cards = cardBuilder.getCards([], "de", "en")

        assertTrue(cards.isEmpty())
    }

    private static List<Word> getWords() {
        def words = []
        for (i in 0..2) {
            Word w = new Word()
            w.setId(i)
            w.setBare("word ru {i}")

            Translation enTr = new Translation().with {
                it.setTranslation("word en {i}")
                it.setLang("en")
                return it
            }

            Translation deTr = new Translation().with {
                it.setTranslation("word de {i}")
                it.setLang("de")
                return it
            }
            w.addTranslation(enTr)
            w.addTranslation(deTr)
            words.add(w)
        }
        return words
    }
}
