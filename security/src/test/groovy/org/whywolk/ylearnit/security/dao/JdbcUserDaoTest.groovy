/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2024.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.security.dao

import com.zaxxer.hikari.HikariDataSource
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.whywolk.ylearnit.security.domain.User
import org.whywolk.ylearnit.security.domain.UserEmail
import org.whywolk.ylearnit.security.domain.UserPassword
import org.whywolk.ylearnit.security.domain.UserRole

import javax.sql.DataSource

import static org.junit.jupiter.api.Assertions.*

class JdbcUserDaoTest {
    private UserDao dao
    private DataSource ds

    @BeforeEach
    void setup() {
        def uds = new UserDataSource("jdbc:h2:mem:users;INIT=runscript from './src/test/resources/security/sql/create_tables.sql'")
        ds = uds.getDataSource()
        dao = new JdbcUserDao(ds)
        def roleDao = new JdbcRoleDao(ds)

        roleDao.create(new UserRole("role_1"))
        roleDao.create(new UserRole("role_2"))
        roleDao.create(new UserRole("role_3"))
    }

    @AfterEach
    void closeDataSource() {
        (ds as HikariDataSource).close()
    }

    @Test
    void getByLogin_whenUserNotCreated_thenException() {
        assertThrows(RuntimeException, () -> {
            dao.getByLogin("login")
        })
    }

    @Test
    void createUser() {
        def user = new User("login", new UserPassword("salt", "hash"))

        dao.create(user)

        def fetchedUser = dao.getByLogin(user.getLogin())
        assertEquals(user, fetchedUser)
    }

    @Test
    void createUser_withEmail() {
        def user = new User("login", new UserPassword("salt", "hash"))
        user.setEmail(new UserEmail("test@test.org"))

        dao.create(user)

        def fetchedUser = dao.getByLogin(user.getLogin())
        assertEquals(user, fetchedUser)
        assertEquals(user.getEmail(), fetchedUser.getEmail())
    }

    @Test
    void createUser_withRole() {
        def user = new User("login", new UserPassword("salt", "hash"))
        user.addRole(new UserRole("role_1"))

        dao.create(user)

        def fetchedUser = dao.getByLogin(user.getLogin())
        assertEquals(user, fetchedUser)
    }

    @Test
    void createUser_withTwoRoles() {
        def user = new User("login", new UserPassword("salt", "hash"))
        user.addRole(new UserRole("role_1"))
        user.addRole(new UserRole("role_2"))

        dao.create(user)

        def fetchedUser = dao.getByLogin(user.getLogin())
        assertEquals(user, fetchedUser)
    }

    @Test
    void createUser_withNotPersistedRole_thenException() {
        def user = new User("login", new UserPassword("salt", "hash"))
        user.addRole(new UserRole("not_persisted_role"))

        assertThrows(RuntimeException, () -> {
            dao.create(user)
        })
    }

    @Test
    void createUsers() {
        def user1 = new User("login_1", new UserPassword("salt", "hash"))
        def user2 = new User("login_2", new UserPassword("salt", "hash"))

        dao.create(user1)
        dao.create(user2)

        def fetchedUsers = dao.getAll()
        assertTrue(fetchedUsers.containsAll([user1, user2]))
    }

    @Test
    void createUsers_withSameLogins_thenException() {
        def user1 = new User("login_1", new UserPassword("salt", "hash"))
        def user2 = new User(user1.getLogin(), new UserPassword("salt", "hash"))

        dao.create(user1)

        assertThrows(RuntimeException, () -> {
            dao.create(user2)
        })
    }

    @Test
    void updateNotChangedUser() {
        def user = new User("login", new UserPassword("salt", "hash"))
        user.addRole(new UserRole("role_1"))
        dao.create(user)

        def notChangedUser = dao.getByLogin(user.getLogin())

        dao.update(notChangedUser)

        def fetchedUser = dao.getByLogin(user.getLogin())
        assertEquals(notChangedUser, fetchedUser)
    }

    @Test
    void updateUser_withNewName() {
        def user = new User("login", new UserPassword("salt", "hash"))
        user.addRole(new UserRole("role_1"))
        dao.create(user)

        def updatedUser = dao.getByLogin(user.getLogin())
        updatedUser.setName("new_name")

        dao.update(updatedUser)

        def fetchedUser = dao.getByLogin(updatedUser.getLogin())
        assertEquals(updatedUser, fetchedUser)
        assertNotEquals(user, fetchedUser)
    }

    @Test
    void updateUser_withNewPassword() {
        def user = new User("login", new UserPassword("hash", "salt"))
        user.addRole(new UserRole("role_1"))
        dao.create(user)

        def updatedUser = dao.getByLogin(user.getLogin())
        updatedUser.setPassword(new UserPassword("new_hash", "new_salt"))

        dao.update(updatedUser)

        def fetchedUser = dao.getByLogin(updatedUser.getLogin())
        assertEquals(updatedUser, fetchedUser)
        assertNotEquals(user, fetchedUser)
        assertEquals(updatedUser.getPassword(), fetchedUser.getPassword())
        assertNotEquals(user.getPassword(), fetchedUser.getPassword())
    }

    @Test
    void updateUser_withNewEmail() {
        def user = new User("login", new UserPassword("hash", "salt"))
        user.addRole(new UserRole("role_1"))
        user.setEmail(new UserEmail("first@test.org"))
        dao.create(user)

        def updatedUser = dao.getByLogin(user.getLogin())
        updatedUser.setEmail(new UserEmail("second@test.org"))

        dao.update(updatedUser)

        def fetchedUser = dao.getByLogin(updatedUser.getLogin())
        assertEquals(updatedUser, fetchedUser)
        assertNotEquals(user, fetchedUser)
        assertEquals(updatedUser.getEmail(), fetchedUser.getEmail())
        assertNotEquals(user.getEmail(), fetchedUser.getEmail())
    }

    @Test
    void updateUser_withNewRoles() {
        def user = new User("login", new UserPassword("hash", "salt"))
        user.addRole(new UserRole("role_1"))
        user.addRole(new UserRole("role_2"))
        dao.create(user)

        def updatedUser = dao.getByLogin(user.getLogin())
        updatedUser.removeRole(new UserRole("role_1"))
        updatedUser.addRole(new UserRole("role_3"))

        dao.update(updatedUser)

        def fetchedUser = dao.getByLogin(updatedUser.getLogin())
        assertEquals(updatedUser, fetchedUser)
        assertNotEquals(user, fetchedUser)
    }

    @Test
    void updateUser_withNotPersistedRole_thenException() {
        def user = new User("login", new UserPassword("hash", "salt"))
        user.addRole(new UserRole("role_1"))
        user.addRole(new UserRole("role_2"))
        dao.create(user)

        def updatedUser = dao.getByLogin(user.getLogin())
        updatedUser.addRole(new UserRole("not_persisted_role"))

        assertThrows(RuntimeException, () -> {
            dao.update(updatedUser)
        })
    }

    @Test
    void deleteUser() {
        def user = new User("login", new UserPassword("salt", "hash"))
        user.addRole(new UserRole("role_1"))
        dao.create(user)

        dao.delete(user)

        def fetchedUsers = dao.getAll()
        assertFalse(fetchedUsers.contains(user))
    }
}
