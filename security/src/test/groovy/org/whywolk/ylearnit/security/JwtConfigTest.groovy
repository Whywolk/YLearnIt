/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.security

import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.whywolk.ylearnit.security.config.JwtConfig
import org.whywolk.ylearnit.security.config.PropsJwtConfig

import javax.crypto.SecretKey
import javax.crypto.spec.SecretKeySpec

import static org.junit.jupiter.api.Assertions.*

class JwtConfigTest {

    private static JwtConfig config

    @BeforeAll
    static void setup() {
        config = Mockito.spy(PropsJwtConfig)

        // mock filesystem operations (save, load) to do nothing
        Mockito.when(config.loadConfig()).thenReturn(new Properties())
        Mockito.doNothing().when(config).save()
        Mockito.doNothing().when(config).saveConfig(Mockito.any())
    }


    @Test
    void initConfig_withCorrectProps() {
        def issuer = "test_issuer"
        def accessDaysExpire = 5
        def refreshDaysExpire = 10
        def signKey = "aGVsbG8gd29ybGQ="
        Properties p = new Properties()
        p.setProperty("issuer", issuer)
        p.setProperty("access_date_expiration", String.valueOf(accessDaysExpire))
        p.setProperty("refresh_date_expiration", String.valueOf(refreshDaysExpire))
        p.setProperty("sign_key", signKey)

        config.initConfigWith(p)

        assertEquals(issuer, config.getIssuer())
        assertEquals(accessDaysExpire, config.getAccessDateExpiration())
        assertEquals(refreshDaysExpire, config.getRefreshDateExpiration())
        assertArrayEquals(base64ToByte(signKey), config.getKey().getEncoded())
    }

    @Test
    void initConfig_withPositiveDaysExpiration() {
        def accessDaysExpire = 5
        def refreshDaysExpire = 10
        Properties p = new Properties()
        p.setProperty("access_date_expiration", String.valueOf(accessDaysExpire))
        p.setProperty("refresh_date_expiration", String.valueOf(refreshDaysExpire))

        config.initConfigWith(p)

        assertEquals(accessDaysExpire, config.getAccessDateExpiration())
        assertEquals(refreshDaysExpire, config.getRefreshDateExpiration())
    }

    @Test
    void initConfig_withNegativeDaysExpiration() {
        def negAccessDaysExpire = -1
        def negRefreshDaysExpire = -1
        Properties p = new Properties()
        p.setProperty("access_date_expiration", String.valueOf(negAccessDaysExpire))
        p.setProperty("refresh_date_expiration", String.valueOf(negRefreshDaysExpire))

        config.initConfigWith(p)

        assertEquals(config.getDefaultAccessDateExpiration(), config.getAccessDateExpiration())
        assertEquals(config.getDefaultRefreshDateExpiration(), config.getRefreshDateExpiration())
    }

    @Test
    void initConfig_emptyKey_thenGenerateNewKey() {
        Properties p = new Properties()

        config.initConfigWith(p)

        assertNotNull(config.getKey())
    }

    @Test
    void initConfig_keyIsNotBase64_thenGenerateNewKey() {
        Properties p = new Properties()
        p.setProperty("sign_key", "hello")

        config.initConfigWith(p)

        assertNotEquals(p.getProperty("sign_key"), keyToBase64(config.getKey()))
    }


    private static SecretKey base64ToKey(String keyBase64) {
        byte[] keyBytes = base64ToByte(keyBase64)
        return new SecretKeySpec(keyBytes, "HmacSHA256")
    }

    private static String keyToBase64(SecretKey key) {
        byte[] keyBytes = key.getEncoded()
        return byteToBase64(keyBytes)
    }

    private static String byteToBase64(byte[] bytes) {
        Base64.Encoder encoder = Base64.getEncoder()
        return encoder.encodeToString(bytes)
    }

    private static byte[] base64ToByte(String str) {
        Base64.Decoder decoder = Base64.getDecoder()
        return decoder.decode(str)
    }
}
