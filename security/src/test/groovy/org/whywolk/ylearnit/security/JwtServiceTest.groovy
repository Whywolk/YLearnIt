/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.security

import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jws
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.security.InvalidKeyException
import io.jsonwebtoken.security.Keys
import io.jsonwebtoken.security.SignatureException
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.whywolk.ylearnit.security.config.JwtConfig
import org.whywolk.ylearnit.security.domain.User
import org.whywolk.ylearnit.security.domain.UserPassword
import org.whywolk.ylearnit.security.domain.UserRole
import org.whywolk.ylearnit.security.service.JwtService

import javax.crypto.SecretKey
import javax.crypto.spec.SecretKeySpec

import static org.junit.jupiter.api.Assertions.*

class JwtServiceTest {
    private static User user
    private static JwtConfig config
    private static JwtService service

    @BeforeAll
    static void setup() {
        config = Mockito.mock(JwtConfig)
        Mockito.when(config.getIssuer()).thenReturn("test_issuer")
        Mockito.when(config.getAccessDateExpiration()).thenReturn(2)
        Mockito.when(config.getRefreshDateExpiration()).thenReturn(10)

        service = new JwtService(config)

        user = new User("test_login", new UserPassword("hash", "salt"))
        user.addRole(new UserRole("role_1"))
        user.addRole(new UserRole("role_2"))
    }

    @Test
    void generateTokens_correctKey() {
        Mockito.when(config.getKey()).thenReturn(generateKey())

        def tokens = service.generateTokens(user)

        Claims claimsAccess = service.getJwsClaims(tokens['access']).getBody()
        Claims claimsRefresh = service.getJwsClaims(tokens['refresh']).getBody()

        Long msecondInDay = 24 * 60 * 60 * 1000
        Long offset = 5 * 60 * 1000
        Long daysExpAccess = (claimsAccess.getExpiration().getTime() - claimsAccess.getIssuedAt().getTime())
        Long daysExpRefresh = (claimsRefresh.getExpiration().getTime() - claimsRefresh.getIssuedAt().getTime())

        assertEquals(user.getLogin(), claimsAccess.getSubject())
        assertEquals(user.getLogin(), claimsAccess.getSubject())

        def roles = claimsAccess.get("roles") as List<String>
        assertTrue(roles.containsAll(user.getRolesStrings()))

        assertEquals("access", claimsAccess.get("type"))
        assertEquals("refresh", claimsRefresh.get("type"))

        assertEquals(config.getAccessDateExpiration() * msecondInDay + offset, daysExpAccess)
        assertEquals(config.getRefreshDateExpiration() * msecondInDay + offset, daysExpRefresh)
    }

    @Test
    void generateTokens_wrongKey() {
        // first key
        SecretKey firstKey = generateKey()
        Mockito.when(config.getKey()).thenReturn(firstKey)

        def tokens = service.generateTokens(user)

        // second key
        SecretKey secondKey = generateKey()
        Mockito.when(config.getKey()).thenReturn(secondKey)

        assertThrows(SignatureException, () -> {
            service.getJwsClaims(tokens['access'])
        })
    }

    @Test
    void generateTokens_weakKey() {
        Mockito.when(config.getKey()).thenReturn(new SecretKeySpec(new byte[] {1, 2, 3, 4}, "HmacSHA256"))

        assertThrows(InvalidKeyException, () -> {
            def tokens = service.generateTokens(user)
            Jws<Claims> claims = service.getJwsClaims(tokens['access'])
        })
    }


    private static SecretKey generateKey() {
        return Keys.secretKeyFor(SignatureAlgorithm.HS256);
    }

    private static SecretKey base64ToKey(String keyBase64) {
        byte[] keyBytes = base64ToByte(keyBase64)
        return new SecretKeySpec(keyBytes, "HmacSHA256")
    }

    private static String keyToBase64(SecretKey key) {
        byte[] keyBytes = key.getEncoded()
        return byteToBase64(keyBytes)
    }

    private static String byteToBase64(byte[] bytes) {
        Base64.Encoder encoder = Base64.getEncoder()
        return encoder.encodeToString(bytes)
    }

    private static byte[] base64ToByte(String str) {
        Base64.Decoder decoder = Base64.getDecoder()
        return decoder.decode(str)
    }
}
