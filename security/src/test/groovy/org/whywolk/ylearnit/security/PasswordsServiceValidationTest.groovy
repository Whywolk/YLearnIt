/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.security

import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.whywolk.ylearnit.security.service.PasswordsService

import static org.junit.jupiter.api.Assertions.*

class PasswordsServiceValidationTest {

    private static PasswordsService service

    @BeforeAll
    static void setup() {
        service = new PasswordsService()
    }

    @Test
    void isValid_4rulesMatched_thenTrue() {
        String password = "SomeCorrectPassword12!"
        assertTrue(service.isValid(password))
    }

    @Test
    void isValid_3rulesMatched_thenTrue() {
        String password = "SomeCorrectPassword12"
        assertTrue(service.isValid(password))
    }

    @Test
    void isValid_2rulesMatched_thenTrue() {
        String password = "SomeCorrectPassword"
        assertTrue(service.isValid(password))
    }

    @Test
    void isValid_shortPassword_thenFalse() {
        String password = "Some!2"
        assertFalse(service.isValid(password))
    }

    @Test
    void isValid_longPassword_thenFalse() {
        String password = "SomeWrongPassword1234!SomeWrongPassword1234!SomeWrongPassword1234!SomeWrongPassword1234!"
        assertFalse(service.isValid(password))
    }

    @Test
    void isValid_withSpaces_thenFalse() {
        String password = "SomePassword!  hello1"
        assertFalse(service.isValid(password))
    }

    @Test
    void isValid_onlyUpperCase_thenFalse() {
        String password = "HELLOWORLD"
        assertFalse(service.isValid(password))
    }

    @Test
    void isValid_onlyLowerCase_thenFalse() {
        String password = "helloworld"
        assertFalse(service.isValid(password))
    }

    @Test
    void isValid_onlyNumber_thenFalse() {
        String password = "0123456789"
        assertFalse(service.isValid(password))
    }

    @Test
    void isValid_onlySpecialSymbol_thenFalse() {
        String password = "_#?!@\$%^&*-"
        assertFalse(service.isValid(password))
    }

    @Test
    void isValid_2rulesMatchedButNotAllowedSymbols_thenFalse() {
        String password = "Password with space ///"
        assertFalse(service.isValid(password))
    }
}
