/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.security

import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.whywolk.ylearnit.security.service.PasswordsService

import static org.junit.jupiter.api.Assertions.*

class PasswordsServiceTest {
    private static PasswordsService service
    private static String firstPassword
    private static String secondPassword
    private static String firstSalt
    private static String secondSalt
    private static String firstHash
    private static String secondHash

    @BeforeAll
    static void setup() {
        service = new PasswordsService()

        firstPassword = "CorrectPass12!"
        firstSalt = service.generateSaltBase64()
        firstHash = service.generateHashBase64(firstPassword, firstSalt)

        secondPassword = "WrongPass12!"
        secondSalt = service.generateSaltBase64()
        secondHash = service.generateHashBase64(secondPassword, secondSalt)
    }

    @Test
    void isCorrectPassword_whenCorrectPassSaltHash_thenTrue() {
        assertTrue(service.isCorrectPassword(firstPassword, firstSalt, firstHash))
    }

    @Test
    void isCorrectPassword_whenOtherPassword_thenFalse() {
        assertFalse(service.isCorrectPassword(secondPassword, firstSalt, firstHash))
    }

    @Test
    void isCorrectPassword_whenOtherSalt_thenFalse() {
        assertFalse(service.isCorrectPassword(firstPassword, secondSalt, firstHash))
    }

    @Test
    void isCorrectPassword_whenOtherHash_thenFalse() {
        assertFalse(service.isCorrectPassword(firstPassword, firstSalt, secondHash))
    }

    @Test
    void generateHash_throwsException_onShortSalt() {
        assertThrows(Exception, () -> {
            service.generateHashBase64(firstPassword, firstSalt.substring(0, 20))
        })
    }
}
