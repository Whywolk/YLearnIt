/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2024.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.security.dao

import com.zaxxer.hikari.HikariDataSource
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.whywolk.ylearnit.security.domain.UserRole

import javax.sql.DataSource

import static org.junit.jupiter.api.Assertions.*

class JdbcRoleDaoTest {
    private RoleDao dao
    private DataSource ds

    @BeforeEach
    void setup() {
        def uds = new UserDataSource("jdbc:h2:mem:users;INIT=runscript from './src/test/resources/security/sql/create_tables.sql'")
        ds = uds.getDataSource()
        dao = new JdbcRoleDao(ds)
    }

    @AfterEach
    void closeDataSource() {
        (ds as HikariDataSource).close()
    }

    @Test
    void create() {
        def role = new UserRole("user")

        dao.create(role)

        def fetched = dao.getAll()
        assertEquals(1, fetched.size())
        assertTrue(fetched.contains(role))
    }

    @Test
    void createSameRoles_thenException() {
        def role = new UserRole("user")

        dao.create(role)

        assertThrows(RuntimeException, () -> {
            dao.create(role)
        })
    }

    @Test
    void createSeveralRoles() {
        def role1 = new UserRole("user")
        def role2 = new UserRole("test")

        dao.create(role1)
        dao.create(role2)

        def fetched = dao.getAll()
        assertEquals(2, fetched.size())
        assertTrue(fetched.containsAll([role1, role2]))
    }

    @Test
    void exists() {
        def roles = [new UserRole("role_1"), new UserRole("role_2"), new UserRole("role_3")]

        roles.forEach(role -> dao.create(role))

        roles.forEach(role -> assertTrue(dao.exists(role)))
        assertFalse(dao.exists(new UserRole("not_created_role")))
    }

    @Test
    void update() {
        def oldRole = new UserRole("old_role")
        dao.create(oldRole)

        def newRole = new UserRole("new_role")
        dao.update(oldRole, newRole)

        def fetched = dao.getAll()
        assertEquals(1, fetched.size())
        assertFalse(fetched.contains(oldRole))
        assertTrue(fetched.contains(newRole))
    }

    @Test
    void delete() {
        def role = new UserRole("user")
        dao.create(role)

        dao.delete(role)

        def fetched = dao.getAll()
        assertTrue(fetched.isEmpty())
    }
}
