/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022-2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.security.rest;

import jakarta.ws.rs.NameBinding;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@NameBinding
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Secured {
    String[] requiredPermissions() default {};
}

