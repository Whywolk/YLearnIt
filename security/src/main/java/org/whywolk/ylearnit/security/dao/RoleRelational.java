/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2024.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.security.dao;

import org.whywolk.ylearnit.security.domain.UserRole;

import java.util.ArrayList;
import java.util.List;

public class RoleRelational {
    private String role;

    public static List<UserRole> mapToModel(List<RoleRelational> roleList) {
        List<UserRole> roles = new ArrayList<>();
        for (RoleRelational r : roleList) {
            roles.add(new UserRole(r.role));
        }
        return roles;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
