/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2024.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.security.dao;

import org.whywolk.ylearnit.security.domain.User;

import java.util.List;

public interface UserDao {
    User getByLogin(String login);

    List<User> getAll();

    void create(User user);

    void update(User user);

    void delete(User user);
}
