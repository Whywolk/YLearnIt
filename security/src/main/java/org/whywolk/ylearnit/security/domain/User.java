/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2024.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.security.domain;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class User {
    private String login;
    private String name;
    private String description;
    private LocalDateTime timeAdded;
    private UserPassword password;
    private UserEmail email;
    private final Set<UserRole> roles;

    private User() {
        this.roles = new HashSet<>();
    }

    public User(String login, UserPassword password) {
        this(login, login, "", LocalDateTime.now(ZoneOffset.UTC), password);
    }

    public User(String login, String name, UserPassword password) {
        this(login, name, "", LocalDateTime.now(ZoneOffset.UTC), password);
    }

    public User(String login, String name, String description, LocalDateTime timeAdded, UserPassword password) {
        this();
        this.login = login;
        this.name = name;
        this.description = description;
        this.timeAdded = timeAdded;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getTimeAdded() {
        return timeAdded;
    }

    public UserPassword getPassword() {
        return password;
    }

    public void setPassword(UserPassword password) {
        this.password = password;
    }

    public UserEmail getEmail() {
        return email;
    }

    public void setEmail(UserEmail email) {
        this.email = email;
    }

    public Set<UserRole> getRoles() {
        return roles;
    }

    public void addRole(UserRole role) {
        roles.add(role);
    }

    public void removeRole(UserRole role) {
        roles.remove(role);
    }

    public Set<String> getRolesStrings() {
        Set<String> rolesStrings = new HashSet<>();
        for (UserRole role : roles) {
            rolesStrings.add(role.getRole());
        }
        return rolesStrings;
    }

    public boolean hasPassword() {
        return password != null;
    }

    public boolean hasEmail() {
        return email != null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(login, user.login)
                && Objects.equals(name, user.name)
                && Objects.equals(description, user.description)
                && Objects.equals(password, user.password)
                && Objects.equals(email, user.email)
                && Objects.equals(roles, user.roles);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login, name, description, password, email, roles);
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", timeAdded='" + timeAdded +
                ", password=" + password +
                ", email=" + email +
                ", roles=" + roles +
                '}';
    }
}
