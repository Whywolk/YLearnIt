package org.whywolk.ylearnit.security;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import jakarta.inject.Singleton;
import org.whywolk.ylearnit.security.dao.*;
import org.whywolk.ylearnit.security.service.AdminService;
import org.whywolk.ylearnit.security.service.UserService;

public class SecurityModule extends AbstractModule {

    @Provides @Singleton
    public UserService provideUserService(UserDao userDao) {
        return new UserService(userDao);
    }

    @Provides @Singleton
    public AdminService provideAdminService() {
        return new AdminService();
    }

    @Provides @Singleton
    public UserDao provideUserDao(UserDataSource userDataSource) {
        return new JdbcUserDao(userDataSource.getDataSource());
    }

    @Provides @Singleton
    public RoleDao provideRoleDao(UserDataSource userDataSource) {
        return new JdbcRoleDao(userDataSource.getDataSource());
    }

    @Provides @Singleton
    public UserDataSource provideUserDataSource() {
        return new UserDataSource("jdbc:h2:./db/users.h2");
    }
}
