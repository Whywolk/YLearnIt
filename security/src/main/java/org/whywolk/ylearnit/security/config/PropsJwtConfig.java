/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.security.config;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.util.Base64;
import java.util.Properties;
import java.util.UUID;

public class PropsJwtConfig implements JwtConfig {
    private static final Logger log = LogManager.getLogger(PropsJwtConfig.class);

    private static final String CONFIGURATION_FILE_PATH = "jwt.properties";
    private static final String CONFIGURATION_DIR_PATH = "config";
    private static final int DEFAULT_ACCESS_DATE_EXPIRATION = 2;
    private static final int DEFAULT_REFRESH_DATE_EXPIRATION = 20;

    private Properties properties;
    private SecretKey key;
    private String issuer;
    private int accessDateExpiration;
    private int refreshDateExpiration;

    public PropsJwtConfig() {
        Properties props = getDefaultProperties();
        initConfigWith(props);
    }

    public PropsJwtConfig(Properties props) {
        initConfigWith(props);
    }

    @Override
    public String getIssuer() {
        return issuer;
    }

    @Override
    public int getAccessDateExpiration() {
        return accessDateExpiration;
    }

    @Override
    public int getRefreshDateExpiration() {
        return refreshDateExpiration;
    }

    @Override
    public SecretKey getKey() {
        return key;
    }


    @Override
    public int getDefaultAccessDateExpiration() {
        return DEFAULT_ACCESS_DATE_EXPIRATION;
    }

    @Override
    public int getDefaultRefreshDateExpiration() {
        return DEFAULT_REFRESH_DATE_EXPIRATION;
    }

    @Override
    public Properties getDefaultProperties() {
        Properties props = new Properties();
        props.setProperty("issuer", UUID.randomUUID().toString());
        props.setProperty("access_date_expiration", String.valueOf(DEFAULT_ACCESS_DATE_EXPIRATION));
        props.setProperty("refresh_date_expiration", String.valueOf(DEFAULT_REFRESH_DATE_EXPIRATION));
        props.setProperty("sign_key", generateKeyBase64());
        return props;
    }

    @Override
    public void initConfigWith(Properties props) {
        boolean changed = false;

        String k = props.getProperty("sign_key");
        String iss = props.getProperty("issuer");
        String ade = props.getProperty("access_date_expiration");
        String rde = props.getProperty("refresh_date_expiration");

        try {
            key = base64ToKey(k);
        } catch (Exception e) {
            key = generateKey();
            changed = true;
        }

        if (iss == null) {
            iss = UUID.randomUUID().toString();
            changed = true;
        }
        issuer = iss;

        try {
            accessDateExpiration = Integer.parseInt(ade);
            if (accessDateExpiration < 1) {
                throw new RuntimeException("Invalid date");
            }
        } catch (Exception e) {
            log.warn("Invalid access token date expiration, set " + DEFAULT_ACCESS_DATE_EXPIRATION);
            accessDateExpiration = DEFAULT_ACCESS_DATE_EXPIRATION;
            changed = true;
        }

        try {
            refreshDateExpiration = Integer.parseInt(rde);
            if (refreshDateExpiration < 1) {
                throw new RuntimeException("Invalid date");
            }
        } catch (Exception e) {
            log.warn("Invalid refresh token date expiration, set " + DEFAULT_REFRESH_DATE_EXPIRATION);
            refreshDateExpiration = DEFAULT_REFRESH_DATE_EXPIRATION;
            changed = true;
        }

        properties = new Properties();
        properties.setProperty("issuer", issuer);
        properties.setProperty("access_date_expiration", String.valueOf(accessDateExpiration));
        properties.setProperty("refresh_date_expiration", String.valueOf(refreshDateExpiration));
        properties.setProperty("sign_key", keyToBase64(key));
        log.info("Loaded configuration: " + properties);

        if (changed) {
            log.info("Config changed, saving...");
            saveConfig(properties);
            log.info("Config saved");
        }
    }

    @Override
    public Properties loadConfig() {
        File file = new File(CONFIGURATION_DIR_PATH, CONFIGURATION_FILE_PATH);
        Properties props = new Properties();
        try (InputStream fis = new FileInputStream(file)) {
            props.load(fis);
        } catch (IOException e) {
            log.warn(e + file.getAbsolutePath());
            props = getDefaultProperties();
            saveConfig(props);
        }
        return props;
    }

    @Override
    public void save() {
        saveConfig(properties);
    }

    public void saveConfig(Properties props) {
        File file = new File(CONFIGURATION_DIR_PATH, CONFIGURATION_FILE_PATH);

        // create folder and/or file if not exists
        try {
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            if (!file.exists()) {
                file.createNewFile();
            }
        } catch (IOException e) {
            log.error(e);
        }

        try (OutputStream fos = new FileOutputStream(file, false)) {
            props.store(fos, "JWT Properties");
        } catch (IOException e) {
            log.error(e);
        }
    }


    private SecretKey generateKey() {
        return Keys.secretKeyFor(SignatureAlgorithm.HS256);
    }

    private String generateKeyBase64() {
        SecretKey k = generateKey();
        return byteToBase64(k.getEncoded());
    }

    private static SecretKey base64ToKey(String keyBase64) {
        byte[] keyBytes = base64ToByte(keyBase64);
        return new SecretKeySpec(keyBytes, "HmacSHA256");
    }

    private static String keyToBase64(SecretKey key) {
        byte[] keyBytes = key.getEncoded();
        return byteToBase64(keyBytes);
    }

    private static String byteToBase64(byte[] bytes) {
        Base64.Encoder encoder = Base64.getEncoder();
        return encoder.encodeToString(bytes);
    }

    private static byte[] base64ToByte(String str) {
        Base64.Decoder decoder = Base64.getDecoder();
        return decoder.decode(str);
    }
}
