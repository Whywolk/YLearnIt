/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2024.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.security.dao;

import org.whywolk.ylearnit.security.domain.User;
import org.whywolk.ylearnit.security.domain.UserEmail;
import org.whywolk.ylearnit.security.domain.UserPassword;
import org.whywolk.ylearnit.security.domain.UserRole;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class UserRelational {
    private String login;
    private String name;
    private String description;
    private Timestamp timeAdded;
    private String hash;
    private String salt;
    private String email;
    private String roleName;

    public UserRelational() {
    }

    public static List<User> mapToModel(List<UserRelational> urList) {
        List<User> users = new ArrayList<>();
        User currentUser = null;
        for (UserRelational ur : urList) {
            if (currentUser == null || !ur.login.equals(currentUser.getLogin())) {
                UserPassword password = new UserPassword(ur.hash, ur.salt);
                currentUser = new User(ur.login, ur.name, ur.description,
                        ur.timeAdded.toLocalDateTime(), password);
                users.add(currentUser);
            }
            if (ur.roleName != null) {
                currentUser.addRole(new UserRole(ur.roleName));
            }
            if (ur.email != null) {
                currentUser.setEmail(new UserEmail(ur.email));
            }
        }
        return users;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getTimeAdded() {
        return timeAdded;
    }

    public void setTimeAdded(Timestamp timeAdded) {
        this.timeAdded = timeAdded;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
