/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2024.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.security.dao;

import org.apache.commons.dbutils.QueryRunner;
import org.whywolk.ylearnit.security.domain.UserRole;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;

public class JdbcRoleDao implements RoleDao {
    private final DataSource ds;
    private final QueryRunner run;

    public JdbcRoleDao(DataSource ds) {
        this.ds = ds;
        this.run = new QueryRunner(this.ds);
    }

    @Override
    public List<UserRole> getAll() {
        RoleHandler h = new RoleHandler();
        String sql = "SELECT r.role_name FROM roles r";
        try {
            List<RoleRelational> roleList = run.query(sql, h);
            return RoleRelational.mapToModel(roleList);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean exists(UserRole role) {
        List<UserRole> roles = getAll();
        return roles.contains(role);
    }

    @Override
    public void create(UserRole role) {
        String sql = "INSERT INTO roles(role_name) VALUES (?)";
        try {
            run.execute(sql, role.getRole());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update(UserRole oldRole, UserRole newRole) {
        String sql =
                "UPDATE roles" +
                "   SET role_name = ?" +
                " WHERE role_name = ?";
        try {
            run.execute(sql, newRole.getRole(), oldRole.getRole());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(UserRole role) {
        String sql =
                "DELETE FROM roles" +
                " WHERE role_name = ?";
        try {
            run.execute(sql, role.getRole());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
