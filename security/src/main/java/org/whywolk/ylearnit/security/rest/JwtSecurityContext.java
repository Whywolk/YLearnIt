/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022-2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.security.rest;

import io.jsonwebtoken.Claims;
import jakarta.ws.rs.core.SecurityContext;
import org.whywolk.ylearnit.security.dto.UserPrincipal;

import java.security.Principal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class JwtSecurityContext implements SecurityContext {
    private final Claims claims;
    private final String issuer;
    private final String subject;
    private final Set<String> roles;
    private final UserPrincipal principal;

    public JwtSecurityContext(Claims claims) {
        this.claims = claims;
        this.issuer = claims.getIssuer();
        this.subject = claims.getSubject();
        List<String> roles = (List<String>) claims.get("roles");
        this.roles = new HashSet<>(roles);
        // FIXME: change to UUID in future
        this.principal = new UserPrincipal(0, this.subject);
    }

    @Override
    public Principal getUserPrincipal() {
        return this.principal;
    }

    public String getSubject() {
        return subject;
    }

    @Override
    public boolean isUserInRole(String role) {
        return this.roles.contains(role);
    }

    @Override
    public boolean isSecure() {
        return false;
    }

    @Override
    public String getAuthenticationScheme() {
        return null;
    }
}
