/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.security.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.whywolk.ylearnit.security.service.AdminService;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

@Tag(name = "Admin resource")
@Path("/admin")
public class AdminResource {
    private static final Logger log = LogManager.getLogger(AdminResource.class);
    private static final String SECRET_FILE_PATH = "/etc/secrets/yli.properties";
    private static final String UPLOAD_SECRET_ENVIRONMENT_VARIABLE = "UPLOAD_SECRET";
    private final AdminService service;

    @Inject
    public AdminResource(AdminService service) {
        this.service = service;
    }

    @GET
    @Path("/download")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @Operation(summary = "Download file")
    @Secured(requiredPermissions = "admin")
    @SecurityRequirement(name = "JWT")
    public Response downloadFile(@QueryParam("file") String filePath) {
        try {
            log.info("Downloading file " + filePath);
            File file = service.downloadFile(filePath);
            return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM)
                    .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"")
                    .build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_FOUND).entity(e.getMessage()).build();
        }
    }

    @POST
    @Path("/upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Operation(summary = "Upload file")
    @Secured(requiredPermissions = "admin")
    @SecurityRequirement(name = "JWT")
    public Response uploadFile(@FormDataParam("file") InputStream fileInputStream,
                               @FormDataParam("file") FormDataContentDisposition fileDetail,
                               @FormDataParam("dirPath") String dirPath) {
        try {
            log.info("Uploading file " + fileDetail.getFileName() + " at " + dirPath);
            service.uploadFile(fileInputStream, fileDetail.getFileName(), dirPath);
            return Response.ok().build();
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }

    @POST
    @Path("/upload-secret")
    @Operation(summary = "Upload file using secret")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadFileWithSecret(@FormDataParam("file") InputStream fileInputStream,
                                         @FormDataParam("file") FormDataContentDisposition fileDetail,
                                         @FormDataParam("dirPath") String dirPath,
                                         @FormDataParam("secret") String secret) {
        String env = System.getenv(UPLOAD_SECRET_ENVIRONMENT_VARIABLE);
        if (env != null && env.equals(secret)) {
            return this.uploadFile(fileInputStream, fileDetail, dirPath);
        }
        try (InputStream fis = new FileInputStream(SECRET_FILE_PATH)) {
            Properties properties = new Properties();
            properties.load(fis);
            if (!properties.getProperty("upload_secret").equals(secret)) {
                throw new RuntimeException("Wrong secret");
            }

            return this.uploadFile(fileInputStream, fileDetail, dirPath);
        } catch (Exception e) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(e.getMessage()).build();
        }
    }
}
