/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.security.service;

import java.io.*;

public class AdminService {

    public AdminService() {
    }

    public File downloadFile(String filePath) {
        File file = new File(filePath);
        if (!file.exists() || file.isDirectory()) {
            throw new RuntimeException("File not found");
        }

        return file;
    }

    public void uploadFile(InputStream fileInputStream, String fileName, String dirPath) {
        File file = new File(dirPath, fileName);

        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }

        try (OutputStream fos = new FileOutputStream(file)) {
            byte[] buffer = new byte[1024];

            int count;
            // reading buffer
            while((count = fileInputStream.read(buffer)) != -1){
                fos.write(buffer, 0, count);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
