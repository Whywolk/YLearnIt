/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022-2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.security.service;

import io.jsonwebtoken.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.whywolk.ylearnit.security.domain.User;
import org.whywolk.ylearnit.security.domain.UserPassword;
import org.whywolk.ylearnit.security.domain.UserRole;
import org.whywolk.ylearnit.security.dao.UserDao;
import org.whywolk.ylearnit.security.dto.UserAuthDto;
import org.whywolk.ylearnit.security.observer.UserServiceListener;
import org.whywolk.ylearnit.security.observer.UserServiceObservable;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserService implements UserServiceObservable {
    private static final Logger log = LogManager.getLogger(UserService.class);

    private final UserDao userDao;
    private final JwtService jwtService;
    private final PasswordsService passwordsService;
    private final Set<UserServiceListener> listeners;

    public UserService(UserDao userDao) {
        this.userDao = userDao;
        this.jwtService = new JwtService();
        this.passwordsService = new PasswordsService();
        this.listeners = new HashSet<>();
    }

    public User getByLogin(String login) {
        return userDao.getByLogin(login);
    }

    public List<User> getAll() {
        return userDao.getAll();
    }

    public boolean exists(String login) {
        try {
            User user = getByLogin(login);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public User auth(String login, String password) {
        try {
            User user = getByLogin(login);
            UserPassword expectedPassword = user.getPassword();
            if (passwordsService.isCorrectPassword(password, expectedPassword)) {
                return user;
            } else {
                throw new RuntimeException();
            }
        } catch (Exception e) {
            String msg = "Wrong login or password";
            log.warn(msg);
            throw new RuntimeException(msg);
        }
    }

    public void register(UserAuthDto authDto) {
        if (! isValid(authDto.getLogin())) {
            String msg = "Invalid login format";
            log.warn(msg);
            throw new RuntimeException(msg);
        }

        if (! passwordsService.isValid(authDto.getPassword())) {
            String msg = "Invalid password format";
            log.warn(msg);
            throw new RuntimeException(msg);
        }

        if (exists(authDto.getLogin())){
            String msg = "User '" + authDto.getLogin() + "' already exists";
            log.warn(msg);
            throw new RuntimeException(msg);
        }

        User user = new User(authDto.getLogin(), generateUserPassword(authDto.getPassword()));
        user.addRole(new UserRole("user"));

        userDao.create(user);

        log.info("User '" + user.getLogin() + "' registered");

        user = getByLogin(authDto.getLogin());
        onUserCreate(user);
    }


    public Map<String, String> generateTokens(String refreshToken) {
        Claims claims = getJwsClaims(refreshToken);
        if (!claims.get("type").equals("refresh")) {
            String msg = "Provided token is not refresh";
            log.warn(msg);
            throw new RuntimeException(msg);
        }
        User user = getByLogin(claims.getSubject());
        return jwtService.generateTokens(user);
    }

    public Map<String, String> generateTokens(UserAuthDto authDto) {
        User user = auth(authDto.getLogin(), authDto.getPassword());
        return generateTokens(user);
    }

    public Map<String, String> generateTokens(User user) {
        return jwtService.generateTokens(user);
    }

    public Claims getJwsClaims(String jws) {
        return jwtService.getJwsClaims(jws).getBody();
    }

    @Override
    public void addListener(UserServiceListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeListener(UserServiceListener listener) {
        listeners.remove(listener);
    }

    @Override
    public void onUserCreate(User user) {
        listeners.forEach(l -> l.onUserCreate(user));
    }

    private boolean isValid(String login) {
        if (login.length() < 3 || login.length() > 25) {
            return false;
        }

        // Any of allowed symbols: english letters, numbers, special symbols
        Pattern p = Pattern.compile("^[A-Za-z0-9_#?!&*~-]+$");
        Matcher m = p.matcher(login);

        return m.matches();
    }

    private UserPassword generateUserPassword(String password) {
        return passwordsService.generateUserPassword(password);
    }
}
