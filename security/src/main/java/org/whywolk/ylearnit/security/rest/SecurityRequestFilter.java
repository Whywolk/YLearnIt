/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022-2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.security.rest;

import jakarta.annotation.Priority;
import jakarta.inject.Inject;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.SecurityContext;
import org.whywolk.ylearnit.security.service.UserService;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Arrays;

@Secured
@Priority(Priorities.AUTHENTICATION)
public class SecurityRequestFilter implements ContainerRequestFilter {

    @Context
    private ResourceInfo resourceInfo;
    private final UserService userService;

    @Inject
    public SecurityRequestFilter(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        String authHeader = requestContext.getHeaderString("Authorization");

        if (authHeader == null || !authHeader.startsWith("Bearer")) {
            requestContext.abortWith(Response
                    .status(Response.Status.UNAUTHORIZED)
                    .entity("JWT required").build());
        } else {
            String token = authHeader.split(" ")[1];

            try {
                SecurityContext securityContext = new JwtSecurityContext(this.userService.getJwsClaims(token));
                requestContext.setSecurityContext(securityContext);

                Method method = this.resourceInfo.getResourceMethod();

                if (method != null) {
                    Secured secured = method.getAnnotation(Secured.class);

                    // no permission required, but jwt anyway is need
                    if (secured.requiredPermissions().length == 0) {
                        return;
                    }

                    // if not in roles then error
                    for (String role : secured.requiredPermissions()) {
                        if (! securityContext.isUserInRole(role)) {
                            requestContext.abortWith(Response
                                    .status(Response.Status.UNAUTHORIZED)
                                    .entity("Required roles: " + Arrays.toString(secured.requiredPermissions())).build());
                        }
                    }

                    // OK
                    return;
                }
            } catch (Exception e) {
                requestContext.abortWith(Response
                        .status(Response.Status.UNAUTHORIZED)
                        .entity(e.getMessage()).build());
            }
        }
    }
}
