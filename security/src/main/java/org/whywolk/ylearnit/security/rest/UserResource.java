/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022-2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.security.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.SecurityContext;
import org.whywolk.ylearnit.security.dto.UserPrincipal;
import org.whywolk.ylearnit.security.domain.User;
import org.whywolk.ylearnit.security.dto.UserDto;
import org.whywolk.ylearnit.security.service.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Tag(name = "Users resource")
@Path("/users")
public class UserResource {
    private final UserService userService;

    @Inject
    public UserResource(UserService userService) {
        this.userService = userService;
    }

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get users")
    @Secured(requiredPermissions = "admin")
    @SecurityRequirement(name = "JWT")
    public List<UserDto> getAll() {
        List<User> users = userService.getAll();
        List<UserDto> dtos = users.stream()
                .map(user -> new UserDto(user))
                .collect(Collectors.toList());
        return dtos;
    }

    @GET
    @Path("/{login}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get info about user")
    @Secured(requiredPermissions = "user")
    @SecurityRequirement(name = "JWT")
    public UserDto getInfoByLogin(@PathParam("login") String login,
                                  @Context SecurityContext securityContext) {
        UserPrincipal userPrincipal = (UserPrincipal) securityContext.getUserPrincipal();
        Integer userId = userPrincipal.getId();
        String username = userPrincipal.getName();

        if (username.equals(login)) {
            User user = userService.getByLogin(login);
            return new UserDto(user);
        }

        throw new RuntimeException("Wrong user!");
    }
}
