/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.security.config;

import javax.crypto.SecretKey;
import java.util.Properties;

public interface JwtConfig {
    String getIssuer();

    int getAccessDateExpiration();

    int getRefreshDateExpiration();

    SecretKey getKey();

    int getDefaultAccessDateExpiration();

    int getDefaultRefreshDateExpiration();

    Properties getDefaultProperties();

    void initConfigWith(Properties props);

    Properties loadConfig();

    void save();
}
