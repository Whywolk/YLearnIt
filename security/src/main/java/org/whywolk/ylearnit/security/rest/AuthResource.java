/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.security.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.whywolk.ylearnit.security.dto.UserAuthDto;
import org.whywolk.ylearnit.security.service.UserService;

import java.util.Date;
import java.util.Map;

@Tag(name = "Auth endpoint")
@Path("/auth")
public class AuthResource {
    private static final Logger log = LogManager.getLogger(AuthResource.class);
    private final UserService userService;

    @Inject
    public AuthResource(UserService userService) {
        this.userService = userService;
    }

    @GET
    @Path("/getCookie")
    @Produces(MediaType.TEXT_PLAIN)
    @Operation(summary = "TEST")
    public Response getCookie() {
        Response.ResponseBuilder r = Response.status(Response.Status.OK).entity("Cookie setted");
        NewCookie cookie = new NewCookie.Builder("refreshToken")
                .value("some-value")
                .path("/api/auth")
                .expiry(new Date(124, 2, 20, 10, 5))
                .httpOnly(true)
                .sameSite(NewCookie.SameSite.NONE)
                .secure(true)
                .build();
        r.cookie(cookie);
        return r.build();
    }

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Auth via login and password")
    public Response login(UserAuthDto authDto) {
        try {
            log.info("Sign in user '" + authDto.getLogin() + "'");
            Map<String, String> tokens = userService.generateTokens(authDto);
            if (! authDto.isRememberMe()) {
                tokens.remove("refresh");
            }
            log.info("Tokens created");
            Response.ResponseBuilder r = Response.status(Response.Status.OK).entity(tokens);

            return r.build();
        } catch (Exception e) {
            log.warn(e.getMessage());
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }

    }

    @POST
    @Path("/register")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Register user")
    public Response register(UserAuthDto authDto) {
        try {
            userService.register(authDto);

            Map<String, String> tokens = userService.generateTokens(authDto);
            log.info("Tokens created");

            Response.ResponseBuilder r = Response.status(Response.Status.OK).entity(tokens);

            return r.build();
        } catch (Exception e) {
            log.warn(e.getMessage());
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
    }

    @POST
    @Path("/refresh")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get new tokens with a refresh token")
    public Response refresh(@CookieParam("refreshToken") Cookie cookie, Map<String, String> token) {
        try {
            Map<String, String> tokens = userService.generateTokens(token.get("refresh"));
            log.info("Tokens created");
            return Response.status(Response.Status.OK).entity(tokens).build();
        } catch (Exception e) {
            log.warn(e.getMessage());
            return Response.status(Response.Status.UNAUTHORIZED).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/logout")
    @Produces(MediaType.TEXT_PLAIN)
    @Operation(summary = "Logout user, delete refresh token cookie")
    public Response logout(@CookieParam("refreshToken") Cookie cookie) {
        Response.ResponseBuilder r = Response.status(Response.Status.OK).entity("");
        if (cookie != null) {
            NewCookie newCookie = new NewCookie.Builder("refreshToken")
                    .value("")
                    .path("/api/auth")
                    .maxAge(0)
                    .httpOnly(true)
                    .build();
            r.cookie(newCookie);
            log.info("Cookie is dropped");
        }
        return r.build();
    }

    private NewCookie createTokenCookie(String token) {
        Date exp = userService.getJwsClaims(token).getExpiration();
        NewCookie cookie = new NewCookie.Builder("refreshToken")
                .value(token)
                .path("/api/auth")
                .expiry(exp)
                .httpOnly(true)
                .build();
        return cookie;
    }
}
