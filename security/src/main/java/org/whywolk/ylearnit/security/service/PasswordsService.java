/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.security.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.whywolk.ylearnit.security.domain.UserPassword;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.*;
import java.util.regex.Pattern;

public class PasswordsService {
    private static final Logger log = LogManager.getLogger(PasswordsService.class);

    private static final int ITERATIONS = 110000;
    private static final int KEY_LENGTH = 512;
    private static final int SALT_LENGTH = 512;
    private final List<Pattern> rules;
    private final Pattern allowSymbols;

    public PasswordsService() {
        Pattern upper = Pattern.compile("^(?=.*[A-Z])[A-Za-z0-9_#?!@$%^&*-]+$");
        Pattern lower = Pattern.compile("^(?=.*[a-z])[A-Za-z0-9_#?!@$%^&*-]+$");
        Pattern number = Pattern.compile("^(?=.*[0-9])[A-Za-z0-9_#?!@$%^&*-]+$");
        Pattern special = Pattern.compile("^(?=.*[_#?!@$%^&*-])[A-Za-z0-9_#?!@$%^&*-]+$");

        allowSymbols = Pattern.compile("^(?=.*[A-Za-z0-9_#?!@$%^&*-])[A-Za-z0-9_#?!@$%^&*-]+$");
        rules = Arrays.asList(upper, lower, number, special);
    }


    public boolean isValid(String password) {
        if (password.length() < 8 || password.length() > 40) {
            return false;
        }

        int countMatches = 0;
        for (Pattern p : rules) {
            if (p.matcher(password).matches()) {
                countMatches += 1;
            }
        }

        return countMatches >= 2 && allowSymbols.matcher(password).matches();
    }

    public boolean isCorrectPassword(String actualPassword, UserPassword expectedPassword) {
        return isCorrectPassword(actualPassword, expectedPassword.getSalt(), expectedPassword.getHash());
    }

    public boolean isCorrectPassword(String password, String salt, String expectedHash) {
        if (! isValid(password)) {
            return false;
        }

        Base64.Decoder decoder = Base64.getDecoder();

        char[] passwordChars = password.toCharArray();
        byte[] saltBytes = decoder.decode(salt);
        byte[] expectedHashBytes = decoder.decode(expectedHash);

        return isCorrectPassword(passwordChars, saltBytes, expectedHashBytes);
    }

    public UserPassword generateUserPassword(String password) {
        String salt = generateSaltBase64();
        String hash = generateHashBase64(password, salt);

        return new UserPassword(hash, salt);
    }

    public String generateSaltBase64() {
        byte[] salt = generateSalt(SALT_LENGTH);
        Base64.Encoder encoder = Base64.getEncoder();

        return encoder.encodeToString(salt);
    }

    public String generateHashBase64(String password, String salt) {
        if (! isValid(password)) {
            String msg = "Invalid password format";
            log.warn(msg);
            throw new RuntimeException(msg);
        }

        Base64.Decoder decoder = Base64.getDecoder();
        Base64.Encoder encoder = Base64.getEncoder();

        char[] passwordChars = password.toCharArray();
        byte[] saltBytes = decoder.decode(salt);
        if (saltBytes.length * 8 < SALT_LENGTH) {
            String msg = String.format("Salt length less than %d", SALT_LENGTH);
            log.warn(msg);
            throw new RuntimeException(msg);
        }
        byte[] hashBytes = generateHash(passwordChars, saltBytes, ITERATIONS, KEY_LENGTH);
        return encoder.encodeToString(hashBytes);
    }


    private boolean isCorrectPassword(char[] password, byte[] salt, byte[] expectedHash) {
        byte[] actualHash = generateHash(password, salt, ITERATIONS, KEY_LENGTH);

        return Arrays.equals(actualHash, expectedHash);
    }

    private byte[] generateSalt(int length) {
        // bits to bytes
        length = length >> 3;
        Random random = new SecureRandom();
        byte[] bytes = new byte[length];
        random.nextBytes(bytes);
        return bytes;
    }

    private byte[] generateHash(char[] password, byte[] salt, int iterations, int keyLength) {
        try {
            PBEKeySpec spec = new PBEKeySpec(password, salt, iterations, keyLength);
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
            SecretKey key = skf.generateSecret(spec);
            byte[] hash = key.getEncoded();
            spec.clearPassword();
            return hash;
        } catch (NoSuchAlgorithmException e) {
            log.fatal(e);
            throw new RuntimeException(e);
        } catch (InvalidKeySpecException e) {
            log.error(e);
            throw new RuntimeException(e);
        }
    }
}
