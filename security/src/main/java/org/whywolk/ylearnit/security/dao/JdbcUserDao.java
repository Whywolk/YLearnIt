/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2024.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.security.dao;

import org.apache.commons.dbutils.QueryRunner;
import org.whywolk.ylearnit.security.domain.User;
import org.whywolk.ylearnit.security.domain.UserEmail;
import org.whywolk.ylearnit.security.domain.UserPassword;
import org.whywolk.ylearnit.security.domain.UserRole;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class JdbcUserDao implements UserDao {
    private final DataSource ds;
    private final QueryRunner run;

    public JdbcUserDao(DataSource ds) {
        this.ds = ds;
        this.run = new QueryRunner(this.ds);
    }

    @Override
    public User getByLogin(String login) {
        UserHandler h = new UserHandler();
        String sql =
            "SELECT u.login, u.name, u.description, u.time_added, p.password_hash, p.password_salt, e.email, r.role_name" +
            "  FROM users u" +
            "  LEFT JOIN passwords p ON p.user_id = u.user_id" +
            "  LEFT JOIN emails e ON e.user_id = u.user_id" +
            "  LEFT JOIN user_roles ur ON u.user_id = ur.user_id" +
            "  LEFT JOIN roles r ON ur.role_id = r.role_id" +
            " WHERE u.login = ?";
        try {
            List<UserRelational> urList = run.query(sql, h, login);
            return UserRelational.mapToModel(urList).get(0);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<User> getAll() {
        UserHandler h = new UserHandler();
        String sql =
            "SELECT u.login, u.name, u.description, u.time_added, p.password_hash, p.password_salt, e.email, r.role_name" +
            "  FROM users u" +
            "  LEFT JOIN passwords p ON p.user_id = u.user_id" +
            "  LEFT JOIN emails e ON e.user_id = u.user_id" +
            "  LEFT JOIN user_roles ur ON u.user_id = ur.user_id" +
            "  LEFT JOIN roles r ON ur.role_id = r.role_id";
        try {
            List<UserRelational> urList = run.query(sql, h);
            return UserRelational.mapToModel(urList);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void create(User user) {
        String sql = "INSERT INTO users(login, name, description, time_added) VALUES (?, ?, ?, ?)";
        try {
            run.execute(sql, user.getLogin(), user.getName(), user.getDescription(), user.getTimeAdded());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        if (user.hasPassword()) {
            createPassword(user);
        }

        if (user.hasEmail()) {
            createEmail(user);
        }

        if (! user.getRoles().isEmpty()) {
            createRoles(user);
        }
    }

    @Override
    public void update(User user) {
        User fetchedUser = getByLogin(user.getLogin());

        if (! fetchedUser.getName().equals(user.getName())
                || ! fetchedUser.getDescription().equals(user.getDescription())) {
            String sql =
                    "UPDATE users" +
                    "   SET name = ?," +
                    "       description = ?" +
                    " WHERE login = ?";
            try {
                run.execute(sql, user.getName(), user.getDescription(), user.getLogin());
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

        if (! fetchedUser.getPassword().equals(user.getPassword())) {
            updatePassword(user);
        }

        if (! Objects.equals(fetchedUser.getEmail(), user.getEmail())) {
            updateEmail(user);
        }

        if (! fetchedUser.getRoles().equals(user.getRoles())) {
            updateRoles(fetchedUser, user);
        }
    }

    @Override
    public void delete(User user) {
        String sql =
                "DELETE FROM users" +
                " WHERE login = ?";
        try {
            run.execute(sql, user.getLogin());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    private void createPassword(User user) {
        UserPassword password = user.getPassword();
        String sql =
                "INSERT INTO passwords(user_id, password_hash, password_salt) VALUES" +
                "   (SELECT u.user_id FROM users u WHERE u.login = ?, ?, ?)";
        try {
            run.execute(sql, user.getLogin(), password.getHash(), password.getSalt());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void updatePassword(User user) {
        UserPassword password = user.getPassword();
        String sql =
                "UPDATE passwords" +
                "   SET password_hash = ?," +
                "       password_salt = ?" +
                " WHERE user_id = (SELECT u.user_id FROM users u WHERE u.login = ?)";
        try {
            run.execute(sql, password.getHash(), password.getSalt(), user.getLogin());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void createEmail(User user) {
        UserEmail email = user.getEmail();
        String sql =
                "INSERT INTO emails(user_id, email) VALUES" +
                "   (SELECT u.user_id FROM users u WHERE u.login = ?, ?)";
        try {
            run.execute(sql, user.getLogin(), email.getEmail());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void updateEmail(User user) {
        UserEmail email = user.getEmail();
        String sql =
                "UPDATE emails" +
                "   SET email = ?" +
                " WHERE user_id = (SELECT u.user_id FROM users u WHERE u.login = ?)";
        try {
            run.execute(sql, email.getEmail(), user.getLogin());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void createRoles(User user) {
        for (UserRole role : user.getRoles()) {
            createRole(user, role);
        }
    }

    private void updateRoles(User oldUser, User newUser) {
        Set<UserRole> oldRoles = oldUser.getRoles();
        Set<UserRole> newRoles = newUser.getRoles();

        for (UserRole oldRole : oldRoles) {
            if (! newRoles.contains(oldRole)) {
                deleteRole(newUser, oldRole);
            }
        }

        for (UserRole newRole : newRoles) {
            if (! oldRoles.contains(newRole)) {
                createRole(newUser, newRole);
            }
        }
    }

    private void createRole(User user, UserRole role) {
        String sql =
                "INSERT INTO user_roles(user_id, role_id) VALUES" +
                "   (SELECT u.user_id FROM users u WHERE u.login = ?," +
                "    SELECT r.role_id FROM roles r WHERE r.role_name = ?)";
        try {
            run.execute(sql, user.getLogin(), role.getRole());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void deleteRole(User user, UserRole role) {
        String sql =
                "DELETE FROM user_roles" +
                " WHERE user_id = (SELECT u.user_id FROM users u WHERE u.login = ?)" +
                "   AND role_id = (SELECT r.role_id FROM roles r WHERE r.role_name = ?)";
        try {
            run.execute(sql, user.getLogin(), role.getRole());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
