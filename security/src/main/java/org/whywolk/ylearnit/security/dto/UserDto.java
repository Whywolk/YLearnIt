/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022-2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.security.dto;

import org.whywolk.ylearnit.security.domain.User;

public class UserDto {

    private String login;
    private String name;
    private String description;

    public UserDto(User user) {
        this.login = user.getLogin();
        this.name = user.getName();
        this.description = user.getDescription();
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
