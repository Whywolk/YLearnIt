/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2024.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.security.dao;

import org.whywolk.ylearnit.security.domain.UserRole;

import java.util.List;

public interface RoleDao {
    List<UserRole> getAll();

    boolean exists(UserRole role);

    void create(UserRole role);

    void update(UserRole oldRole, UserRole newRole);

    void delete(UserRole role);
}
