package org.whywolk.ylearnit.security.observer;

import org.whywolk.ylearnit.security.domain.User;

public interface UserServiceObservable {
    void addListener(UserServiceListener listener);
    void removeListener(UserServiceListener listener);
    void onUserCreate(User user);
}
