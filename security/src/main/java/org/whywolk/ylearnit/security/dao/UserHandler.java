/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2024.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.security.dao;

import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.util.HashMap;
import java.util.Map;

public class UserHandler extends BeanListHandler<UserRelational> {

    public UserHandler() {
        super(UserRelational.class, new BasicRowProcessor(new BeanProcessor(getColumnsToFieldsMap())));
    }

    private static Map<String, String> getColumnsToFieldsMap() {
        Map<String, String> columnsToFieldsMap = new HashMap<>();
        columnsToFieldsMap.put("LOGIN", "login");
        columnsToFieldsMap.put("NAME", "name");
        columnsToFieldsMap.put("DESCRIPTION", "description");
        columnsToFieldsMap.put("TIME_ADDED", "timeAdded");
        columnsToFieldsMap.put("PASSWORD_HASH", "hash");
        columnsToFieldsMap.put("PASSWORD_SALT", "salt");
        columnsToFieldsMap.put("EMAIL", "email");
        columnsToFieldsMap.put("ROLE_NAME", "roleName");
        return columnsToFieldsMap;
    }
}
