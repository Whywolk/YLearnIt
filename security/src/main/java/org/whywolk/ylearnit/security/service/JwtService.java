/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2022-2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.security.service;

import io.jsonwebtoken.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.whywolk.ylearnit.security.config.JwtConfig;
import org.whywolk.ylearnit.security.config.PropsJwtConfig;
import org.whywolk.ylearnit.security.domain.User;

import java.util.*;

public class JwtService {
    private static final Logger log = LogManager.getLogger(JwtService.class);

    private final JwtConfig config;

    public JwtService() {
        config = new PropsJwtConfig();
        config.initConfigWith(config.loadConfig());
    }

    public JwtService(JwtConfig config) {
        this.config = config;
    }

    public Map<String, String> generateTokens(User user) {
        Map<String, String> map = new HashMap<>();
        String id = UUID.randomUUID().toString();
        map.put("access", generateAccessJwt(id, user));
        map.put("refresh", generateRefreshJwt(id, user));
        return map;
    }

    public String generateAccessJwt(String id, User user) {
        JwtBuilder builder = Jwts.builder();

        Date cur = new Date();

        // set 5 minutes offset
        Calendar curCalendar = Calendar.getInstance();
        curCalendar.setTimeInMillis(cur.getTime());
        curCalendar.add(Calendar.MINUTE, -5);
        Date curDate = curCalendar.getTime();

        Calendar expCalendar = Calendar.getInstance();
        expCalendar.setTimeInMillis(cur.getTime());
        expCalendar.add(Calendar.DATE, config.getAccessDateExpiration());
        Date expDate = expCalendar.getTime();

        String jws = builder
                .setId(id)
                .setIssuer(config.getIssuer())
                .setSubject(user.getLogin())
                .setIssuedAt(curDate)
                .setExpiration(expDate)
                .claim("roles", user.getRolesStrings())
                .claim("type", "access")
                .signWith(config.getKey())
                .compact();
        return jws;
    }

    public String generateRefreshJwt(String id, User user) {
        JwtBuilder builder = Jwts.builder();

        Date cur = new Date();

        // set 5 minutes offset
        Calendar curCalendar = Calendar.getInstance();
        curCalendar.setTimeInMillis(cur.getTime());
        curCalendar.add(Calendar.MINUTE, -5);
        Date curDate = curCalendar.getTime();

        Calendar expCalendar = Calendar.getInstance();
        expCalendar.setTimeInMillis(cur.getTime());
        expCalendar.add(Calendar.DATE, config.getRefreshDateExpiration());
        Date expDate = expCalendar.getTime();

        String jws = builder
                .setId(id)
                .setIssuer(config.getIssuer())
                .setSubject(user.getLogin())
                .setIssuedAt(curDate)
                .setExpiration(expDate)
                .claim("type", "refresh")
                .signWith(config.getKey())
                .compact();
        return jws;
    }

    public Jws<Claims> getJwsClaims(String jws) {
        JwtParserBuilder builder = Jwts.parserBuilder();
        Jws<Claims> claims = builder.setSigningKey(config.getKey())
                .build()
                .parseClaimsJws(jws);
        return claims;
    }
}
