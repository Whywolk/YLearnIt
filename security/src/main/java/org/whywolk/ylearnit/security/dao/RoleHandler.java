/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2024.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.security.dao;

import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.util.HashMap;
import java.util.Map;

public class RoleHandler extends BeanListHandler<RoleRelational> {

    public RoleHandler() {
        super(RoleRelational.class, new BasicRowProcessor(new BeanProcessor(getColumnsToFieldsMap())));
    }

    private static Map<String, String> getColumnsToFieldsMap() {
        Map<String, String> columnsToFieldsMap = new HashMap<>();
        columnsToFieldsMap.put("ROLE_NAME", "role");
        return columnsToFieldsMap;
    }
}
