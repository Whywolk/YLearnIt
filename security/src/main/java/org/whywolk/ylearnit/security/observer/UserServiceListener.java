package org.whywolk.ylearnit.security.observer;

import org.whywolk.ylearnit.security.domain.User;

public interface UserServiceListener {
    void onUserCreate(User user);
}
