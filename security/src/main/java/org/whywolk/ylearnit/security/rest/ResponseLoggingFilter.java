/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.security.rest;

import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.container.ContainerResponseFilter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.whywolk.ylearnit.security.dto.UserPrincipal;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class ResponseLoggingFilter implements ContainerResponseFilter {
    private static final Logger log = LogManager.getLogger(ResponseLoggingFilter.class);
    private static final Set<String> ignoringMethods = new HashSet<>(Arrays.asList("OPTIONS"));

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        UserPrincipal userPrincipal = (UserPrincipal) requestContext.getSecurityContext().getUserPrincipal();

        String method = requestContext.getMethod();
        String path = requestContext.getUriInfo().getAbsolutePath().getPath();
        int status = responseContext.getStatus();

        if (ignoringMethods.contains(method)) {
            return;
        }

        String msg = method + " " + status + " '" + path + "' ";
        if (userPrincipal != null) {
            msg += userPrincipal.getName();
        }

        if (status >= 400) {
            log.warn(msg);
        } else {
            log.info(msg);
        }
    }
}
