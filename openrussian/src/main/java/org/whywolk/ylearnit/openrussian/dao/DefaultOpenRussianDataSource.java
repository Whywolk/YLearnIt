/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2024.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.openrussian.dao;

public class DefaultOpenRussianDataSource {
    private static final OpenRussianDataSource instance = new OpenRussianDataSource("jdbc:h2:./db/openrussian.h2");

    public static OpenRussianDataSource getInstance() {
        return instance;
    }
}
