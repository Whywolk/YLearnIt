/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.openrussian.service;

import org.whywolk.ylearnit.openrussian.domain.Level;
import org.whywolk.ylearnit.openrussian.domain.Word;

import java.util.List;

public interface OpenRussianService {

    Word getById(int id);

    List<Word> getByIds(List<Integer> ids);

    List<Word> getByLevel(Level level);

    List<Word> getByLevel(String level);

    List<Word> find(String str, int page);

    List<Word> find(String str, String lang);
}
