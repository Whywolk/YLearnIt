/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.openrussian.domain;

import java.io.Serializable;
import java.util.Objects;

public class Translation implements Serializable {
    private Integer id;
    private String translation;
    private Lang lang;
    private Integer position;

    public Translation() {
    }

    public Translation(Integer id, String translation, Lang lang, Integer position) {
        this.id = id;
        this.translation = translation;
        this.lang = lang;
        this.position = position;
    }

    public Translation(Integer id, String translation, String lang, Integer position) {
        this(id, translation, Lang.valueOf(lang), position);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    public Lang getLang() {
        return lang;
    }

    public void setLang(Lang lang) {
        this.lang = lang;
    }

    public void setLang(String lang) {
        if (Lang.contains(lang)) {
            this.lang = Lang.get(lang);
        }
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "Translation{" +
                "id=" + id +
                ", lang=" + lang +
                ", position=" + position +
                ", translation='" + translation + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Translation that = (Translation) o;
        return translation.equals(that.translation)
                &&lang == that.lang
                && Objects.equals(position, that.position);
    }

    @Override
    public int hashCode() {
        return Objects.hash(translation, lang, position);
    }
}
