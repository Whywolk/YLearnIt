package org.whywolk.ylearnit.openrussian.domain;

public enum Lang {
    en("en"),
    de("de");

    private final String lang;

    Lang(String lang) {
        this.lang = lang;
    }

    public static Lang get(String lang) {
        for (Lang l : Lang.values()) {
            if (l.getCode().equals(lang)) {
                return l;
            }
        }
        throw new IllegalStateException("Illegal lang='" + lang + "'");
    }

    public static boolean contains(String lang) {
        for (Lang l : Lang.values()) {
            if (l.getCode().equals(lang)) {
                return true;
            }
        }
        return false;
    }

    public String getCode() {
        return lang;
    }
}
