/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2024.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.openrussian.dao;

import org.whywolk.ylearnit.openrussian.domain.Translation;
import org.whywolk.ylearnit.openrussian.domain.Word;

import java.util.ArrayList;
import java.util.List;

public class WordRelational {
    private int wordId;
    private String bare;
    private String accented;
    private int rank;
    private String level;
    private int translationId;
    private String lang;
    private int position;
    private String translation;

    public static List<Word> mapToModel(List<WordRelational> wordList) {
        List<Word> words = new ArrayList<>();
        Word currentWord = null;
        for (WordRelational wr : wordList) {
            if (currentWord == null || (currentWord.getId() != wr.wordId)) {
                currentWord = new Word(wr.wordId, wr.bare, wr.accented, wr.rank, wr.level);
                words.add(currentWord);
            }
            Translation tl = new Translation(wr.translationId, wr.translation, wr.lang, wr.position);
            currentWord.addTranslation(tl);
        }
        return words;
    }

    public int getWordId() {
        return wordId;
    }

    public void setWordId(int wordId) {
        this.wordId = wordId;
    }

    public String getBare() {
        return bare;
    }

    public void setBare(String bare) {
        this.bare = bare;
    }

    public String getAccented() {
        return accented;
    }

    public void setAccented(String accented) {
        this.accented = accented;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public int getTranslationId() {
        return translationId;
    }

    public void setTranslationId(int translationId) {
        this.translationId = translationId;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }
}
