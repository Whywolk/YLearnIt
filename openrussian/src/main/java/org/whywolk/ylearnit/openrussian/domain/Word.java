/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.openrussian.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Word implements Serializable {
    private Integer id;
    private String bare;
    private String accented;
    private Integer rank;
    private Level level;
    private List<Translation> translations;

    public Word() {
        this.translations = new ArrayList<>();
    }

    public Word(Integer id, String bare, String accented, Integer rank, Level level) {
        this();
        this.id = id;
        this.bare = bare;
        this.accented = accented;
        this.rank = rank;
        this.level = level;
    }

    public Word(Integer id, String bare, String accented, Integer rank, String level) {
        this(id, bare, accented, rank, Level.valueOf(level));
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBare() {
        return bare;
    }

    public void setBare(String bare) {
        this.bare = bare;
    }

    public String getAccented() {
        return accented;
    }

    public void setAccented(String accented) {
        this.accented = accented;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public void setLevel(String level) {
        if (Level.contains(level)) {
            this.level = Level.get(level);
        } else {
            this.level = Level.NO_CATEGORY;
        }
    }

    public List<Translation> getTranslations() {
        return translations;
    }

    public List<Translation> getTranslations(String lang) {
        Lang l = Lang.get(lang);
        return translations.stream()
                .filter(t -> t.getLang() == l)
                .collect(Collectors.toList());
    }

    public void setTranslations(List<Translation> translations) {
        this.translations = translations;
    }

    public void addTranslation(Translation t) {
        this.translations.add(t);
    }

    @Override
    public String toString() {
        return "Word{" +
                "id=" + id +
                ", bare='" + bare + '\'' +
                ", accented='" + accented + '\'' +
                ", rank=" + rank +
                ", level=" + level +
                ", translations=" + translations +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Word word = (Word) o;
        return bare.equals(word.bare)
                && accented.equals(word.accented)
                && Objects.equals(rank, word.rank)
                && level == word.level
                && translations.equals(word.translations);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bare, accented, rank, level, translations);
    }
}
