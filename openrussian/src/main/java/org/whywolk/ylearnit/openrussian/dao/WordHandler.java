/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2024.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.openrussian.dao;

import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.util.HashMap;
import java.util.Map;

public class WordHandler extends BeanListHandler<WordRelational> {

    public WordHandler() {
        super(WordRelational.class, new BasicRowProcessor(new BeanProcessor(getColumnsToFieldsMap())));
    }

    private static Map<String, String> getColumnsToFieldsMap() {
        Map<String, String> columnsToFieldsMap = new HashMap<>();
        columnsToFieldsMap.put("WORD_ID", "wordId");
        columnsToFieldsMap.put("BARE", "bare");
        columnsToFieldsMap.put("ACCENTED", "accented");
        columnsToFieldsMap.put("LEVEL", "level");
        columnsToFieldsMap.put("WORD_RANK", "rank");
        columnsToFieldsMap.put("TRANSLATION_ID", "translationId");
        columnsToFieldsMap.put("LANG", "lang");
        columnsToFieldsMap.put("POSITION", "position");
        columnsToFieldsMap.put("TL", "translation");
        return columnsToFieldsMap;
    }
}
