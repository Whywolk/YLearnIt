package org.whywolk.ylearnit.openrussian.domain;

public enum Level {
    A1("A1"),
    A2("A2"),
    B1("B1"),
    B2("B2"),
    C1("C1"),
    C2("C2"),
    NO_CATEGORY("NO_CAT");

    private final String level;

    Level(String level) {
        this.level = level;
    }

    public static Level get(String level) {
        for (Level l : Level.values()) {
            if (l.getLevel().equals(level)) {
                return l;
            }
        }
        throw new IllegalStateException("Illegal level='" + level + "'");
    }

    public static boolean contains(String level) {
        for (Level l : Level.values()) {
            if (l.getLevel().equals(level)) {
                return true;
            }
        }
        return false;
    }

    public String getLevel() {
        return level;
    }
}
