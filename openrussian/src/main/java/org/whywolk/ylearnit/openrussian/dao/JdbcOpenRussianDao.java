/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2024.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.openrussian.dao;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ColumnListHandler;
import org.whywolk.ylearnit.openrussian.domain.Level;
import org.whywolk.ylearnit.openrussian.domain.Word;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;

public class JdbcOpenRussianDao implements OpenRussianDao {
    private final int DEFAULT_PAGE_SIZE = 50;
    private final DataSource ds;
    private final QueryRunner run;

    public JdbcOpenRussianDao(DataSource ds) {
        this.ds = ds;
        this.run = new QueryRunner(this.ds);
    }

    @Override
    public Word getById(int id) {
        String sql = selectHead() + "WHERE w.id = ?";
        try {
            List<WordRelational> wordList = run.query(sql, new WordHandler(), id);
            return WordRelational.mapToModel(wordList).get(0);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Word> getByIds(List<Integer> ids) {
        String sql = selectHead() + "WHERE w.id = ANY(?)";
        try {
            List<WordRelational> wordList = run.query(sql, new WordHandler(), (Object) ids.toArray());
            return WordRelational.mapToModel(wordList);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Word> getByLevel(Level l) {
        String sql = selectHead() + "WHERE w.level = ?";
        try {
            List<WordRelational> wordList = run.query(sql, new WordHandler(), l.getLevel());
            return WordRelational.mapToModel(wordList);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Word> find(String str, int page) {
        if (page < 0) {
            throw new RuntimeException("Page number shouldn't be lesser than 0");
        }

        String s = str.trim();
        if (s.isEmpty()) {
            throw new RuntimeException("Find string cannot be empty");
        }

        String sql = selectHead() +
                " WHERE LOWER(w.bare) LIKE LOWER(?)" +
                "    OR LOWER(t.tl) LIKE LOWER(?)" +
                " ORDER BY LENGTH(w.bare), LENGTH(t.tl)" +
                " LIMIT ? OFFSET ?";
        try {
            List<Integer> ids = run.query(sql, new ColumnListHandler<>("word_id"),
                    s, s, DEFAULT_PAGE_SIZE, page * DEFAULT_PAGE_SIZE);

            sql = selectHead() +
                    " WHERE w.id = ANY(?)" +
                    " ORDER BY LENGTH(w.bare), LENGTH(t.tl)";
            List<WordRelational> wordList = run.query(sql, new WordHandler(), (Object) ids.toArray());
            return WordRelational.mapToModel(wordList);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private String selectHead() {
        return  "SELECT w.id as word_id, w.bare, w.accented, w.level, w.rank AS word_rank, " +
                "       t.id as translation_id, t.tl, t.lang, t.position" +
                "  FROM words as w" +
                "  LEFT JOIN translations t ON t.word_id = w.id ";
    }
}
