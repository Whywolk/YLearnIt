package org.whywolk.ylearnit.openrussian;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import jakarta.inject.Singleton;
import org.whywolk.ylearnit.openrussian.dao.JdbcOpenRussianDao;
import org.whywolk.ylearnit.openrussian.dao.OpenRussianDao;
import org.whywolk.ylearnit.openrussian.dao.OpenRussianDataSource;
import org.whywolk.ylearnit.openrussian.service.OpenRussianService;
import org.whywolk.ylearnit.openrussian.service.OpenRussianServiceImpl;

public class OpenRussianModule extends AbstractModule {

    @Provides @Singleton
    public OpenRussianService provideOpenRussianService(OpenRussianDao openRussianDao) {
        return new OpenRussianServiceImpl(openRussianDao);
    }

    @Provides @Singleton
    public OpenRussianDao provideOpenRussianDao(OpenRussianDataSource openRussianDataSource) {
        return new JdbcOpenRussianDao(openRussianDataSource.getDataSource());
    }

    @Provides @Singleton
    public OpenRussianDataSource provideOpenRussianDataSource() {
        return new OpenRussianDataSource("jdbc:h2:./db/openrussian.h2");
    }
}
