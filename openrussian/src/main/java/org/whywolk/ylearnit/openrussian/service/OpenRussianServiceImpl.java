/*
 *  License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
 *  Copyright (c) 2023.  Author: Alex Shirshov <https://github.com/Whywolk>
 */

package org.whywolk.ylearnit.openrussian.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.whywolk.ylearnit.openrussian.dao.JdbcOpenRussianDao;
import org.whywolk.ylearnit.openrussian.dao.OpenRussianDao;
import org.whywolk.ylearnit.openrussian.domain.Level;
import org.whywolk.ylearnit.openrussian.domain.Word;

import java.util.List;

public class OpenRussianServiceImpl implements OpenRussianService {
    private static final Logger log = LogManager.getLogger(OpenRussianServiceImpl.class);
    private final OpenRussianDao dao;

    public OpenRussianServiceImpl(OpenRussianDao dao) {
        this.dao = dao;
    }

    @Override
    public Word getById(int id) {
        return dao.getById(id);
    }

    @Override
    public List<Word> getByIds(List<Integer> ids) {
        return dao.getByIds(ids);
    }

    @Override
    public List<Word> getByLevel(Level level) {
        return dao.getByLevel(level);
    }

    @Override
    public List<Word> getByLevel(String level) {
        return dao.getByLevel(Level.get(level));
    }

    @Override
    public List<Word> find(String str, int page) {
        return dao.find(str, page);
    }

    @Override
    public List<Word> find(String str, String lang) {
        return null;
    }
}
